package org.gcube.application.geoportal.common.legacy;

import static org.junit.Assert.assertEquals;

import org.gcube.application.geoportal.common.model.legacy.AccessPolicy;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.report.ConstraintCheck;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;

public class DefaultsTests {

    //@Test
    public void checkConstraints(){
        assertEquals(AccessPolicy.OPEN,ConstraintCheck.defaultFor(null,AccessPolicy.OPEN).evaluate());
        assertEquals(AccessPolicy.EMBARGOED,ConstraintCheck.defaultFor(AccessPolicy.EMBARGOED,AccessPolicy.OPEN).evaluate());
    }


    //@Test
    public void checkDefaults(){

        Concessione c= new Concessione();

        c.setDefaults();
        c.validate();
        System.out.println(c.getReport());
        // Mandatory fields without defaults
        assertEquals(ValidationReport.ValidationStatus.ERROR,c.getReport().getStatus());

        c=new Concessione();
        c.setRelazioneScavo(new RelazioneScavo());
        c.getRelazioneScavo().setPolicy(AccessPolicy.EMBARGOED);
        c.setPolicy(AccessPolicy.OPEN);
        assertEquals(AccessPolicy.EMBARGOED,c.getRelazioneScavo().getPolicy());
        c.setDefaults();
        assertEquals(AccessPolicy.EMBARGOED,c.getRelazioneScavo().getPolicy());
    }



}

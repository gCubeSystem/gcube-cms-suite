package org.gcube.application.geoportal.common;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportal.common.utils.Files;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilesTests {

    static File baseFolder=new File("../test-data/concessioni");

    //@Test
    public void testNames(){

        ArrayList<String> toTestStrings=new ArrayList<>(Arrays.asList(baseFolder.list()));
        toTestStrings.add("e. gna_topografia_timponedellamotta.qmd");

       for(String name : toTestStrings){
           if(name.contains(".")) {
               int extensionIndex = name.lastIndexOf(".");
               String originalExtension = extensionIndex>0?name.substring(extensionIndex):null;
               String obtained = Files.fixFilename(name);
               log.info(name + "->" + obtained + "[" + originalExtension + "]");
               if(originalExtension!=null)
                   assertTrue(obtained.endsWith(originalExtension));
           }
       }
    }

    //@Test
    public void testClustering() throws IOException {
        Map<String, List<File>> map=Files.getAllShapeSet(baseFolder.getParentFile(),true);
        map.forEach((k, v)->{
            System.out.println(k);
            v.forEach(f->{System.out.println(f.getName());});
        });

        assertTrue(map.get(new File(baseFolder,"pos.shp").getAbsolutePath()).size()==5);
        assertTrue(map.get(new File(baseFolder,"pianta.shp").getAbsolutePath()).size()==8);
    }

}

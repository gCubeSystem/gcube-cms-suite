package org.gcube.application.geoportal.common.model.plugins;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.application.geoportal.common.model.useCaseDescriptor.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
public class OperationDescriptor {

    public OperationDescriptor(String id) {
        this.id = id;
    }

    public OperationDescriptor(String id, String description) {
        this.id = id;
        this.description = description;
    }

    private String id;
    private Map<String, Field> parameters;
    private String description;

    private List<String> appliableToPhases;
}

package org.gcube.application.geoportal.common.model.plugins;

import javax.xml.bind.annotation.XmlRootElement;

import com.vdurmont.semver4j.Semver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
public class PluginDescriptor {

    public PluginDescriptor(String id, String type) {
        this.id = id;
        this.type = type;
    }

    private String id;
    private String type;
    private String label;
    private String description;
    private Semver version;
}

package org.gcube.application.geoportal.common.model.document.relationships;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Relationship {

    public static final String RELATIONSHIP_NAME="_relationshipName";
    public static final String TARGET_ID="_targetID";
    public static final String TARGET_UCD="_targetUCD";

    @JsonProperty(RELATIONSHIP_NAME)
    private String relationshipName;
    @JsonProperty(TARGET_ID)
    private String targetID;
    @JsonProperty(TARGET_UCD)
    private String targetUCD;

}

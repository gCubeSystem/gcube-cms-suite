package org.gcube.application.geoportal.common.utils;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Deprecated
public class ContextUtils {

	
	public static String getCurrentCaller(){
		try{
			String token=SecurityTokenProvider.instance.get();
			if(token==null) throw new Exception("Security Token is null");
			log.trace("Token is : "+token.substring(0,2)+"..."+token.substring(token.length()-3));
			AuthorizationEntry entry = authorizationService().get(token);
			return entry.getClientInfo().getId();
		}catch(Exception e ){
			log.warn("Unable to resolve token using legacy utils, using default ..",e);
			return "CMS_default_user";
		}
	}
}

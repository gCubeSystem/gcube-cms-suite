package org.gcube.application.geoportal.common.model.legacy;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

@EqualsAndHashCode(callSuper=true)
public class WorkspaceContent extends PersistedContent{

	private String mimetype;
	private String storageID;
	private String link;
	private String name;
}

package org.gcube.application.geoportal.common.model.legacy;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

@EqualsAndHashCode(callSuper=true)
public class GeoServerContent extends PersistedContent{

		//GeoServer Details
		private String geoserverHostName;		
		private String geoserverPath;	
		
		private List<String> fileNames=new ArrayList<String>();
		
	
		private String workspace;
		private String store;
		private String featureType;
}

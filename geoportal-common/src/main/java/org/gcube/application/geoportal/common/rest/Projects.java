package org.gcube.application.geoportal.common.rest;

import java.rmi.RemoteException;
import java.util.Iterator;

import org.bson.Document;
import org.gcube.application.geoportal.common.faults.InvalidRequestException;
import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.document.relationships.RelationshipNavigationObject;
import org.gcube.application.geoportal.common.model.rest.CreateRelationshipRequest;
import org.gcube.application.geoportal.common.model.rest.DeleteRelationshipRequest;
import org.gcube.application.geoportal.common.model.rest.PerformStepRequest;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.rest.RegisterFileSetRequest;

public interface Projects<P extends Project> {

	public Class<P> getManagedClass();

	// CRUD
	public P createNew(Document toCreate)throws RemoteException;
	public P getById(String id) throws RemoteException;
	public P updateDocument(String id,Document updatedDocument) throws RemoteException;
	public void deleteById(String id) throws RemoteException;
	public void deleteById(String id,Boolean force) throws RemoteException;
	
	public P patchDocument(String id, String path, Document updatedDocument) throws RemoteException;



	// CONFIG
	public Configuration getConfiguration() throws RemoteException;

	// QUERY
	public Iterator<P> query (QueryRequest request) throws RemoteException;
	public <C> Iterator<C> queryForClass (QueryRequest request,Class<C> clazz) throws RemoteException;
	public String queryForJSON(QueryRequest request)throws RemoteException;

	//Execution
	public P performStep(String id, PerformStepRequest request) throws RemoteException;

	//FileSets
	public P registerFileSet(String id, RegisterFileSetRequest req) throws RemoteException, InvalidRequestException;
	//FileSets
	public P deleteFileSet(String id, String path, Boolean force, Boolean ignore_errors) throws RemoteException;

	public P forceUnlock(String id) throws RemoteException;
	public P setAccessPolicy(String id, Access toSet) throws RemoteException;

	// Relationships
	public Project setRelation(CreateRelationshipRequest request)throws RemoteException;

	public Project deleteRelation(DeleteRelationshipRequest request)throws RemoteException;

	public Iterator<RelationshipNavigationObject> getRelationshipChain(String id, String relationId, Boolean deep) throws RemoteException;

	public Iterator<RelationshipNavigationObject> getRelationshipChain(String id, String relationId) throws RemoteException;

	
}

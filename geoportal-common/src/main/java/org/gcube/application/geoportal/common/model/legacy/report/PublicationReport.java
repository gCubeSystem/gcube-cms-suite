package org.gcube.application.geoportal.common.model.legacy.report;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.gcube.application.geoportal.common.model.legacy.Record;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.NONE)
public class PublicationReport extends ValidationReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1422004928222440165L;
	
	@Getter
	@Setter
	private Record theRecord;

	public PublicationReport(String objectName) {
		super(objectName);
	}
	
}

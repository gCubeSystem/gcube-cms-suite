package org.gcube.application.geoportal.common.model.useCaseDescriptor;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@XmlRootElement
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Field extends Document {

    public static final String TYPE="_type";
    public static final String CHILDREN="_children";
    public static final String MAX_CARDINALITY="_max";
    public static final String MIN_CARDINALITY="_min";
    public static final String LABEL="_label";

    @JsonIgnore
    public String getLabel(){
        return this.getString(LABEL);
    }

    @JsonIgnore
    public String getType(){
        return this.getString(TYPE);
    };

    @JsonIgnore
    public Boolean isLeaf(){
        List children = getChildren();
        return children == null || children.isEmpty() || children.get(0)==null;
    }

    @JsonIgnore
    public List getChildren(){
        return this.get(CHILDREN,List.class);
    }

    @JsonIgnore
    public Boolean isCollection() {
        Integer maxCard=this.getMaxCardinality();
        return (maxCard>1||maxCard<0); // Negative values for unbounded
    }

    @JsonIgnore
    public Integer getMaxCardinality(){
        return (Integer) this.getOrDefault(MAX_CARDINALITY,1);
    }

    @JsonIgnore
    public Integer getMinCardinality(){
        return (Integer) this.getOrDefault(MIN_CARDINALITY,0);
    }
    @JsonIgnore
    public Boolean isMandatory(){
        return getMinCardinality()==0;
    }

}

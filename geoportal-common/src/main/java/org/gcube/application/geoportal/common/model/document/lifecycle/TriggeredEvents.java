package org.gcube.application.geoportal.common.model.document.lifecycle;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TriggeredEvents {

    public static final String PHASE="_phase";
    public static final String LAST_INVOKED_STEP="_lastInvokedStep";
    public static final String LAST_OPERATION_STATUS="_lastOperationStatus";
    public static final String ERROR_MESSAGES="_errorMessages";
    public static final String WARNING_MESSAGES="_warningMessages";
    public static final String TRIGGERED_EVENTS="_triggeredEvents";

    private String event;
    private LifecycleInformation.Status lastOperationStatus;
    private List<String> errorMessages;
    private List<String> warningMessages;

    public void addErrorMessage(String msg){
        if(errorMessages==null)
            errorMessages=new ArrayList<>();
        errorMessages.add(msg);
    }
    public void addWarningMessage(String msg){
        if(warningMessages==null)
            warningMessages=new ArrayList<>();
        warningMessages.add(msg);
    }
}

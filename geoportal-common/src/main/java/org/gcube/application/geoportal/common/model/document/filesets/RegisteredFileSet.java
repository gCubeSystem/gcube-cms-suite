package org.gcube.application.geoportal.common.model.document.filesets;

import java.util.List;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.ToString;


@ToString (callSuper = true)
public class RegisteredFileSet extends Document {



    public static final String UUID="_uuid";
    public static final String CREATION_INFO="_creationInfo";
    public static final String ACCESS="_access";
    public static final String FOLDER_ID="_folderID";
    public static final String PAYLOADS="_payloads";
    public static final String MATERIALIZATIONS="_materializations";

    @JsonIgnore
    public Object getCreationInfo(){ return this.get(CREATION_INFO); }
    @JsonIgnore
    public Object getAccess(){ return this.get(ACCESS); }
    @JsonIgnore
    public String getFolderId(){ return super.getString(FOLDER_ID); }
    @JsonIgnore
    public List getPayloads(){return super.get(PAYLOADS,List.class);}
    @JsonIgnore
    public List getMaterializations(){return super.get(MATERIALIZATIONS,List.class);}
    @JsonIgnore
    public String getUUID(){return super.getString(UUID);}
}

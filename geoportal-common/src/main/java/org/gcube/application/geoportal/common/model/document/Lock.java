package org.gcube.application.geoportal.common.model.document;

import org.gcube.application.geoportal.common.model.document.accounting.AccountingInfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Lock {

    public static final String ID="_id";
    public static final String INFO="_info";
    public static final String OPERATION="_operation";

    @JsonProperty(INFO)
    private AccountingInfo info;
    @JsonProperty(ID)
    private String id;
    @JsonProperty(OPERATION)
    private String operation;

}

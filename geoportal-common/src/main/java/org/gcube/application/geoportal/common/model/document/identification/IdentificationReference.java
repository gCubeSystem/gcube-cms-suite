package org.gcube.application.geoportal.common.model.document.identification;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@NoArgsConstructor
public class IdentificationReference extends Document {

    public static final String TYPE="_type";

    public IdentificationReference(String type) {
        setType(type);
    }

    @JsonIgnore
    public String getType(){return super.getString(TYPE);}
    @JsonIgnore
    public void setType(String type){super.put(TYPE,type);}
}

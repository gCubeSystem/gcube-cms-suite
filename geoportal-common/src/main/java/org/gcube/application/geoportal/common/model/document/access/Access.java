package org.gcube.application.geoportal.common.model.document.access;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Access {

    public static final String POLICY="_policy";
    public static final String LICENSE="_license";

    @JsonProperty(POLICY)
    private AccessPolicy policy;
    @JsonProperty(LICENSE)
    private String license;

}

package org.gcube.application.geoportal.common.model.document.filesets;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.ToString;

@ToString (callSuper = true)
public class Materialization extends Document {

    public static final String TYPE ="_type";

    @JsonIgnore
    public String getType(){return super.getString(TYPE);}

    public Materialization(){super();}
    @JsonIgnore
    public Materialization (String type){
        this.put(TYPE,type);
    }
}

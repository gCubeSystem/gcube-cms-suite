package org.gcube.application.geoportal.common.model.document.accounting;

import org.gcube.application.geoportal.common.model.document.access.Access;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class PublicationInfo {

    public static final String CREATION_INFO="_creationInfo";
    public static final String LAST_EDIT_INFO="_lastEditInfo";
    public static final String ACCESS = "_access";

    @JsonProperty(CREATION_INFO)
    private AccountingInfo creationInfo;
    @JsonProperty(LAST_EDIT_INFO)
    private AccountingInfo lastEditInfo;
    @JsonProperty(ACCESS)
    private Access access;

}

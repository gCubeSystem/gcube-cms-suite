package org.gcube.application.geoportal.common.model.legacy;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public abstract class SDILayerDescriptor extends AssociatedContent{


	public abstract Long getLayerID();
	public abstract void setLayerID(Long layerID);
	public abstract String getLayerUUID();
	public abstract void setLayerUUID(String layerUUID);
	public abstract String getLayerName();
	public abstract void setLayerName(String layerName);
	public abstract String getWmsLink();
	public abstract void setWmsLink(String wmsLink);
	
	public abstract void setWorkspace(String workspace);
	public abstract String getWorkspace();
	public abstract BBOX getBbox();
	public abstract void setBbox(BBOX toSet);
}

package org.gcube.application.geoportal.common.model.document.filesets.sdi;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class GeoServerPlatform extends PlatformInfo{


    public static final String GS_PLATFORM="Geoserver";
    public static final String WORKSPACE= "workspace";
    public static final String LAYER_NAME= "layerName";
    public static final String PERSISTENCE_PATH = "persistencePath";
    public static final String FILES="files";
    public static final String STORENAME="storeName";

    @JsonIgnore
    public String getWorkspace(){return this.getString(WORKSPACE);}
    @JsonIgnore
    public String getLayerName(){return this.getString(LAYER_NAME);}
    @JsonIgnore
    public String getPersistencePath(){return this.getString(PERSISTENCE_PATH);}
    @JsonIgnore
    public List getFiles(){return this.get(FILES,List.class);}
    @JsonIgnore
    public String getStoreName(){return this.getString(STORENAME);}

}

package org.gcube.application.geoportal.common.model.document.filesets;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RegisteredFile {

    public static final String MIMETYPE="_mimetype";
    public static final String STORAGE_ID="_storageID";
    public static final String LINK="_link";
    public static final String NAME="_name";

    @JsonProperty(MIMETYPE)
    private String mimetype;
    @JsonProperty(STORAGE_ID)
    private String storageID;
    @JsonProperty(LINK)
    private String link;
    @JsonProperty(NAME)
    private String name;
}

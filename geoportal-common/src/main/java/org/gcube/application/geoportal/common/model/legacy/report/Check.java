package org.gcube.application.geoportal.common.model.legacy.report;

import java.util.Collection;

public interface Check{
	
	public static Check collectionSizeMax(final int upperBound) {
		return new Check() {
			int size=0;
			@Override
			public boolean isOk(Object toCheck) {
				int size=((Collection)toCheck).size();
				return size<=upperBound;
			}
			@Override
			public String getMessage() {
				return "Numero elementi : "+size+" [max : "+upperBound+"]";
			}
		};
	}
	
	public static Check collectionSizeMin(final int lowerBound) {
		return new Check() {
			int size=0;
			@Override
			public boolean isOk(Object toCheck) {
				int size=((Collection)toCheck).size();
				return size>=lowerBound;
			}
			@Override
			public String getMessage() {
				return "Numero elementi : "+size+" [min : "+lowerBound+"]";
			}
		};
	}
	
	public static Check collectionSize(final int lowerBound, final int upperBound) {
		return new Check() {
			int size=0;
			
			@Override
			public boolean isOk(Object toCheck) {
				size=((Collection)toCheck).size();
				return size<=upperBound&&size>=lowerBound;
			}
			
			@Override
			public String getMessage() {
				return "Numero elementi : "+size+" [min : "+lowerBound+" max : "+upperBound+"]";
			}
		};
	}
	
	public boolean isOk(Object toCheck);
	public String getMessage();
}
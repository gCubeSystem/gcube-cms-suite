package org.gcube.application.geoportal.common.model.legacy.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ValidationReport implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -3629142756787381094L;


	public static enum ValidationStatus{
		PASSED, ERROR, WARNING
	}


	private String objectName;


	private List<String> errorMessages=new ArrayList<String>();
	private List<String> warningMessages=new ArrayList<String>();
	private List<ValidationReport> children=new ArrayList<ValidationReport>();


	public ValidationStatus getStatus() {
		if(!errorMessages.isEmpty()) return ValidationStatus.ERROR;

		boolean isWarning=(!warningMessages.isEmpty());
		for(ValidationReport obj : children) {
			ValidationStatus status=obj.getStatus();
			if(status.equals(ValidationStatus.ERROR)) return ValidationStatus.ERROR;
			if(status.equals(ValidationStatus.WARNING)) isWarning=true;
		}
		if(isWarning) return ValidationStatus.WARNING;
		return ValidationStatus.PASSED;			
	}

	public void addChild(ValidationReport obj) {
		children.add(obj);
	}

	public void addMessage(ValidationStatus status,String message) {
		switch (status) {
		case ERROR:
			errorMessages.add(message);
			break;
		case WARNING :
			warningMessages.add(message);
		default:
			break;
		}
	}

	
	
	
	public <T> boolean checkMandatory(T toCheck,String name,Check...checks) {
		ConstraintCheck<T> check=new ConstraintCheck<T>(toCheck,name,checks);
		if(check.isError())
			this.addMessage(ValidationStatus.ERROR, check.getMessage());
		return !check.isError();			
	}


	public ValidationReport(String objectName) {
		super();
		this.objectName = objectName;
	}


}

package org.gcube.application.geoportal.common.model.useCaseDescriptor;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HandlerDeclaration {

    public static final String ID="_id";
    public static final String TYPE="_type";
    public static final String CONFIGURATION="_configuration";

    @JsonProperty(ID)
    private String id;
    @JsonProperty(TYPE)
    private String type;
    @JsonProperty(CONFIGURATION)
    private Document configuration;
}

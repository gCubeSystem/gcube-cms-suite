package org.gcube.application.geoportal.common.model.legacy;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @Type(value = GeoServerContent.class),
    @Type(value = WorkspaceContent.class),
    })
public abstract class PersistedContent {

	//Generic Info
	private long id;	


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersistedContent [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}

package org.gcube.application.geoportal.common.model.legacy;

public enum AccessPolicy {

	OPEN,RESTRICTED,EMBARGOED;
	
}

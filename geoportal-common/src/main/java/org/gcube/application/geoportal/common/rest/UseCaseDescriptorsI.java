package org.gcube.application.geoportal.common.rest;

import java.rmi.RemoteException;
import java.util.Iterator;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;


public interface UseCaseDescriptorsI {

    public UseCaseDescriptor create(Document toCreate)throws RemoteException;

    public Iterator<UseCaseDescriptor> query(QueryRequest request) throws Exception;

    public void deleteById(String id,boolean force)throws RemoteException;

    public UseCaseDescriptor update(String ID, Document toSet)throws RemoteException;

    public UseCaseDescriptor getById(String id) throws Exception;
}

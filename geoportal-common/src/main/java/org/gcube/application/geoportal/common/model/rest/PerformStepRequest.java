package org.gcube.application.geoportal.common.model.rest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.bson.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerformStepRequest {

    private String stepID;
    @XmlAttribute(required = false)
    private String message;
    private Document options;
}

package org.gcube.application.geoportal.common.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateRelationshipRequest {

    private String projectId;
    private String relationshipId;
    private String targetId;
    private String targetUCD;

}

package org.gcube.application.geoportal.common.model.useCaseDescriptor;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RelationshipDefinition {

    public static final String ID="_id";
    public static final String LABEL="_label";
    public static final String REVERSE_RELATION_ID="_reverseRelationId";

    @JsonProperty(ID)
    private String id;
    @JsonProperty(LABEL)
    private String label;
    @JsonProperty(REVERSE_RELATION_ID)
    private String reverseRelationId;

}

package org.gcube.application.geoportal.common.model.configuration;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Archive extends Document {
    public static final String TYPE = "_type";

    @JsonIgnore
    public String getType() {
        return super.getString(TYPE);
    }

    public Archive() {
        super();
    }

    @JsonIgnore
    public Archive(String type) {
        this.put(TYPE, type);
    }
}

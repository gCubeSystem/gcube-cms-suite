package org.gcube.application.geoportal.common.model.document.accounting;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AccountingInfo {

    public static final String USER="_user";
    public static final String CONTEXT="_context";
    public static final String INSTANT="_instant";

    @JsonProperty(USER)
    private User user;
    @JsonProperty(CONTEXT)
    private Context context;
    @JsonProperty(INSTANT)
    private LocalDateTime instant;
}

package org.gcube.application.geoportal.common.utils.tests;

public class GCubeTest {

	public static String getContext() {
		String testContext = null;
		testContext = System.getProperty("testContext");
		if (testContext == null) {
			// trying with env
			testContext = System.getenv("testContext");
		}

//            InterfaceConstants.SERVICE_CLASS="Application";
//            InterfaceConstants.SERVICE_NAME="GeoPortal";

		//testContext = "/pred4s/preprod/preVRE";
		//testContext = "/pred4s/preprod/preVRE_ext";
		// testContext = "/d4science.research-infrastructures.eu/D4OS/GNA";
		testContext= "/gcube/devsec/devVRE";

		System.out.println("TEST CONTEXT = " + testContext);
		return testContext;
	}

	public static boolean isTestInfrastructureEnabled() {
		return getContext() != null;
	}

}

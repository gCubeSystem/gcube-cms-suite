package org.gcube.application.geoportal.common.model.document.relationships;

import java.util.List;

import org.gcube.application.geoportal.common.model.document.Project;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class RelationshipNavigationObject {
    public static final String CHILDREN="_children";
    public static final String TARGET="_target";

    @JsonProperty(CHILDREN)
    List<RelationshipNavigationObject> children;

    @NonNull
    @JsonProperty(TARGET)
    Project target;
}

package org.gcube.application.geoportal.common.model.legacy;

import java.util.List;

import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;
import org.gcube.application.geoportal.common.utils.CollectionsUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
public class RelazioneScavo extends AssociatedContent {

	


	private List<String> responsabili;
	private List<String> soggetto;

	
	@Override
	public ValidationReport validateForInsertion() {
		
		ValidationReport toReturn=super.validateForInsertion();
		toReturn.setObjectName("Relazione Scavo");
		toReturn.checkMandatory(responsabili, "Responsabili");
		toReturn.checkMandatory(soggetto, "Soggetto");
		return toReturn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + CollectionsUtils.hashCode(responsabili);
		result = prime * result + CollectionsUtils.hashCode(soggetto);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelazioneScavo other = (RelazioneScavo) obj;

		if(!CollectionsUtils.equalsCollections(responsabili, other.responsabili)) return false;

		if(!CollectionsUtils.equalsCollections(soggetto, other.soggetto)) return false;

		return true;
	}
	
}

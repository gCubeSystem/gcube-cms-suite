package org.gcube.application.geoportal.common.model.document.filesets.sdi;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class PlatformInfo extends Document {

    public static final String TYPE="_type";
    public static final String HOST="_host";
    public static final String ENGINE_VERSION = "_engineVersion";

    @JsonIgnore
    public String getType(){ return this.getString(TYPE); }
    @JsonIgnore
    public String getHost(){ return this.getString(HOST); }
    @JsonIgnore
    public String getEngineVersion(){ return this.getString(ENGINE_VERSION); }


}


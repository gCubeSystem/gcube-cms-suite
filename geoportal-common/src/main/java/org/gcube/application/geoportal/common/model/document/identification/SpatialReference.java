package org.gcube.application.geoportal.common.model.document.identification;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString(callSuper = true)
public class SpatialReference extends IdentificationReference{

    public static final String SPATIAL_REFERENCE_TYPE="SPATIAL REFERENCE";
    public static final String GEO_JSON="geoJSON";


    public SpatialReference(Object geoJSON) {
        setGeoJson(geoJSON);
        setType(SPATIAL_REFERENCE_TYPE);
    }

    @JsonIgnore
    public Object getGeoJson(){return super.get(GEO_JSON);}
    @JsonIgnore
    public Object setGeoJson(Object geoJSON){return super.put(GEO_JSON,geoJSON);}
}

package org.gcube.application.geoportal.common.model.rest;

import java.util.List;

import org.bson.Document;

import lombok.Data;

@Data
public class QueryRequest {

    @Data
    public static class PagedRequest{
        private int offset;
        private int Limit;
    }

    @Data
    public static class OrderedRequest {
        public static enum Direction {ASCENDING,DESCENDING}
        private Direction direction;
        private String json;
        private List<String> fields;
    }
    private Document filter;
    private Document projection;
    private OrderedRequest ordering;
    private PagedRequest paging;
}

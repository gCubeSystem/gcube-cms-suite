package org.gcube.application.geoportal.common.model.configuration;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class MongoConnection {

	private String user;
	private String password;
	private String database;
	private List<String> hosts=new ArrayList<String>();
	private int port;

	@Override
	public String toString() {
		return "MongoConnection{" +
				"user='" + user + '\'' +
				", password=***" +
				", database='" + database + '\'' +
				", hosts=" + hosts +
				", port=" + port +
				'}';
	}
}

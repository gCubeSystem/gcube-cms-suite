package org.gcube.application.geoportal.common.model.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.Document;
import org.gcube.application.geoportal.common.faults.InvalidRequestException;
import org.gcube.application.geoportal.common.model.legacy.Concessione;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
@Deprecated
public class AddSectionToConcessioneRequest {

	private String destinationPath;
	private List<TempFile> streams;
	private Document attributes;

	public void validate()throws InvalidRequestException {
		Concessione.Paths.validate(destinationPath);
		if(streams==null || streams.isEmpty()) throw new InvalidRequestException("No Temp File declared");
		for(TempFile t : streams) t.validate();
	}
}

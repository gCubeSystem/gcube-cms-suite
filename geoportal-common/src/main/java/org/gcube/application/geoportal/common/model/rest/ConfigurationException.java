package org.gcube.application.geoportal.common.model.rest;

public class ConfigurationException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -3810929853461018566L;

    public ConfigurationException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ConfigurationException(String message, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public ConfigurationException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ConfigurationException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }



}
package org.gcube.application.geoportal.common.model.configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Configuration{

    public static final String PROFILE_ID="profile_id";
    public static final String CONTEXT = "context";
    public static final String LAST_UPDATED_TIME = "last_updated_time";

    public static final String ERROR_MESSAGES="errorMessages";
    public static final String WARNING_MESSAGES="warningMessages";
    public static final String STATUS="status";


    public static final String INDEXES = "indexes";
    public static final String ARCHIVES = "archives";

    public static enum Status{
        OK,ERROR,WARNING
    }


    @JsonProperty(PROFILE_ID)
    private String profileId;
    @JsonProperty(CONTEXT)
    private String context;
    @JsonProperty(LAST_UPDATED_TIME)
    private LocalDateTime lastUpdatedTime;

    @JsonProperty(INDEXES)
    private List<Index> indexes;
    @JsonProperty(ARCHIVES)
    private List<Archive> archives;

    @JsonProperty(ERROR_MESSAGES)
    private List<String> errorMessages;
    @JsonProperty(WARNING_MESSAGES)
    private List<String> warningMessages;

    @JsonProperty(STATUS)
    private Status status=Status.OK;


    @JsonIgnore
    public Configuration addErrorMessage(String msg){
        status = Status.ERROR;
        if(errorMessages==null)
            errorMessages=new ArrayList<>();
        errorMessages.add(msg);
        return this;
    }
    @JsonIgnore
    public Configuration addWarningMessage(String msg){
        if(status == null || status.equals(Status.OK))
            status = Status.WARNING;
        if(warningMessages==null)
            warningMessages=new ArrayList<>();
        warningMessages.add(msg);
        return this;
    }
}

package org.gcube.application.geoportal.common.rest;

public class InterfaceConstants {

	public static final String NAMESPACE="http://gcube-system.org/namespaces/data/sdi-service";

	public static final String APPLICATION_BASE_PATH="geoportal-service";
	public static final String APPLICATION_PATH="/srv";
	public static String SERVICE_CLASS="org.gcube.application";
	public static String SERVICE_NAME="geoportal-service";

	public static final class Methods{
		public static final String UCD ="ucd";
		public static final String PROJECTS="projects";
		public static final String PLUGINS="plugins";


		public static final String CONCESSIONI="concessioni";
		public static final String MONGO_CONCESSIONI="mongo-concessioni";
		
		
		public static final String PUBLISH_PATH="publish";
		public static final String REGISTER_FILES_PATH="registerFiles";
		public static final String DELETE_FILES_PATH="deleteFiles";
		public static final String CONFIGURATION_PATH="configuration";
		public static final String SEARCH_PATH="search";
		public static final String QUERY_PATH="query";

		public static final String STEP="step";

		public static final String RELATIONSHIP="relationship";
		public static final String FORCE_UNLOCK="forceUnlock";
		public static final String SET_PROJECT_ACCESS_POLICY="setAccess";

	}

	public static final class Parameters{
		public static final String PROJECT_ID="project_id";
		public static final String SECTION_ID="section_id";
		public static final String UCID ="usecase_id";

		public static final String FORCE="force";
		
		public static final String IGNORE_ERRORS="ignore_errors";

		public static final String RELATIONSHIP_ID="relationship_id";
		public static final String DEEP="deep";
		public static final String TARGET_UCD="target_ucd";
		public static final String TARGET_ID="target_id";
		public static final String PATH="path";
	}


}

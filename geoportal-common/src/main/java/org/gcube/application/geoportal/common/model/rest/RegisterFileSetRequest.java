package org.gcube.application.geoportal.common.model.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.Document;
import org.gcube.application.geoportal.common.faults.InvalidRequestException;
import org.gcube.application.geoportal.common.model.document.access.Access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterFileSetRequest {

    /** Behavior for existing JSON Object at @destinationPath
     *
     *  REPLACE_EXISTING : removes previously defined attributes (only on single match)
     *  MERGE_EXISTING : merges the provided attributes with the matching ones (only on single match)
     *  APPEND : appends the object to the existing collection (only if field is multiple)
     */
    public static enum ClashOptions {
        REPLACE_EXISTING,MERGE_EXISTING, APPEND
    }

    private String fieldDefinitionPath;
    private String parentPath;
    private String fieldName;
    private List<TempFile> streams;
    private Document attributes;

    // Default is project's access
    private Access toSetAccess;

    private ClashOptions clashOption;

    public void validate()throws InvalidRequestException {
        if(streams==null || streams.isEmpty()) throw new InvalidRequestException("No Temp File declared");
        for(TempFile t : streams) t.validate();
    }
}

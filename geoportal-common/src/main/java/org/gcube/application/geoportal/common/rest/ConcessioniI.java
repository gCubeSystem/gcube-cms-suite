package org.gcube.application.geoportal.common.rest;

public interface ConcessioniI {

	
	public String create(String toCreate) throws Exception;
	public String readById(String id) throws Exception;
	public String getAll() throws Exception;
	public String addSection(String id,String updated) throws Exception;
}

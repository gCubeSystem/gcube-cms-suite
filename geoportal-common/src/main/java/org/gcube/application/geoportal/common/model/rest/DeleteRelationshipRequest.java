package org.gcube.application.geoportal.common.model.rest;

public class DeleteRelationshipRequest extends CreateRelationshipRequest{

    public DeleteRelationshipRequest(String projectId, String relationshipId, String targetId, String targetUCD) {
        super(projectId, relationshipId, targetId, targetUCD);
    }

    public DeleteRelationshipRequest() {
    }
}

package org.gcube.application.geoportal.common.model.rest;

import org.gcube.application.geoportal.common.faults.InvalidRequestException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TempFile {

	private String id;
	private String url;
	private String filename;

	// Added by Francesco, see #28150
	private Long size;

	public void validate() throws InvalidRequestException {
		if ((id == null || id.isEmpty()) && (url == null || url.isEmpty()))
			throw new InvalidRequestException("Invalid temp file " + this + " : ID null or empty and no url defined");
		if (filename == null || filename.isEmpty())
			throw new InvalidRequestException("Invalid temp file " + this + " : filename null or empty");
	}

	public TempFile(String id, String url, String filename) {
		this.id = id;
		this.url = url;
		this.filename = filename;
	}

}

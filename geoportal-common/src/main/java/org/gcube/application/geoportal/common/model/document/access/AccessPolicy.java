package org.gcube.application.geoportal.common.model.document.access;

public enum AccessPolicy {

	OPEN,RESTRICTED,EMBARGOED;
	
}

package org.gcube.application.geoportal.common.model.document.lifecycle;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LifecycleInformation {

    public static final class CommonPhases{
        public static final String DRAFT_PHASE="DRAFT";
    }

    //COMMON PHASES


    public static final String PHASE="_phase";
    public static final String LAST_INVOKED_STEP="_lastInvokedStep";
    public static final String LAST_OPERATION_STATUS="_lastOperationStatus";
    public static final String ERROR_MESSAGES="_errorMessages";
    public static final String WARNING_MESSAGES="_warningMessages";
    public static final String TRIGGERED_EVENTS="_triggeredEvents";
    public static final String NOTES="_notes";

    public static enum Status{
        OK,ERROR,WARNING
    }

    @JsonProperty(PHASE)
    private String phase;
    @JsonProperty(LAST_INVOKED_STEP)
    private String lastInvokedStep;
    @JsonProperty(LAST_OPERATION_STATUS)
    private Status lastOperationStatus;
    @JsonProperty(ERROR_MESSAGES)
    private List<String> errorMessages;
    @JsonProperty(WARNING_MESSAGES)
    private List<String> warningMessages;
    @JsonProperty(TRIGGERED_EVENTS)
    private List<TriggeredEvents> triggeredEvents;
    @JsonProperty(NOTES)
    private String notes;


    @JsonIgnore
    public TriggeredEvents getLastEvent(){
        if(triggeredEvents==null || triggeredEvents.isEmpty()) return null;
        return triggeredEvents.get(triggeredEvents.size()-1);
    }

    @JsonIgnore
    public LifecycleInformation addErrorMessage(String msg){
        if(errorMessages==null)
            errorMessages=new ArrayList<>();
        errorMessages.add(msg);
        return this;
    }
    @JsonIgnore
    public LifecycleInformation addWarningMessage(String msg){
        if(warningMessages==null)
            warningMessages=new ArrayList<>();
        warningMessages.add(msg);
        return this;
    }
    @JsonIgnore
    public LifecycleInformation addEventReport(TriggeredEvents info){
        if(triggeredEvents==null) triggeredEvents=new ArrayList<>();
        triggeredEvents.add(info);
        return this;
    }
    @JsonIgnore
    public LifecycleInformation cleanState(){
        setLastOperationStatus(null);
        setLastInvokedStep(null);
        setTriggeredEvents(new ArrayList<>());
        setErrorMessages(new ArrayList<>());
        setWarningMessages(new ArrayList<>());
        return this;
    }
}

package org.gcube.application.geoportal.common.model.document.identification;

import lombok.ToString;

@ToString (callSuper = true)
public class TemporalReference extends IdentificationReference {

    public static final String TEMPORAL_REFERENCE_TYPE="TEMPORAL_REFERENCE_TYPE";
}

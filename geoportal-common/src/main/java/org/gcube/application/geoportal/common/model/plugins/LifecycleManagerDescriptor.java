package org.gcube.application.geoportal.common.model.plugins;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@Slf4j
public class LifecycleManagerDescriptor extends PluginDescriptor{

    public static final String LIFECYCLE_MANAGER_TYPE="LifecycleManagement";

    public LifecycleManagerDescriptor(String id) {
        super(id,LIFECYCLE_MANAGER_TYPE);
    }

    private Map<String, OperationDescriptor> supportedSteps;
    private Map<String, OperationDescriptor> supportedEvents;

}

package org.gcube.application.geoportal.common.model.configuration;

import org.bson.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Index extends Document {

    public static class KnownTypes{
        public static final String GIS_CENTROIDS="GIS_CENTROIDS";
    }


    public static final String TYPE = "_type";

    @JsonIgnore
    public String getType() {
        return super.getString(TYPE);
    }

    public Index() {
        super();
    }

    @JsonIgnore
    public Index(String type) {
        this.put(TYPE, type);
    }
}

package org.gcube.application.geoportal.common.model.document.accounting;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Context {
    public static final String ID="_id";
    public static final String NAME = "_name";

    @JsonProperty(ID)
    private String id;
    @JsonProperty(NAME)
    private String name;
}

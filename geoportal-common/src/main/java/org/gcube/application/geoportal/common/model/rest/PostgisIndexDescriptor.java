package org.gcube.application.geoportal.common.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class PostgisIndexDescriptor {

    public DatabaseConnection postgisDBIndex;
    public String wmsLink;
}

# Changelog for org.gcube.application.geoportal-common

## [v1.1.1] - 2024-10-01
- Included the file size to reduce/optimize the time to upload files to the storage hub [#28150]

## [v1.1.0] - 2024-04-08
- Added message to StepExecutionRequest [#27192]
- Fixed pom. Duplicate declaration of `registry-publisher` dependency

## [v1.0.13] - 2023-09-06
- Using parent range version [#25572]

## [v1.0.12] - 2023-05-11
- Integrated the PATCH method [#24985]

## [v1.0.11] - 2023-01-10
- Pom updates

## [v1.0.10] - 2022-12-07
- Pom updates

# [v1.0.9] - 2022-01-17
- Minor fixes in model
- Schema and jsonPath support
- Fixes [#22722](https://support.d4science.org/issues/22722)

# [v1.0.8] - 2021-11-10
- Fixes [#22369](https://support.d4science.org/issues/22369)
- Fixes [#22338](https://support.d4science.org/issues/22338)
- Profiled Documents
- Changed group id 


# [v1.0.7] - 2021-09-20
- Refactored repositories

# [v1.0.6-SNAPSHOT] - 2021-08-3
- Forced Deletion
- Search & query
- Clean Fileset
- Interfaces return iterator instead of iterable
- Name in WorkspaceContent (https://support.d4science.org/issues/22032)
- Section AbstractRelazione (https://support.d4science.org/issues/22031)

# [v1.0.5] - 2020-12-9
- Mongo Id in record
- Mongo Concessioni interface 
- Added Files.fixFileNAme
- ValidationReport as field
- Updated Model (#20357)


# [v1.0.4-SNAPSHOT] - 2020-12-9
- Projects Rest Interface
- TempFile support

# [v1.0.3] - 2020-12-4
- Project model update

# [v1.0.2-SNAPSHOT] - 2020-12-4
- Model update

## [v1.0.1] - 2020-11-11
- Model update

## [v1.0.0] - 2020-11-11
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.cms.default-lc-managers

## [v1.3.0]- 2024-03-22
- Updated visibility of phases and steps

## [v1.2.2]- 2023-09-06
- Using parent version range [#25572]

## [v1.2.1]

- Improved some logs

## [v1.2.0]

- Integrated the field 'geov_link' (Geoportal GisViewer link) in the centroid layer [#24859]
- Fixed Draft execution step executing dematerialization and deIndexing actions [#24877]

## [v1.1.1] - 2023-03-06

- [#24570] Integrated the UnPublish operation 

## [v1.0.1] - 2023-01-10
- Pom updates

## [v1.0.0] - 2022-02-24
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

package org.gcube.application.cms.plugins.implementations.executions;

import org.gcube.application.cms.plugins.faults.InsufficientPrivileges;
import org.gcube.application.cms.plugins.faults.StepException;
import org.gcube.application.cms.plugins.implementations.RoleManager;
import org.gcube.application.cms.plugins.reports.StepExecutionReport;
import org.gcube.application.cms.plugins.requests.StepExecutionRequest;
import org.gcube.application.geoportal.common.model.plugins.OperationDescriptor;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

import lombok.NonNull;

public abstract class GuardedStepExecution extends GuardedExecution<StepExecutionRequest, StepExecutionReport>{


    public GuardedStepExecution(@NonNull OperationDescriptor op) {
        super(op);
    }

    @Override
    protected void checks() throws ConfigurationException, InsufficientPrivileges, StepException {
        super.checks();
        RoleManager r = new RoleManager(config);
        if(!r.canInvokeStep(theReport.getTheRequest().getStep(),theReport.getTheRequest().getCaller()))
            throw new InsufficientPrivileges("User is not allowed to execute "+theReport.getTheRequest().getStep());

    }
}

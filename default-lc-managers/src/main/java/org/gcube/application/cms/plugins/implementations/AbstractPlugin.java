package org.gcube.application.cms.plugins.implementations;

import java.util.List;
import java.util.Map;

import org.gcube.application.cms.plugins.Plugin;
import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

public abstract class AbstractPlugin implements Plugin {





    protected HandlerDeclaration getConfigurationFromProfile(UseCaseDescriptor useCaseDescriptor) throws InvalidProfileException{
          return  getMultipleDeclarationsFromProfile(useCaseDescriptor).get(0);
    }

    protected List<HandlerDeclaration> getMultipleDeclarationsFromProfile(UseCaseDescriptor p)throws InvalidProfileException {
        Map<String,List<HandlerDeclaration>> map = p.getHandlersMapByID();
        if(map.containsKey(getDescriptor().getId()))
             return map.get(getDescriptor().getId());
        else throw new InvalidProfileException("No Configuration found for "+getDescriptor().getId()+" in "+p.getId());
    }



}

package org.gcube.application.cms.plugins.implementations.executions;

import org.gcube.application.cms.plugins.reports.EventExecutionReport;
import org.gcube.application.cms.plugins.requests.EventExecutionRequest;
import org.gcube.application.geoportal.common.model.plugins.OperationDescriptor;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GuardedEventManager extends GuardedExecution<EventExecutionRequest,EventExecutionReport>{


    public GuardedEventManager(@NonNull OperationDescriptor op) {
        super(op);
    }
}

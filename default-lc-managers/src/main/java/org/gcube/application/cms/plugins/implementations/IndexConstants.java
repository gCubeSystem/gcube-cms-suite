package org.gcube.application.cms.plugins.implementations;

/**
 * The Class IndexConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Mar 29, 2023
 */
public class IndexConstants {
	
    public static final String INDEX_PARAMETER_FLAGINTERNALINDEX = "flagInternalIndex";
	public static final String INDEX_PARAMETER_INDEXNAME = "indexName";
	public static final String INDEX_PARAMETER_WORKSPACE = "workspace";

}

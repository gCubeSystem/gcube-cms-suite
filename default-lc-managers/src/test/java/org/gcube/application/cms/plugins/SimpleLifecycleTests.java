package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.faults.StepException;
import org.gcube.application.cms.plugins.implementations.Default3PhaseManager;
import org.gcube.application.geoportal.common.model.plugins.OperationDescriptor;

public class SimpleLifecycleTests {

	// NB cannot use test commons, need to separate modules
//    extends
// BasicPluginTest {
//
//    @Test
//    public void testFullCycle(){
//        SimpleLifeCycleManager manager = plugins.get(SimpleLifeCycleManager.PLUGIN_ID);
//
//        PerformStepRequest request = new PerformStepRequest();
//
//        Project p = null;
//
//        EventExecutionRequest eventRequest = new EventExecutionRequest();
//
//        manager.onEvent()
//
//        manager.performStep()
//    }

	public static void main(String[] args) {

		System.out.println("Check start");
		OperationDescriptor op = Default3PhaseManager.STEPS.APPROVE;

		try {
			// Check document phase
			if (op.getAppliableToPhases() != null && !op.getAppliableToPhases().isEmpty()) {
				String currentPhase = "Pending Approval";
				if (!op.getAppliableToPhases().contains(currentPhase)) {
					System.out.println("check KO");
					throw new StepException(
							"Document must be in one of the following phases : " + op.getAppliableToPhases());
				}
			}
			System.out.println("check OK");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

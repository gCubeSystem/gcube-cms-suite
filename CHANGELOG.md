This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.gcube-sdi-suite

## [v1.0.6]

- Integrated an EventManager centrally [#26321]
- sdi-plugins: add the logic to apply a regex to the value that must be added to index [#26322]
- notifications-plugins: integrated [#26453]
- catalogue-binding-plugins: integrated [#26454]
- Moved to `gcube-smartgears-bom 2.5.1{-SNAPSHOT}` [#28026]

## [v1.0.5]

- Added maven profiles `geoportal-release-profile` and `geoportal-snapshot-profile` [#25570]
- Moved to maven-parent.v1.2.0 [#25570]

## [v1.0.4] - 2023-01-10

- Updated plugin framework

## [v1.0.3] - 2022-12-07

- Updated plugin framework
- Introduced module default-lc-managers

## [v1.0.2] - 2021-02-24

-Introduced module sdi-plugins
-Introduced module notifications-plugins
-Introduced module dataminer-plugins
-Introduced module images-plugins
-Introduced module ckan-plugin

## [v1.0.1] - 2021-12-07

- Introduced cms-plugin-framework
- Introduced concessioni use case
- Fixed internal group ids

## [v1.0.0] - 2021-2-11
- First release

package org.gcube.application.cms.commons.model;

import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.model.BasicTests;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.junit.Test;

import java.util.Collections;

import static junit.framework.TestCase.assertNotNull;

public class RolesTesting extends BasicTests {



    public UseCaseDescriptor getUcd() {
        return TestProfiles.profiles.get("profiledConcessioni");
    }

    @Test
    public void checkRoles(){
        User u = getCurrentUser();
        UseCaseDescriptor ucd = getUcd();
        u.setRoles(Collections.emptySet());
        assertNotNull(ucd.getMatching(u));

        u.setRoles(Collections.singleton("FakeUser"));
        assertNotNull(ucd.getMatching(u));
        u.setRoles(Collections.singleton("FakeAdmin"));
        assertNotNull(ucd.getMatching(u));
        u.setRoles(Collections.singleton("FakeEditor"));
        assertNotNull(ucd.getMatching(u));
        u.setRoles(Collections.singleton("FakeMember"));
        assertNotNull(ucd.getMatching(u));


    }

}

package org.gcube.application.cms.tests;

import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

public class TestProfiles {


    public static File BASE_FOLDER =new File("../test-data/profiledDocuments/profiles");
    public static final HashMap<String, UseCaseDescriptor> profiles =new HashMap<>();

    static{

        for(File f:BASE_FOLDER.listFiles()){
            try {
                UseCaseDescriptor p =Serialization.read(
                        Files.readFileAsString(f.getAbsolutePath(), Charset.defaultCharset()), UseCaseDescriptor.class);
                profiles.put(p.getId(), p);
            } catch (IOException e) {
                throw new RuntimeException("Unable to read "+f.getAbsolutePath(),e);
            }
        }
    }
}

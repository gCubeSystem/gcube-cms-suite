package org.gcube.application.cms.tests;

import org.bson.Document;
import org.gcube.application.geoportal.common.utils.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

public class TestSchemas {



    public static File BASE_FOLDER =new File("../test-data/profiledDocuments/schemas");
    public static final HashMap<String, Document> schemas =new HashMap<>();

    static{

        for(File f:BASE_FOLDER.listFiles()){
            try {
                schemas.put(f.getName(), Document.parse(Files.readFileAsString(f.getAbsolutePath(), Charset.defaultCharset())));
            } catch (IOException e) {
                throw new RuntimeException("Unable to read "+f.getAbsolutePath(),e);
            }
        }
    }
}

package org.gcube.application.cms.tests.plugins;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.caches.Engine;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.cms.plugins.Plugin;
import org.gcube.application.cms.plugins.PluginsReflections;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.cms.tests.model.BasicTests;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Assert;
import org.junit.BeforeClass;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class BasicPluginTest extends BasicTests {



    protected Context getTestContext(){
        Context toReturn = new Context();
        String contextId =GCubeTest.getContext();
        toReturn.setId(contextId);
        toReturn.setName(contextId.substring(contextId.lastIndexOf("/")));
        return toReturn;
    }

    protected static Map<String,Plugin> plugins=new HashMap<>();

    @BeforeClass
    public static void checkPluginRegistration() {
        plugins.putAll(PluginsReflections.load());
        plugins.forEach((s,p) -> System.out.println(s+" "+p.getDescriptor()));

        Assert.assertFalse(plugins.isEmpty());

        plugins.forEach(PluginCheks.descriptor);
        plugins.forEach(PluginCheks.init);
        System.out.println("Plugin Loading OK");


        ImplementationProvider.get().setEngine(new Engine<StorageUtils>() {
            @Override
            public void init() {}

            @Override
            public void shutdown() {}

            @Override
            public StorageUtils getObject() throws ConfigurationException {
                TokenSetter.set(GCubeTest.getContext());
                return new StorageUtils();
            }

        },StorageUtils.class);

    }



}

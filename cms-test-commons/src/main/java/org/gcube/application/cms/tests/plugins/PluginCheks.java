package org.gcube.application.cms.tests.plugins;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.plugins.*;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.model.plugins.IndexerPluginDescriptor;
import org.gcube.application.geoportal.common.model.plugins.LifecycleManagerDescriptor;
import org.gcube.application.geoportal.common.model.plugins.MaterializerPluginDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Assert;

import java.util.function.BiConsumer;

import static junit.framework.TestCase.assertTrue;

@Slf4j
public class PluginCheks {

    static BiConsumer<String, Plugin> init= (s,p)->{
            if(p instanceof InitializablePlugin){
                log.info("INIT Plugin "+p.getDescriptor());
                InitializablePlugin ip=(InitializablePlugin)p;
                try {
                    ip.init().validate();

                    if(GCubeTest.isTestInfrastructureEnabled()){
                        log.info("INIT Plugin "+p.getDescriptor()+" under "+GCubeTest.getContext());
                        TokenSetter.set(GCubeTest.getContext());
                        ip.initInContext().validate();
                    }
                } catch (Exception e) {
                    log.error("Unable to init {} ",p,e);
                    Assert.fail("Unable to Init "+p.getDescriptor().getId());
                }
            }
    };

    static BiConsumer<String, Plugin> descriptor= (s,p)->{
        log.info("Checking Plugin Descriptor "+p.getClass());
        Assert.assertNotNull(p.getDescriptor());
        Assert.assertNotNull(p.getDescriptor().getId());
        Assert.assertNotNull(p.getDescriptor().getType());
        switch(p.getDescriptor().getType()){
            case LifecycleManagerDescriptor.LIFECYCLE_MANAGER_TYPE:{
                assertTrue(p instanceof LifecycleManager);
                break; }
            case MaterializerPluginDescriptor.MATERIALIZER:{
                assertTrue(p instanceof MaterializationPlugin);
                break; }
            case IndexerPluginDescriptor.INDEXER:{
                assertTrue(p instanceof IndexerPluginInterface);
                break; }
            default:{}
        }
    };

}

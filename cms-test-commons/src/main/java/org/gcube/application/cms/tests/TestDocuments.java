package org.gcube.application.cms.tests;

import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.utils.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

public class TestDocuments {
    public static File BASE_FOLDER =new File("../test-data/profiledDocuments");
    public static final HashMap<String, Project> documentMap =new HashMap<>();

    static{
        if(!BASE_FOLDER.exists()) System.err.println("Cannot load documents : folder not found, path "+BASE_FOLDER.getAbsolutePath());
        for(File f:BASE_FOLDER.listFiles(pathname -> {return pathname.getName().endsWith(".json");})){
            try {
                if(!f.isDirectory()) {
                    Project p = Serialization.read(
                            Files.readFileAsString(f.getAbsolutePath(), Charset.defaultCharset()), Project.class);
                    documentMap.put(f.getName(), p);
                }
            } catch (IOException e) {
                System.err.println("WARN : Unable to read file "+f.getAbsolutePath());
            }
        }
    }

}

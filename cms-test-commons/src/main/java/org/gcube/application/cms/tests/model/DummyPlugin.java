package org.gcube.application.cms.tests.model;

import com.vdurmont.semver4j.Semver;
import org.gcube.application.cms.plugins.IndexerPluginInterface;
import org.gcube.application.cms.plugins.LifecycleManager;
import org.gcube.application.cms.plugins.MaterializationPlugin;
import org.gcube.application.cms.plugins.PluginManagerInterface;
import org.gcube.application.cms.plugins.faults.*;
import org.gcube.application.cms.plugins.reports.*;
import org.gcube.application.cms.plugins.requests.*;
import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.configuration.Index;
import org.gcube.application.geoportal.common.model.plugins.PluginDescriptor;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public class DummyPlugin implements LifecycleManager, IndexerPluginInterface, MaterializationPlugin {


    @Override
    public IndexDocumentReport index(IndexDocumentRequest request) throws InvalidPluginRequestException {
        return new IndexDocumentReport(request);
    }

    @Override
    public IndexDocumentReport deindex(IndexDocumentRequest request) throws InvalidPluginRequestException {
        return null;
    }

    @Override
    public Index getIndex(BaseRequest request) throws ConfigurationException {
        return new Index("Dummy index");
    }

    @Override
    public InitializationReport initInContext() throws InitializationException {
        return new InitializationReport(Report.Status.WARNING,"DUMB");
    }

    @Override
    public InitializationReport init() throws InitializationException {
        return new InitializationReport(Report.Status.WARNING,"DUMB");
    }

    @Override
    public void shutdown() throws ShutDownException {

    }

    @Override
    public StepExecutionReport performStep(StepExecutionRequest request) throws StepException, InvalidPluginRequestException {
        request.validate();
        return new StepExecutionReport(request);
    }

    @Override
    public Configuration getCurrentConfiguration(BaseRequest ucd) throws ConfigurationException {
        return new Configuration();
    }

    @Override
    public EventExecutionReport onEvent(EventExecutionRequest request) throws EventException, InvalidPluginRequestException {
        return new EventExecutionReport(request);
    }

    @Override
    public void setPluginManager(PluginManagerInterface manager) {

    }

    @Override
    public MaterializationReport materialize(MaterializationRequest request) throws MaterializationException, InvalidPluginRequestException {
        return new MaterializationReport(request);
    }

    @Override
    public MaterializationReport dematerialize(MaterializationRequest request) throws MaterializationException, InvalidPluginRequestException {
        return null;
    }

    @Override
    public PluginDescriptor getDescriptor() {
        return new PluginDescriptor("DUMMY-PLUGIN","DUMMY-TYPE","Dummy","No op plugin", new Semver("1.0.0"));
    }
}

# Changelog for org.gcube.application.cms.cms-test-commons

## [v1.0.5] - 2024-09-18
- Pom updates

## [v1.0.4] - 2023-01-10
- Pom updates

## [v1.0.3] - 2022-12-07
- Pom updates

## [v1.0.2] - 2022-01-17
- Profiles

## [v1.0.1] - 2021-09-11
- Introduced profiled documents

## [v1.0.0] - 2021-09-11
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

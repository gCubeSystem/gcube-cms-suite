# Changelog for org.gcube.application.geoportal-client

## [v1.2.2] - 2024-04-11
- Refactored

## [v1.2.1] - 2023-09-06
- Using parent version range [#25572]

## [v1.2.0] - 2023-05-11
- Added PATCH method [#24985]
- Integrated `ingnore_errors` in the deleteFileSet

## [v1.1.2] - 2023-01-10
- Pom updates

## [v1.1.1] - 2022-12-07
- Pom updates

## [v1.1.0] - 2022-02-01


## [v1.0.8] - 2022-02-01
- Fixed pom
- Fixes #22722

## [v1.0.7] - 2022-05-25
- Clone Concessione

## [v1.0.6] - 2021-09-20
- Changed artifact dependencies
- Default Profiled Documents client

## [v1.0.5] - 2021-09-20
- Refactored repositories

## [v1.0.4-SNAPSHOT] - 2020-11-11
- Serialization utils
- Queries & Filters support
- Test Reports
- UseCases
- Fixes #21897

## [v1.0.3] - 2020-11-11
- Stateful Concessioni Manager client over mongo 

## [v1.0.2] - 2020-11-11
- Fixed duplicate dependency declaration

## [v1.0.1] - 2020-11-11
-Excluded common-calls 1.2.0

## [v1.0.0] - 2020-11-11
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

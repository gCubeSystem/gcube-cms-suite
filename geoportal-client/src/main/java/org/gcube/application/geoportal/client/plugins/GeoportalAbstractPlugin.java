package org.gcube.application.geoportal.client.plugins;

import javax.ws.rs.client.WebTarget;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.legacy.ConcessioniPlugin;
import org.gcube.application.geoportal.client.legacy.MongoConcessioniPlugin;
import org.gcube.application.geoportal.client.legacy.StatefulMongoConcessioniPlugin;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.rest.ConcessioniI;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportal.common.rest.UseCaseDescriptorsI;
import org.gcube.common.clients.Plugin;
import org.gcube.common.clients.ProxyBuilder;
import org.gcube.common.clients.ProxyBuilderImpl;
import org.gcube.common.clients.config.ProxyConfig;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class GeoportalAbstractPlugin <S, P> implements Plugin<S, P>{


	public static ProxyBuilder<UseCaseDescriptorsI> useCaseDescriptors() {
		return new ProxyBuilderImpl<WebTarget, UseCaseDescriptorsI>(new UCDPlugin());
	}


	public static ProxyBuilder<Projects<Project>> projects(String profileID) {
		ProjectsInterfacePlugin plugin=new ProjectsInterfacePlugin(profileID);
		return new ProxyBuilderImpl<WebTarget, Projects<Project>>(plugin);
	}

	public static <C extends Projects<Project>> ProxyBuilder<C> projects(String profileID, Class<C> customClient) {
		ProjectsInterfacePlugin plugin=new ProjectsInterfacePlugin(profileID);
		plugin.setCustomClientImplementation(customClient);
		return new ProxyBuilderImpl<WebTarget, C>(plugin);
	}

	public static <P extends Project> ProxyBuilder<Projects<P>>
	customModel(String profileID, Class<P> customModel ) {
		ProjectsInterfacePlugin plugin=new ProjectsInterfacePlugin(profileID);
		plugin.setProfiledModel(customModel);
		return new ProxyBuilderImpl<WebTarget, Projects<P>>(plugin);
	}

	public static <P extends Project,C extends Projects<P>> ProxyBuilder<C>
	customModel(String profileID, Class<P> customModel , Class<C> customClient) {
		ProjectsInterfacePlugin plugin=new ProjectsInterfacePlugin(profileID);
		plugin.setProfiledModel(customModel);
		plugin.setCustomClientImplementation(customClient);
		return new ProxyBuilderImpl<WebTarget, C>(plugin);
	}
	
	@Override
	public Exception convert(Exception fault, ProxyConfig<?, ?> config) {
		return fault;
	}

	
	@Override
	public String name() {
		return InterfaceConstants.APPLICATION_BASE_PATH+InterfaceConstants.APPLICATION_PATH;
	}

	@Override
	public String namespace() {
		return InterfaceConstants.NAMESPACE;
	}

	@Override
	public String serviceClass() {
		return InterfaceConstants.SERVICE_CLASS;
	}

	@Override
	public String serviceName() {
		return InterfaceConstants.SERVICE_NAME;
	}

	


	//******** LEGACY
	public static ProxyBuilder<ConcessioniI> concessioni() {
		return new ProxyBuilderImpl<WebTarget,ConcessioniI>(concessioni_plugin);
	}

	public static ProxyBuilder<MongoConcessioni> mongoConcessioni(){
		return new ProxyBuilderImpl<WebTarget, MongoConcessioni>(mongo_concessioni_plugin);
	}

	public static ProxyBuilder<ConcessioniManagerI> statefulMongoConcessioni(){
		return new ProxyBuilderImpl<WebTarget, ConcessioniManagerI>(stateful_mongo_concessioni_plugin);
	}

	//** LEGACY

	@Deprecated
	private static ConcessioniPlugin concessioni_plugin=new ConcessioniPlugin();
	@Deprecated
	private static MongoConcessioniPlugin mongo_concessioni_plugin=new MongoConcessioniPlugin();
	@Deprecated
	private static StatefulMongoConcessioniPlugin stateful_mongo_concessioni_plugin=new StatefulMongoConcessioniPlugin();




}

package org.gcube.application.geoportal.client;

import java.util.Iterator;

import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;

public class DefaultProfiledConcessioni implements MongoConcessioni {


    @Override
    public Concessione createNew(Concessione c) throws Exception {
        return null;
    }

    @Override
    public void deleteById(String id) throws Exception {

    }

    @Override
    public void deleteById(String id, Boolean force) throws Exception {

    }

    @Override
    public Concessione getById(String id) throws Exception {
        return null;
    }

    @Override
    public Iterator<Concessione> getList() throws Exception {
        return null;
    }

    @Override
    public Concessione publish(String id) throws Exception {
        return null;
    }

    @Override
    public Concessione registerFileSet(String id, AddSectionToConcessioneRequest request) throws Exception {
        return null;
    }

    @Override
    public Concessione cleanFileSet(String id, String path) throws Exception {
        return null;
    }

    @Override
    public Concessione update(String id, String jsonUpdate) throws Exception {
        return null;
    }

    @Override
    public Concessione replace(Concessione replacement) throws Exception {
        return null;
    }

    @Override
    public void unPublish(String id) throws Exception {

    }

    @Override
    public Configuration getCurrentConfiguration() throws Exception {
        return null;
    }

    @Override
    public Iterator<Concessione> search(String filter) throws Exception {
        return null;
    }

    @Override
    public Iterator<Concessione> query(QueryRequest request) throws Exception {
        return null;
    }

    @Override
    public String queryForJSON(QueryRequest request) throws Exception {
        return null;
    }

    @Override
    public <T> Iterator<T> queryForType(QueryRequest request, Class<T> clazz) throws Exception {
        return null;
    }
}

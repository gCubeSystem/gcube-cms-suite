package org.gcube.application.geoportal.client;

import javax.ws.rs.core.Response;

import org.gcube.application.geoportal.client.utils.Serialization;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResponseCommons {

    protected static<R> R check(Response resp, Class<R> clazz) throws Exception {
        String resString=resp.readEntity(String.class);
        if(resp.getStatus()<200||resp.getStatus()>=300) {
            throw new Exception("RESP STATUS IS " + resp.getStatus() + ". Message : " + resString);
        }
        log.debug("Resp String is "+resString);
        if(clazz!=null)
            if (clazz==String.class)
                return (R) resString;
            else
                return Serialization.read(resString, clazz);
        else return null;
    }
}

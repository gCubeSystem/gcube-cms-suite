package org.gcube.application.geoportal.client.legacy;

import org.gcube.application.geoportal.common.model.legacy.AbstractRelazione;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.rest.TempFile;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;

public interface ConcessioniManagerI extends MongoConcessioni{


	public Concessione getCurrent();
	public Concessione addImmagineRappresentativa(UploadedImage toAdd,TempFile f) throws Exception;
	public Concessione addPiantaFineScavo(LayerConcessione toAdd,TempFile...files)throws Exception;
	public Concessione setPosizionamento(LayerConcessione toSet,TempFile...files)throws Exception;
	public Concessione setRelazioneScavo(RelazioneScavo toSet,TempFile...files)throws Exception;
	public Concessione setAbstractRelazioneScavo(AbstractRelazione toSet, TempFile...files)throws Exception;
	
	
	public Concessione addImmagineRappresentativa(UploadedImage toAdd,InputStreamDescriptor f) throws Exception;
	public Concessione addPiantaFineScavo(LayerConcessione toAdd,InputStreamDescriptor...files)throws Exception;
	public Concessione setPosizionamento(LayerConcessione toSet,InputStreamDescriptor...files)throws Exception;
	public Concessione setRelazioneScavo(RelazioneScavo toSet,InputStreamDescriptor f)throws Exception;
	public Concessione setAbstractRelazioneScavo(AbstractRelazione toSet,InputStreamDescriptor f)throws Exception;
	
	public Concessione publish() throws Exception;
	public void delete() throws Exception;
}

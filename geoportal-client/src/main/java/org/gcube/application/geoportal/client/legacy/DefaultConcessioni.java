package org.gcube.application.geoportal.client.legacy;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.gcube.application.geoportal.common.rest.ConcessioniI;
import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DefaultConcessioni implements ConcessioniI{

	@NonNull
	private final ProxyDelegate<WebTarget> delegate;

	@Override
	public String create(final String toCreate) throws Exception {
		Call<WebTarget,String> call= new Call<WebTarget,String>(){
			@Override
			public String call(WebTarget endpoint) throws Exception {				
				return endpoint.request(MediaType.APPLICATION_JSON).put(Entity.entity(toCreate, MediaType.APPLICATION_JSON)).readEntity(String.class);
			}
		};		
		return delegate.make(call);
	}
	
	
	@Override
	public String readById(final String readById) throws Exception {
		Call<WebTarget,String> call= new Call<WebTarget,String>(){
			@Override
			public String call(WebTarget endpoint) throws Exception {				
				return endpoint.path(readById).request(MediaType.APPLICATION_JSON).get().readEntity(String.class);
			}
		};
		return delegate.make(call);
	}
	
	@Override
	public String getAll() throws Exception {
		Call<WebTarget,String> call= new Call<WebTarget,String>(){
			@Override
			public String call(WebTarget endpoint) throws Exception {				
				return endpoint.request(MediaType.APPLICATION_JSON).get().readEntity(String.class);
			}
		};
		return delegate.make(call);
	}
	
//	@Override
//	public String update(final String id, final String updated) throws Exception {
//		Call<WebTarget,String> call= new Call<WebTarget,String>(){
//			@Override
//			public String call(WebTarget endpoint) throws Exception {				
//				return endpoint.path(id).request(MediaType.APPLICATION_JSON).put(Entity.entity(updated, MediaType.APPLICATION_JSON)).readEntity(String.class);
//			}
//		};
//		return delegate.make(call);
//	}


	@Override
	public String addSection(String arg0, String arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}

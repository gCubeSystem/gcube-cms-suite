package org.gcube.application.geoportal.client.legacy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.ws.rs.client.WebTarget;

import org.gcube.application.geoportal.client.DefaultMongoConcessioni;
import org.gcube.application.geoportal.common.model.legacy.AbstractRelazione;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.Concessione.Paths;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.model.rest.TempFile;
import org.gcube.application.geoportal.common.utils.FileSets;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.common.clients.delegates.ProxyDelegate;

import lombok.Getter;


public class StatefulMongoConcessioni extends DefaultMongoConcessioni implements ConcessioniManagerI{

	
	
	public StatefulMongoConcessioni(ProxyDelegate<WebTarget> delegate) {
		super(delegate);
	}

	@Getter
	private Concessione current =null;
	private StorageUtils storage=new StorageUtils();


	// Override methods to handle state
	@Override
	public Concessione createNew(Concessione c) throws Exception {
		current =super.createNew(c);
		return current;
	}

	@Override
	public Concessione getById(String id) throws Exception {
		current = super.getById(id);
		return current;
	}

	@Override
	public Concessione publish() throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		current =super.publish(current.getMongo_id());
		return current;
	}

	
	@Override
	public void delete() throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		super.deleteById(current.getMongo_id());
		current =null;
	}

	@Override
	public Concessione addImmagineRappresentativa(UploadedImage toAdd, TempFile f) throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		if(current.getImmaginiRappresentative()==null) current.setImmaginiRappresentative(new ArrayList<UploadedImage>());
		current.getImmaginiRappresentative().add(toAdd);
		current =replace(current);

		AddSectionToConcessioneRequest req = new AddSectionToConcessioneRequest();
		req.setDestinationPath(Paths.imgByIndex(current.getImmaginiRappresentative().size()-1));
		req.setStreams(Collections.singletonList(f));

		
		current =super.registerFileSet(current.getMongo_id(), req);
		return current;
	}

	@Override
	public Concessione addPiantaFineScavo(LayerConcessione toAdd, TempFile... files) throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		if(current.getPianteFineScavo()==null) current.setPianteFineScavo(new ArrayList<>());
		current.getPianteFineScavo().add(toAdd);
		current =replace(current);

		AddSectionToConcessioneRequest req = new AddSectionToConcessioneRequest();
		req.setDestinationPath(Paths.piantaByIndex(current.getPianteFineScavo().size()-1));
		req.setStreams(Arrays.asList(files));


		current =super.registerFileSet(current.getMongo_id(),req);
		return current;
	}

	@Override
	public Concessione setPosizionamento(LayerConcessione toSet, TempFile... files) throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		current.setPosizionamentoScavo(toSet);
		current =replace(current);
		AddSectionToConcessioneRequest req = new AddSectionToConcessioneRequest();
		req.setDestinationPath(Paths.POSIZIONAMENTO);
		req.setStreams(Arrays.asList(files));
		
		current =super.registerFileSet(current.getMongo_id(),req);
		return current;
	}

	@Override
	public Concessione setRelazioneScavo(RelazioneScavo toSet, TempFile... files) throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		current.setRelazioneScavo(toSet);
		current =replace(current);

		AddSectionToConcessioneRequest req = new AddSectionToConcessioneRequest();
		req.setDestinationPath(Paths.RELAZIONE);
		req.setStreams(Arrays.asList(files));
		
		current =super.registerFileSet(current.getMongo_id(),req);
		return current;
	}

	@Override
	public Concessione setAbstractRelazioneScavo(AbstractRelazione toSet, TempFile... files) throws Exception {
		if(current ==null) throw new Exception("Invalid operation : current Concessione is null.");
		current.setAbstractRelazione(toSet);
		current =replace(current);

		AddSectionToConcessioneRequest req = new AddSectionToConcessioneRequest();
		req.setDestinationPath(Paths.ABSTRACT_RELAZIONE);
		req.setStreams(Arrays.asList(files));

		current =super.registerFileSet(current.getMongo_id(),req);
		return current;
	}


	@Override
	public Concessione addImmagineRappresentativa(UploadedImage toAdd, InputStreamDescriptor f) throws Exception {
		return addImmagineRappresentativa(toAdd,FileSets.asTemp(storage,f));
	}

	@Override
	public Concessione addPiantaFineScavo(LayerConcessione toAdd, InputStreamDescriptor... files) throws Exception {
		return addPiantaFineScavo(toAdd, FileSets.asTemp(storage,files));
	}

	@Override
	public Concessione setPosizionamento(LayerConcessione toSet, InputStreamDescriptor... files) throws Exception {
		return setPosizionamento(toSet,FileSets.asTemp(storage,files));
	}

	@Override
	public Concessione setRelazioneScavo(RelazioneScavo toSet, InputStreamDescriptor f) throws Exception {
		return setRelazioneScavo(toSet,FileSets.asTemp(storage,f));
	}

	@Override
	public Concessione setAbstractRelazioneScavo(AbstractRelazione toSet, InputStreamDescriptor f) throws Exception {
		return setAbstractRelazioneScavo(toSet,FileSets.asTemp(storage,f));
	}
}

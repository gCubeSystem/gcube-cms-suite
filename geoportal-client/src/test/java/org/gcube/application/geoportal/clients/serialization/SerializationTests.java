package org.gcube.application.geoportal.clients.serialization;

import java.io.IOException;
import java.time.LocalDateTime;

import org.gcube.application.geoportal.client.utils.Serialization;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SerializationTests {


    @Test
    public void testDates() throws IOException {
        LocalDateTime obj = LocalDateTime.now();

        roundTrip(obj);
    }

    public boolean roundTrip(Object obj) throws IOException {
        log.info("Round trip for {}",obj);
        Class clazz =Object.class;
        if(obj!=null) clazz=obj.getClass();
        log.debug("Class is {}",obj.getClass());
        String json= Serialization.write(obj);
        log.debug("JSON String is : {}",json);
        Object unmarshalled = Serialization.read(json,clazz);
        log.debug("Unmarshalled as {}",unmarshalled);
        return obj.equals(unmarshalled);
    }
}

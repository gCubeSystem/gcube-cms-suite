package org.gcube.application.geoportal.clients;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.projects;

import java.io.File;
import java.io.FileNotFoundException;
import java.rmi.RemoteException;

import org.bson.Document;
import org.gcube.application.cms.tests.Tests;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.faults.InvalidRequestException;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.rest.RegisterFileSetRequest;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportal.common.utils.FileSets;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class GenericUseCases extends BasicVreTests{

    protected String getUCID(){ return "basic";}

    protected Projects<Project> getClient(String profileID){
        return projects(profileID).build();
    }



    protected Projects<Project> getClient(){
        return getClient(getUCID());
    }

    @Test
    public void createNew () throws RemoteException, FileNotFoundException, JsonProcessingException, InvalidRequestException {
        Projects<Project> client = getClient();

        // Create project
        Project project = client.createNew(getNewDocument());

        project = prepareWithFileSet(project);

        System.out.println("Resulting Project : "+project);
        System.out.println("JSON Reprepsentation : "+ Serialization.write(project));
    }

    protected Document getNewDocument(){
        Document myDocument= new Document();
        myDocument.put("section",new Document("title","myTitle"));
        return myDocument;
    }

    protected Project prepareWithFileSet(Project project) throws FileNotFoundException, InvalidRequestException, RemoteException {

        return getClient().registerFileSet(project.getId(), prepareRequest());
    }

    protected RegisterFileSetRequest prepareRequest() throws FileNotFoundException {
        String parentPath = "$.section";
        String fieldName = "fileset";
        String fieldDefinition = "$.section._children[?(@.fileset)]";

        // Prepare request
        return FileSets.
                prepareRequest(new StorageUtils(),
                        parentPath,fieldName,fieldDefinition, new File(Tests.FOLDER_CONCESSIONI,"pos.shp"));
    }

}

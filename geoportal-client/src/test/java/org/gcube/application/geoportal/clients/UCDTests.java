package org.gcube.application.geoportal.clients;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

import org.bson.Document;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.DataAccessPolicy;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.rest.UseCaseDescriptorsI;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Assert;
import org.junit.Test;

public class UCDTests{




    public UseCaseDescriptorsI getClient(){
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        TokenSetter.set(GCubeTest.getContext());
        return useCaseDescriptors().build();
    }

    @Test
    public void query() throws Exception {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        UseCaseDescriptorsI client =getClient();
        QueryRequest request = new QueryRequest();
        // All
        AtomicLong counter = new AtomicLong(0l);
        client.query(request).forEachRemaining(u ->counter.incrementAndGet());

        // Filter by presence of handler Id
        request.setFilter(Document.parse("{\"_handlers._id\" : {\"$eq\" : \"org.gcube....geoportal-data-entry-portlet\"}}"));

        System.out.println("Count : "+counter.get());
    }

    @Test
    public void getByIdAll() throws Exception {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        UseCaseDescriptorsI client =getClient();

        QueryRequest request = new QueryRequest();
        // All

        client.query(request).forEachRemaining(u -> {
            try {
                System.out.println("Obtained "+client.getById(u.getId()).getId());
            } catch (Exception e) {
                e.printStackTrace(System.err);
                Assert.fail("Unable to get "+u.getId());
            }
        });
    }

    @Test
    public void getById() throws Exception {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        UseCaseDescriptorsI client =getClient();

        System.out.println(client.getById("profiledConcessioni"));
    }

    @Test
    public void checkPolicies() throws Exception {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        UseCaseDescriptorsI client =getClient();
        UseCaseDescriptor ucd = client.getById("profiledConcessioni");
        System.out.println(Serialization.write(ucd));
        User u = new User();
        u.setRoles(new HashSet<>());
        u.getRoles().addAll(Arrays.asList("Members","Data-Manager"));

        DataAccessPolicy p = ucd.getMatching(u);
        System.out.println("Evaluated policy : "+Serialization.write(p));
        assertEquals(p.getPolicy().getRead(), DataAccessPolicy.Policy.Type.any);
        assertEquals(p.getPolicy().getWrite(), DataAccessPolicy.Policy.Type.any);

    }
}

# Changelog for org.gcube.application.cms.concessioni-model

## [v1.0.4] - 2023-09-06
- Using parent version range [#25572]

## [v1.0.3] - 2023-01-10
- Pom updates

## [v1.0.2] - 2022-12-07
- Pom updates

## [v1.0.1] - 2022-01-17
- Defaults Evaluation


## [v1.0.0] - 2021-12-15
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

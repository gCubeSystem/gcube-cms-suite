package org.gcube.application.cms.custom.gna.concessioni.model;


import org.gcube.application.geoportal.common.model.document.Project;


public class ProfiledConcessione extends Project {

        public static final String NOME="nome";
        //Introduzione (descrizione del progetto)
         public final static String INTRODUZIONE = "introduzione";
        //Descrizione del contenuto
         public final static String DESCRIZIONE_CONTENUTO ="descrizioneContenuto";
        //Autori
         public final static String AUTHORS= "authors";
        //Soggetto che materialmente invia i dati.
         public final static String CONTRIBUTORE ="contributore";

        //Indicare il nome del titolare/i dei dati contenuti nel dataset e/o per sue specifiche parti.
         public final static String TITOLARI="titolari";

         public final static String RESPONSABILE="responsabile";
         public final static String EDITORS ="editore";

         public final static String FONTI_FINANZIAMENTO="fontiFinanziamento";
        //Research Excavation; Archaeology  (valori di default)
         public final static String SOGGETTO="soggetto";

        //Referenze bibliografiche, DOI (se esistenti) di risorse correlate all’indagine in oggetto
         public final static String RISORSE_CORRELATE="risorseCorrelate";

         public final static String DATA_INZIO_PROGETTO="dataInizioProgetto";
         public final static String DATA_FINE_PROGETTO="dataFineProgetto";

         public final static String TITOLARE_LICENZA="titolareLicenza";
         public final static String TITOLARE_COPYRIGHT="titolareCopyright";

         public final static String PAROLE_CHIAVE_LIBERE="paroleChiaveLibere";
         public final static String PAREOLE_CHIAVE_ICCD="paroleChiaveICCD";

         public final static String RELAZIONE_SCAVO="relazioneScavo";
         public final static String ABSTRACT_RELAZIONE ="abstractRelazione";
         public final static String IMMAGINI_RAPPRESENTATIVE="immaginiRappresentative";
         public final static String POSIZIONAMENTO_SCAVO = "posizionamentoScavo";
         public final static String PIANTE_FINE_SCAVO = "pianteFineScavo";
         public final static String GENERIC_CONTENT= "genericContent";

         public static class Sections{
             public static final String TITOLO="titolo";
             public static final String ABSTRACT="abstractSection";
         }

         public static class Layers{
             public static final String TOPIC="topicCategory";
             public static final String SUB_TOPIC="subTopic";

             public static final String VALUTAZIONE_QUALITA="valutazioneQualita";
             public static final String METODO_RACCOLTA="metodoRaccoltaDati";
             public static final String SCALA_ACQUISIZIONE="scalaAcquisizione";

         }

         public static class Relazione{
             public static final String RESPONSABILI=  "responsabili";
         }

        public static class Immagini{
            public static final String DIDASCALIA=  "didascalia";
        }
         public static class AbstractRelazione{
             public static final String ABSTRACT_ITA=  "abstractIta";
             public static final String ABSTRACT_ENG=  "abstractEng";
         }


}

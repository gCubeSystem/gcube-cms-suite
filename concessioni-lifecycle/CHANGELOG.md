# Changelog for org.gcube.application.cms.concessioni-lifecycle

## [v1.1.2] - 2024-11-21
- Just to include the new geoportal-common.1.1.1 , see [27505#note-41]

## [v1.1.1] - 2023-09-06
- Using parent version range [#25572]

## [v1.1.0] - 2023-03-13
- [#24754] Fixed hide/show project ids on the latest node of the relation chain

## [v1.0.4] - 2023-03-06
- [#24702] Fixed the default-lc-managers dependency 

## [v1.0.3] - 2023-01-10
- Pom updates
- Fixes default access

## [v1.0.2] - 2022-12-07
- Pom updates
- Introduced module default-lc-managers
- 
## [v1.0.1] - 2022-01-17
- Serialization adaptation

## [v1.0.0] - 2021-12-15
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

package org.gcube.application.cms.concessioni.plugins;

import org.bson.Document;
import org.gcube.application.cms.custom.gna.concessioni.model.ProfiledConcessione;
import org.gcube.application.cms.plugins.faults.IndexingException;
import org.gcube.application.cms.plugins.requests.IndexDocumentRequest;
import org.gcube.application.cms.plugins.requests.StepExecutionRequest;
import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.cms.tests.TestDocuments;
import org.gcube.application.cms.tests.plugins.BasicPluginTest;
import org.gcube.application.geoportal.common.model.JSONPathWrapper;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.document.filesets.RegisteredFileSet;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import static junit.framework.TestCase.*;

public class ConcessioniPluginTests extends BasicPluginTest {
	
	public static final String SCOPE_TEST = "/gcube/devsec/devVRE";
	public static final String USERNAME = "francesco.mangiacrapa";
	public static final String UCD_ID = "concessioni-estere";

    @Test
    public void testLoad(){
        Assert.assertTrue(true);
    }


    //@Test
    public void testDefaults() throws IOException {
        Project c= TestDocuments.documentMap.get("dummy.json");

        LocalDateTime start = LocalDateTime.now();
        c.getTheDocument().put(ProfiledConcessione.DATA_INZIO_PROGETTO,start);
        c.getTheDocument().put(ProfiledConcessione.DATA_FINE_PROGETTO,start);

        c.getTheDocument().put(ProfiledConcessione.POSIZIONAMENTO_SCAVO,new Document());
        c.getTheDocument().put(ProfiledConcessione.IMMAGINI_RAPPRESENTATIVE,new Document[]{new Document()});
        c.getTheDocument().put(ProfiledConcessione.PIANTE_FINE_SCAVO,new Document[]{new Document()});

        c= (ProfiledConcessione) ConcessioniLifeCycleManager.setDefaults(c);

        System.out.println("Obtained "+Serialization.write(c));

        assertNotNull(c.getTheDocument().get(ProfiledConcessione.SOGGETTO));
        assertNotNull(c.getTheDocument().getString(ProfiledConcessione.DESCRIZIONE_CONTENUTO));

        assertNotNull(c.getTheDocument().get(ProfiledConcessione.DATA_INZIO_PROGETTO));
        assertEquals(Serialization.convert(c.getTheDocument().get(ProfiledConcessione.DATA_INZIO_PROGETTO),LocalDateTime.class),start);
        assertNotNull(c.getTheDocument().get(ProfiledConcessione.DATA_FINE_PROGETTO));
        assertEquals(Serialization.convert(c.getTheDocument().get(ProfiledConcessione.DATA_FINE_PROGETTO),LocalDateTime.class),start);

        assertNotNull(c.getTheDocument().get(ProfiledConcessione.RELAZIONE_SCAVO));
        Document rel = Serialization.convert(c.getTheDocument().get(ProfiledConcessione.RELAZIONE_SCAVO), Document.class);
        assertNotNull(rel.get(ProfiledConcessione.Sections.TITOLO));
        assertNotNull(rel.get(ProfiledConcessione.SOGGETTO));
        assertNull(rel.get(RegisteredFileSet.CREATION_INFO));
        assertNull(rel.get(RegisteredFileSet.ACCESS));

        Document abs = Serialization.convert(c.getTheDocument().get(ProfiledConcessione.ABSTRACT_RELAZIONE), Document.class);
        assertNotNull(abs.get(ProfiledConcessione.Sections.TITOLO));
        assertNull(abs.get(RegisteredFileSet.CREATION_INFO));
        assertNull(abs.get(RegisteredFileSet.ACCESS));


        Document pos = Serialization.convert(c.getTheDocument().get(ProfiledConcessione.POSIZIONAMENTO_SCAVO), Document.class);
        assertNotNull(pos.get(ProfiledConcessione.Sections.TITOLO));
        assertNotNull(pos.get(ProfiledConcessione.Sections.ABSTRACT));
        assertNotNull(pos.get(ProfiledConcessione.Layers.TOPIC));
        assertNotNull(pos.get(ProfiledConcessione.Layers.SUB_TOPIC));
        assertNull(pos.get(RegisteredFileSet.CREATION_INFO));
        assertNull(pos.get(RegisteredFileSet.ACCESS));


        JSONPathWrapper wrapper = new JSONPathWrapper(c.getTheDocument().toJson());
        assertTrue(c.getTheDocument().containsKey(ProfiledConcessione.IMMAGINI_RAPPRESENTATIVE));
        c.getTheDocument().get(ProfiledConcessione.IMMAGINI_RAPPRESENTATIVE, List.class).forEach(o -> {
            try{
                Document img = Serialization.asDocument(o);
                assertNotNull(img.get(ProfiledConcessione.SOGGETTO));
                assertNull(img.get(RegisteredFileSet.CREATION_INFO));
                assertNull(img.get(RegisteredFileSet.ACCESS));
            }catch (Throwable t){
                System.out.println("Unable to read IMG "+o);
                t.printStackTrace();
                fail(t.getMessage());
            }
        });

        assertTrue(c.getTheDocument().containsKey(ProfiledConcessione.PIANTE_FINE_SCAVO));
        c.getTheDocument().get(ProfiledConcessione.PIANTE_FINE_SCAVO, List.class).forEach(o -> {
            try{
                Document pianta = Serialization.asDocument(o);
                assertNotNull(pianta.get(ProfiledConcessione.Sections.TITOLO));
                assertNotNull(pianta.get(ProfiledConcessione.Sections.ABSTRACT));
                assertNotNull(pianta.get(ProfiledConcessione.Layers.TOPIC));
                assertNotNull(pianta.get(ProfiledConcessione.Layers.SUB_TOPIC));
                assertNull(pianta.get(RegisteredFileSet.CREATION_INFO));
                assertNull(pianta.get(RegisteredFileSet.ACCESS));
            }catch (Throwable t){
                System.out.println("Unable to read pianta "+o);
                t.printStackTrace();
                fail(t.getMessage());
            }
        });

    }
}

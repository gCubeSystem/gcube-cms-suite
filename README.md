gCube CMS Suite
--------------------------------------------------
 
gCube CMS Suite is a distributed full stack application for publication management in a gCube Hybrid e-infrastructure. Check wiki [here](https://sublime-and-sphinx-guide.readthedocs.io)

[<img src="https://gcube.wiki.gcube-system.org/images_gcube/e/e4/Geo_Portale%281%29.png">](https://geoportal.cloud.d4science.org/geoportal-service/docs/index.html)


Rationale :

Publication involves lots of common features as well as custom behaviour and formats. The service implements the basic common logic, delegating to installed plugin both custom and configurable functions ranging from validation, data manifestation, indexing and lifecycle management.
**Lifecycle management** is itself an extension allowing for both common and complex ad-hoc workflows. 
**High modularity of plugins** allows for the composition of ad hoc use cases with maximized re-usability. 

The suite comes with a set of pre-built plugins and GUIs that communities can easily extend and / or reuse.

## Built with
* [gCube SmartGears](https://gcube.wiki.gcube-system.org/gcube/SmartGears) - The gCube SmartGears framework 
* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [JAX-RS](https://github.com/eclipse-ee4j/jaxrs-api) - Java™ API for RESTful Web Services
* [Jersey](https://jersey.github.io/) - JAX-RS runtime
* [Maven](https://maven.apache.org/) - Dependency Management
* [Enunciate](http://enunciate.webcohesion.com/) - API Documentation

## General Architecture
[<img src="https://gcube.wiki.gcube-system.org/images_gcube/c/ce/Geoportal_General_Architecture.jpg">](https://geoportal.cloud.d4science.org/geoportal-service/docs/architecture.html)

## Lifecycle Managements

Document Lifecycle Managements in action:

##### Single Step (no moderation)

```mermaid
flowchart LR
    New[New Project] -..-> DRAFT(DRAFT)
    Update[Update Project] -..-> DRAFT(DRAFT)
    DRAFT ==>|publish| Published(Published)
    Published ==>|unpublish| DRAFT

    classDef noteClass fill:#ffe4b5,stroke:none,font-style:italic,font-size:14px;
    classDef box-b fill:#3D6BA6,stroke:#12407B,stroke-width:1px,color:#fff,font-size:16px;
    classDef box-g fill:#3BBF9E,stroke:#03926E,stroke-width:1px,color:#fff,font-size:16px;

    class New,Update noteClass
    class DRAFT box-b
    class Published box-g
```

##### 3 Phases (content moderation)

```mermaid
flowchart LR
    New[New Project] -..-> DRAFT(DRAFT)
    Update[Update Project] -..-> DRAFT(DRAFT)
    DRAFT ==>|submit-for-review| PendingApproval(Pending<br>Approval)
    PendingApproval ==> |approve-submitted| Published(Published)
    PendingApproval ==>|reject-draft| DRAFT
    Published ==>|unpublish| DRAFT

    classDef noteClass fill:#ffe4b5,stroke:none,font-style:italic,font-size:14px;
    classDef box-b fill:#3D6BA6,stroke:#12407B,stroke-width:1px,color:#fff,font-size:16px;
    classDef box-g fill:#3BBF9E,stroke:#03926E,stroke-width:1px,color:#fff,font-size:16px;
    classDef box-o fill:#FF904E,stroke:#E15604,stroke-width:1px,color:#fff,font-size:16px;

    class New,Update noteClass
    class DRAFT box-b
    class PendingApproval box-o
    class Published box-g
```

## Plugins

Plugins in action:

* [Lifecycle Manager](/gCubeSystem/gcube-cms-suite/src/branch/master/default-lc-managers)
* [Spatial Data Infrastructure "SDI"](/gCubeSystem/gcube-cms-suite/src/branch/master/sdi-plugins)
* [Notifications](/gCubeSystem/gcube-cms-suite/src/branch/master/notifications-plugins)
* [Catalogue Binding](/gCubeSystem/gcube-cms-suite/src/branch/event_manager/catalogue-binding-plugin)

## Use Case
##### The D4GNA

["Dataset per il Geoportale Nazionale per l'Archeologia"](https://gna.d4science.org)

###### D4GNA Component diagram

```mermaid
graph TB
    subgraph Geoportal [D4GNA Components]
        geoportalservice[<img src='https://gcube.wiki.gcube-system.org/images_gcube/6/63/Geoportal_logo.png'> Geoportal Service]
        mongo[<img src='https://webimages.mongodb.com/_com_assets/cms/kuyjf3vea2hg34taa-horizontal_default_slate_blue.svg?auto=format%252Ccompress'> Mongo DB]
        postgis[<img src='https://www.osgeo.org/wp-content/uploads/postgis-logo-1.png'> PostGIS DB]
        geoserver[<img src='https://geoserver.org/img/geoserver-logo.png'> Geoserver]
        note-gs-to-mongo[Geoportal Service:<br>creates/reads Profiled Documents and UCDs<br>creates/reads Plugins configured<br>on Mongo DB]
        note-gs-to-geoserver[Geoportal Service:<br>creates/reads Workspaces,Stores,Layers,Styles<br>on Geoserver]
        note-gs-to-geoserver2[Geoportal Service:<br>READS via GS Rest Interface<br>e.g. the GeoServerRESTReader.class reads from REST path <br>geoserver_endpoint/rest/layers/profiledconcessioni_gna:profiledconcessioni_gna_centroids.xml<br>WRITES via FeatureTypes<br>e.g. the SDIManagerWrapper.class creates the index layer via FTE]
        note-gs-to-postgis[Geoportal Service:<br>creates/reads Index Layers <br>as Tables on PostGIS for Profile Document ID.<br>Add the Index Layers Store/Workspace to Geoserver]
        note-to-featuretypes[A feature type is a vector based spatial resource<br> or data set that originates from a data store.<br> In some cases, such as with a shapefile,<br> a feature type has a one-to-one relationship<br> with its data store.<br> In other cases, such as PostGIS,<br> the relationship of feature type<br> to data store is many-to-one,<br> feature types corresponding to a table in the database,<br>see https://docs.geoserver.org/main/en/user/rest/api/featuretypes.html]
    end

    subgraph Storage [Cloud Storage]
        shub[SHUB]
        disk1[Storage]
    end

    geoportalservice -->|uses| geoserver
    geoportalservice -->|stores docs| mongo
    geoportalservice -->|sends data| shub
    geoportalservice -->|uses| postgis
    geoserver -->|uses| postgis
    shub -->|stores data| disk1

    note-gs-to-mongo -.-> mongo
    note-gs-to-geoserver -.-> geoserver
    note-gs-to-geoserver2 -.-> geoserver
    note-gs-to-postgis -.-> postgis

    classDef noteClass fill:#ffe4b5,stroke:#000,stroke-dasharray: 1 5,font-style:italic,font-size:12px;
    classDef note2Class fill:#aaccdd,stroke:#000,stroke-dasharray: 1 5,font-style:italic,font-size:12px;
    class note-gs-to-mongo,note-gs-to-geoserver,note-gs-to-geoserver2,note-gs-to-postgis noteClass
    class note-to-featuretypes note2Class
```

###### D4GNA Architecture

[![](https://mermaid.ink/img/pako:eNqFk1FvmzAQx7-K5ScikQgI0MQPk9JF6ia1VVU6TVrogwNXYi3YyJgpaZTvPoMJUKZ0PGBz97vz-f7cCSciBUwwlcmOKUhUJWG6BUVjjvSTSVEVKANRCKno3mJcgeSgJpu1f_e4eh1SpRKSZmAle1Glk83XekGRMX4AM04HiUwa4y9B_mEJ9Ae2BqteQWr44kCR8bwixnt8nCcXPBNWShXd0hImm4f6G61vx1HDmEKUKmPlIOpJW-6-R_-J02ZT5bBYs_0sLPV1OwopDscurmkt-nmI0Orp8_t1DuD0nQmuN_-2CtDjxYlWWmYQe5ExajJzOs5Z7qptlyT69sNcuhV3VDsrf7tW_dZkK_QHuFV9pCZ5RtPpF3RPupZd4e4b7pk0Ml5hXtpcddlXkNsWaZXtKHN2637p3QbohenquFZv3_0O7aO7_uoCu5s3nas92MY5yJyyVA_hqSZjrHaQQ4yJ3qbwRqu9inHMzxqllRLRkSeYKFmBjfU4ZTtM3uq72rgq9D8La0YzSfPOWlD-S4j8EgIp0-o8mKlvhr9BMDnhAyZhcDObB67vO763DBc3gY2PmMydcBa6c9dbhKHnOkv_bOP3JqczCx1_ufSDuee6YbDwwvNfS2J43w?type=png)](https://mermaid.live/edit#pako:eNqFk1FvmzAQx7-K5ScikQgI0MQPk9JF6ia1VVU6TVrogwNXYi3YyJgpaZTvPoMJUKZ0PGBz97vz-f7cCSciBUwwlcmOKUhUJWG6BUVjjvSTSVEVKANRCKno3mJcgeSgJpu1f_e4eh1SpRKSZmAle1Glk83XekGRMX4AM04HiUwa4y9B_mEJ9Ae2BqteQWr44kCR8bwixnt8nCcXPBNWShXd0hImm4f6G61vx1HDmEKUKmPlIOpJW-6-R_-J02ZT5bBYs_0sLPV1OwopDscurmkt-nmI0Orp8_t1DuD0nQmuN_-2CtDjxYlWWmYQe5ExajJzOs5Z7qptlyT69sNcuhV3VDsrf7tW_dZkK_QHuFV9pCZ5RtPpF3RPupZd4e4b7pk0Ml5hXtpcddlXkNsWaZXtKHN2637p3QbohenquFZv3_0O7aO7_uoCu5s3nas92MY5yJyyVA_hqSZjrHaQQ4yJ3qbwRqu9inHMzxqllRLRkSeYKFmBjfU4ZTtM3uq72rgq9D8La0YzSfPOWlD-S4j8EgIp0-o8mKlvhr9BMDnhAyZhcDObB67vO763DBc3gY2PmMydcBa6c9dbhKHnOkv_bOP3JqczCx1_ufSDuee6YbDwwvNfS2J43w)

 
## Documentation
* [Dedicated Wiki](https://geoportal.d4science.org/geoportal-service/docs/index.html#) - powered by [Sphynx](https://www.sphinx-doc.org/en/master/)
* [Service Interactive API](https://geoportal.d4science.org/geoportal-service/api-docs/index.html) - powered by [Enunciate](http://enunciate.webcohesion.com/)
* [Gcube System Wiki](https://gcube.wiki.gcube-system.org/gcube/GeoPortal).
* [Guide Notebooks](use-cases) -  powered by [Jupyter](https://jupyter.org/) 


## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Fabio Sinibaldi** as "Architect", "Developer" ([ORCID](https://orcid.org/0000-0003-1013-6203)) Computer Scientist at ISTI-CNR

* **Francesco Mangiacrapa** as "Architect", "Developer" and "Maintainer" ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.
 
## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - iMarine(grant no. 283644);
    - EUBrazilOpenBio (grant no. 288754).
- the H2020 research and innovation programme 
    - SoBigData (grant no. 654024);
    - PARTHENOS (grant no. 654119);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - BlueBRIDGE (grant no. 675680);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);


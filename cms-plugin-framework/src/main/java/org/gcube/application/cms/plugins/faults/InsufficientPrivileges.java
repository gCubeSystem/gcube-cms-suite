package org.gcube.application.cms.plugins.faults;

public class InsufficientPrivileges extends Exception {
    public InsufficientPrivileges() {
    }

    public InsufficientPrivileges(String message) {
        super(message);
    }

    public InsufficientPrivileges(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientPrivileges(Throwable cause) {
        super(cause);
    }

    public InsufficientPrivileges(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package org.gcube.application.cms.implementations;

import org.gcube.application.cms.caches.Engine;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;

public class StorageHubProvider implements Engine<StorageHubClient> {

	
	@Override
	public StorageHubClient getObject() throws ConfigurationException {
		return new StorageHubClient();
	}
	
	@Override
	public void init() {}
	@Override
	public void shutdown() { }

}

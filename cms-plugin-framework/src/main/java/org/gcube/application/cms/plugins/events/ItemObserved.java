package org.gcube.application.cms.plugins.events;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ItemObserved<T extends Project> implements ItemObservable {

	private User userCaller;
	private Context context;
	private UseCaseDescriptor useCaseDescriptor;
	private T project;
	private EventManager.Event event;
	private Document optional;

	public static enum OPTIONAL_FIELD {
		message, preview_url
	}

	public String getProjectId() {
		log.debug("Called getProjectId");
		if (project == null)
			return null;
		return project.getId();
	}

	@Override
	public String getUCD_Id() {
		log.debug("Called getUCD_Id");
		if (useCaseDescriptor == null)
			return null;
		return useCaseDescriptor.getId();
	}

	public LinkedHashMap<String, Object> getDocumentEntries(int limit) {

		LinkedHashMap<String, Object> documentAsMap = new LinkedHashMap<>();

		try {

			Iterator<Entry<String, Object>> entrySetsIt = project.getTheDocument().entrySet().iterator();
			int i = 0;
			while (entrySetsIt.hasNext()) {
				if (i + 1 > limit)
					break;

				Entry<String, Object> entry = entrySetsIt.next();
				documentAsMap.put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
				i++;
			}

		} catch (Exception e) {
			throw e;
		}

		return documentAsMap;
	}

	public void setOptional(OPTIONAL_FIELD optionalField, String value) {
		this.optional = new Document();
		if (optional == null)
			this.optional = new Document();

		this.optional.put(optionalField.name(), value);
	}

	public String getOptionalValue(OPTIONAL_FIELD optionalField) {
		if (this.optional != null) {
			return this.optional.getString(optionalField.name());
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemObserved [userCaller=");
		builder.append(userCaller);
		builder.append(", context=");
		builder.append(context);
		builder.append(", useCaseDescriptor id=");
		builder.append(useCaseDescriptor != null ? useCaseDescriptor.getId() : null);
		builder.append(", project id=");
		builder.append(project != null ? project.getId() : null);
		builder.append(", event=");
		builder.append(event);
		builder.append(", optional=");
		builder.append(optional);
		builder.append("]");
		return builder.toString();
	}

}

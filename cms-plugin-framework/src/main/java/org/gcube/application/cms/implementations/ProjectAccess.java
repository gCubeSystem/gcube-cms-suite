package org.gcube.application.cms.implementations;

import java.util.List;

import org.bson.Document;
import org.gcube.application.cms.implementations.faults.InvalidUserRoleException;
import org.gcube.application.cms.implementations.faults.ProjectNotFoundException;
import org.gcube.application.cms.implementations.faults.RegistrationException;
import org.gcube.application.cms.implementations.faults.UnauthorizedAccess;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.relationships.RelationshipNavigationObject;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;

public interface ProjectAccess {


    public Project getById(String ucid,String id) throws RegistrationException, ConfigurationException, InvalidUserRoleException, ProjectNotFoundException, UnauthorizedAccess;

    public Iterable<Document> query(String ucid, QueryRequest query) throws RegistrationException, ConfigurationException, InvalidUserRoleException;

    public List<RelationshipNavigationObject> getRelations(String ucid, String id, String relation,Boolean deep) throws InvalidUserRoleException, RegistrationException, ProjectNotFoundException, ConfigurationException, UnauthorizedAccess;
}

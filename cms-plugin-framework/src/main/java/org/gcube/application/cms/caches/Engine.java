package org.gcube.application.cms.caches;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;


public interface Engine <T> {

	public void init();
	public void shutdown();
	
	public T getObject() throws ConfigurationException;
}

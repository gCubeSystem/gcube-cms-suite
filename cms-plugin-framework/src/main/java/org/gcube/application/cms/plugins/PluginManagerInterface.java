package org.gcube.application.cms.plugins;

import java.util.Map;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public interface PluginManagerInterface {

    public Plugin getById(String pluginID) throws ConfigurationException;
    public Map<String,Plugin> getByType(String type) throws ConfigurationException;
}

package org.gcube.application.cms.plugins.faults;

public class MaterializationException extends PluginExecutionException {

    public MaterializationException() {
    }

    public MaterializationException(String message) {
        super(message);
    }

    public MaterializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public MaterializationException(Throwable cause) {
        super(cause);
    }

    public MaterializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

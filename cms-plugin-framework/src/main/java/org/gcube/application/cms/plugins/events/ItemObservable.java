package org.gcube.application.cms.plugins.events;

public interface ItemObservable {
	
	public String getProjectId();
	
	public String getUCD_Id();

}

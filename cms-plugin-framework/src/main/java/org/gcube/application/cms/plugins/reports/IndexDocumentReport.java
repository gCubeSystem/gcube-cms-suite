package org.gcube.application.cms.plugins.reports;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.requests.IndexDocumentRequest;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class IndexDocumentReport extends DocumentHandlingReport<IndexDocumentRequest> {

//    protected List<IdentificationReference> toSetIndexReferences=new ArrayList<>();


    public IndexDocumentReport(@NonNull IndexDocumentRequest theRequest) throws InvalidPluginRequestException {
        super(theRequest);
    }

//    @Override
//    public Project prepareResult() throws JsonProcessingException, PluginExecutionException {
//        Project toReturn= super.prepareResult();
//        if(toSetSpatialReference != null) toReturn.setSpatialReference(toSetSpatialReference);
//        if(toSetTemporalReference != null) toReturn.setTemporalReference(toSetTemporalReference);
//        return toReturn;
//    }
}

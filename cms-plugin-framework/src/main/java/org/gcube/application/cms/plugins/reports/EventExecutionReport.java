package org.gcube.application.cms.plugins.reports;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.requests.EventExecutionRequest;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@ToString(callSuper = true)
@Slf4j
public class EventExecutionReport extends DocumentHandlingReport<EventExecutionRequest>{

    public EventExecutionReport(EventExecutionRequest req) throws InvalidPluginRequestException {
        super(req);
        try{
            if(theRequest.getDocument().getLifecycleInformation()!=null)
                this.setToSetLifecycleInformation(theRequest.getDocument().getLifecycleInformation().cleanState());
            else theRequest.getDocument().setLifecycleInformation(new LifecycleInformation().cleanState());
        }catch(Throwable t){
            log.warn("Cannot to clean state for lifecycle information {} in {} : {}",
                    theRequest.getDocument().getLifecycleInformation(),
                    theRequest.getDocument().getProfileID(),
                    theRequest.getDocument().getId(),
                    t);
        }
        this.getToSetLifecycleInformation().setLastOperationStatus(LifecycleInformation.Status.OK);
    }


    @Override
    public void setToSetLifecycleInformation(LifecycleInformation toSetLifecycleInformation) {
    }

}

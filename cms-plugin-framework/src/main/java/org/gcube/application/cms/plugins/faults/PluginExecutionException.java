package org.gcube.application.cms.plugins.faults;

public class PluginExecutionException extends Exception{
    public PluginExecutionException() {
    }

    public PluginExecutionException(String message) {
        super(message);
    }

    public PluginExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginExecutionException(Throwable cause) {
        super(cause);
    }

    public PluginExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

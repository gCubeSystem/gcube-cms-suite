package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.geoportal.common.model.document.Project;

public interface EventListenerPluginInterface extends InitializablePlugin {

	public void doAction(ItemObserved<Project> itemObserved);

}

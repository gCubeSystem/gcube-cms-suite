package org.gcube.application.cms.plugins.reports;

import java.util.ArrayList;
import java.util.List;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.faults.PluginExecutionException;
import org.gcube.application.cms.plugins.requests.EventExecutionRequest;
import org.gcube.application.cms.plugins.requests.StepExecutionRequest;
import org.gcube.application.geoportal.common.model.document.Project;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StepExecutionReport extends DocumentHandlingReport<StepExecutionRequest>{

    public StepExecutionReport(@NonNull StepExecutionRequest theRequest) throws InvalidPluginRequestException {

        super(theRequest);
        this.getToSetLifecycleInformation().cleanState();
        this.getToSetLifecycleInformation().setLastInvokedStep(theRequest.getStep());
    }

    List<EventExecutionRequest> toTriggerEvents;

    List<StepExecutionRequest> cascadeSteps;




    public StepExecutionReport addToTriggerEvent(EventExecutionRequest req){
        if(toTriggerEvents==null) toTriggerEvents = new ArrayList<>();
        toTriggerEvents.add(req);
        return this;
    }


    public StepExecutionReport addCascadeStep(StepExecutionRequest req){
        if(cascadeSteps == null ) cascadeSteps = new ArrayList<>();
        cascadeSteps.add(req);
        return this;
    }

    @Override
    public Project prepareResult() throws JsonProcessingException, PluginExecutionException {
        Project toReturn= super.prepareResult();

        return toReturn;
    }
}

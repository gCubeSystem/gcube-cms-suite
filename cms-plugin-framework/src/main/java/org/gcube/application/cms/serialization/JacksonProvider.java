package org.gcube.application.cms.serialization;

import java.util.EnumSet;
import java.util.Set;

import org.gcube.application.geoportal.common.JSONSerializationProvider;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

public class JacksonProvider implements JSONSerializationProvider {


    @Override
    public void setJSONWrapperDefaults() {
        Configuration.setDefaults(new Configuration.Defaults() {
            private JsonProvider jacksonProvider  = new JacksonJsonProvider(Serialization.mapper);

            private final MappingProvider mappingProvider = new JacksonMappingProvider(Serialization.mapper);
            @Override
            public JsonProvider jsonProvider() {
                return jacksonProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }
        });
    }
}

package org.gcube.application.cms.plugins.requests;

import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.NonNull;
import lombok.ToString;

@ToString(callSuper = true)
public class MaterializationRequest extends BaseExecutionRequest{
    public MaterializationRequest(@NonNull UseCaseDescriptor useCaseDescriptor, @NonNull User caller, @NonNull Context context, Project document) {
        super(useCaseDescriptor, caller, context, document);
    }
}

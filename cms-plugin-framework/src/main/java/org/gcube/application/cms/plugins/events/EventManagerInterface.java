package org.gcube.application.cms.plugins.events;

import org.gcube.application.cms.plugins.events.EventManager.Event;

public interface EventManagerInterface<T extends ItemObservable> {

	public void subscribe(Event eventType, EventListener<T> listener);

	public void unsubscribe(Event eventType, EventListener<T> listener);

	public void notify(Event eventType, T item);

}

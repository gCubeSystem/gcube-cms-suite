package org.gcube.application.cms.plugins.reports;

import java.util.ArrayList;
import java.util.List;

import org.gcube.application.cms.plugins.faults.PluginExecutionException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Report {

    public static enum Status {
        ERROR,WARNING,OK
    }

    private Status status;
    private List<String> messages;

    public Report(Status status,String ... messages) {
        this.status = status;
        this.messages=new ArrayList<>();
        for (String s : messages)
            this.messages.add(s);
    }

    public Report putMessage(String msg){
        if(messages==null)messages=new ArrayList<>();
        messages.add(msg);
        return this;
    }

    public void validate()throws PluginExecutionException {
        if(status == null) throw new PluginExecutionException("Status is null");
        if(!status.equals(Status.OK))
            if(messages==null || messages.isEmpty()) throw new PluginExecutionException("Messages are mandatory for status != OK ");
    }

}

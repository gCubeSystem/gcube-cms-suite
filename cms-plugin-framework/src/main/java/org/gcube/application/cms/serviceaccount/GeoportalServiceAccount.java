package org.gcube.application.cms.serviceaccount;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.scope.api.ScopeProvider;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class GeoportalServiceAccount.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 3, 2024
 */
@Slf4j
public class GeoportalServiceAccount {

	// geoportal service account config property file
	protected static final String CLIENT_ID_PROPERTY_NAME = "clientId";

	private static final String SE_PROFILE_NAME = "geoportal";
	private static final String SE_CATEGORY_NAME = "SystemWorkspaceClient";

	private static String clientId = "geoportal";

	/**
	 * Gets the client id and client secret.
	 *
	 * @param context the context
	 * @return the client id and client secret
	 */
	private static Entry<String, String> getClientIdAndClientSecret(String context) {
		try {
			IAMClientCredentials credentials = IAMClientCredentialsReader.getCredentials(context,
					SE_PROFILE_NAME, SE_CATEGORY_NAME);

			clientId = credentials.getClientId() == null ? clientId : credentials.getClientId();
			String clientSecret = credentials.getClientSecret();
			SimpleEntry<String, String> entry = new SimpleEntry<String, String>(clientId, clientSecret);
			return entry;
		} catch (Exception e) {
			throw new InternalServerErrorException("Unable to retrieve Application Token for context "
					+ SecretManagerProvider.instance.get().getContext(), e);
		}
	}

	/**
	 * Gets the JWT access token.
	 *
	 * @return the JWT access token
	 * @throws Exception the exception
	 */
	private static TokenResponse getJWTAccessToken() throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		if (secretManager != null) {
			String context = getContext();
			log.info("Context is {}", context);
			Entry<String, String> entry = getClientIdAndClientSecret(context);
			TokenResponse tr = KeycloakClientFactory.newInstance().queryUMAToken(context, entry.getKey(),
					entry.getValue(), context, null);
			return tr;
		} else {
			throw new Exception(SecretManager.class.getSimpleName() + " is null!! Please set it");
		}
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public static String getContext() {
		String context = null;
		SecretManager secretManager = SecretManagerProvider.instance.get();

		// Test case
		if (secretManager == null) {
			log.info(SecretManager.class.getSimpleName() + " is null trying to read the scope from "
					+ ScopeProvider.class.getSimpleName());
			context = ScopeProvider.instance.get();
			log.info("Returning scope {} read from {}", context, ScopeProvider.class.getSimpleName());
			return context;
		}

		context = secretManager.getContext();

		if (context == null) {
			log.info(SecretManager.class.getSimpleName() + " has getContext null trying to read the scope from "
					+ ScopeProvider.class.getSimpleName());
			context = ScopeProvider.instance.get();
			log.info("Returning scope {} read from {}", context, ScopeProvider.class.getSimpleName());
			return context;
		}

		log.info("Returning scope {} read from {}", context, SecretManager.class.getSimpleName());
		return context;
	}

	/**
	 * Gets the geoportal secret.
	 *
	 * @return the geoportal secret
	 * @throws Exception the exception
	 */
	public static Secret getGeoportalSecret() throws Exception {
		TokenResponse tr = getJWTAccessToken();
		Secret secret = new JWTSecret(tr.getAccessToken());
		return secret;
	}

}

package org.gcube.application.cms.caches;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public interface ObjectManager<T>{

    public T insert(T object) throws ConfigurationException;
}

package org.gcube.application.cms.plugins.faults;

public class StepException extends PluginExecutionException {

    public StepException() {
    }

    public StepException(String message) {
        super(message);
    }

    public StepException(String message, Throwable cause) {
        super(message, cause);
    }

    public StepException(Throwable cause) {
        super(cause);
    }

    public StepException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

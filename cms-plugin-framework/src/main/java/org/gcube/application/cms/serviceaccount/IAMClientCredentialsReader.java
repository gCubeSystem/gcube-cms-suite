package org.gcube.application.cms.serviceaccount;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.Collection;
import java.util.List;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class IAMClientCredentialsReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 3, 2024
 */
@Slf4j
public class IAMClientCredentialsReader {

	/**
	 * Gets the credentials.
	 *
	 * @param currentContext the current context
	 * @param seProfileName the se profile name
	 * @param seCategoryName the se category name
	 * @return the credentials
	 * @throws Exception the exception
	 */
	public static IAMClientCredentials getCredentials(String currentContext, String seProfileName,
			String seCategoryName) throws Exception {

		log.info("Searching SE in the scope: " + currentContext + " with profile name: " + seProfileName
				+ " and category name: " + seCategoryName);

		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Name/text() eq '" + seProfileName + "'");
		query.addCondition("$resource/Profile/Category/text() eq '" + seCategoryName + "'");

		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> resources = client.submit(query);

		if (resources.size() > 0)
			log.info("The query returned " + resources.size() + " ServiceEndpoint/s");
		else
			throw new RuntimeException("ServiceEndpoint not found. Searching for profile name '" + seProfileName
					+ "' and category name '" + seCategoryName + "' in the scope: " + currentContext);

		ServiceEndpoint se = resources.get(0);
		Collection<AccessPoint> theAccessPoints = se.profile().accessPoints().asCollection();
		String clientId = null;
		String secredPwd = null;
		for (AccessPoint accessPoint : theAccessPoints) {
			clientId = accessPoint.username();
			secredPwd = accessPoint.password();
			log.debug("Found clientId: " + clientId + " and encrypted secret: " + secredPwd);
			// decrypting the pwd
			try {
				if (secredPwd != null) {
					secredPwd = StringEncrypter.getEncrypter().decrypt(secredPwd);
					log.debug("Secret decrypted is: " + secredPwd.substring(0, secredPwd.length() / 2)
							+ "_MASKED_TOKEN_");
				}
			} catch (Exception e) {
				throw new RuntimeException("Error on decrypting the pwd: ", e);
			}
		}

		log.info("Returning keycloack credentials for SE {} read from SE", seProfileName);
		return new IAMClientCredentials(clientId, secredPwd);

	}

}

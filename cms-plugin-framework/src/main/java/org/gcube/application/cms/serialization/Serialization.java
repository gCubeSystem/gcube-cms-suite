package org.gcube.application.cms.serialization;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.gcube.application.cms.plugins.model.ComparableVersion;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.vdurmont.semver4j.Semver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Serialization {
	
	public static final DateTimeFormatter FULL_FORMATTER=DateTimeFormatter.ofPattern("uuuuMMdd_HH-mm-ss");
	
	public static ObjectMapper mapper;
		
		static {
			mapper=new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
			mapper.registerModule(new JavaTimeModule());

			SimpleModule s=new SimpleModule();
			s.addDeserializer(ObjectId.class,new ObjectIdDeserializer());
			s.addSerializer(ObjectId.class,new ObjectIdSerializer());
			
//			s.addDeserializer(ComparableVersion.class,new ComparableVersionDeserializer());
//			s.addSerializer(ComparableVersion.class,new ComparableVersionSerializer());

			s.addDeserializer(Semver.class,new SemverDeserializer());
			s.addSerializer(Semver.class,new SemverSerializer());

			mapper.registerModule(s);
		}
		
		public static <T> T read(String jsonString,Class<T> clazz) throws JsonProcessingException, IOException {
			return mapper.readerFor(clazz).readValue(jsonString);
		}

		public static <T> Iterator<T> readCollection(String jsonString, Class<T> clazz) throws IOException {
			return mapper.readerFor(clazz).readValues(jsonString);
		}


		public static String write(Object toWrite) throws JsonProcessingException {
			String toReturn= mapper.writeValueAsString(toWrite);
			return toReturn;
		}


		public static QueryRequest parseQuery(String queryString) throws IOException {
			log.trace("Parsing query Request {} ",queryString);
			Document queryDocument = Document.parse(queryString);
			log.trace("Document is ",queryDocument.toJson());
			QueryRequest req = new QueryRequest();
			if (queryDocument.getOrDefault("ordering",null)!=null)
				req.setOrdering(Serialization.read(((Document) queryDocument.get("ordering")).toJson(), QueryRequest.OrderedRequest.class));
			if (queryDocument.getOrDefault("paging",null)!=null)
				req.setPaging(Serialization.read(((Document) queryDocument.get("paging")).toJson(), QueryRequest.PagedRequest.class));
			req.setProjection(queryDocument.get("projection", Document.class));
			req.setFilter(queryDocument.get("filter", Document.class));
			return req;
		}


		//**** PROFILED DOCUMENTS

		public static final <T> T convert(Object d,Class<T> clazz){
			return mapper.convertValue(d,clazz);
		}
		
		public static final <T> T convertAcceptStringAsNull(Object d,Class<T> clazz){
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
			T converted = mapper.convertValue(d,clazz);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,false);
			return converted;
		}

		public static final Document asDocument(Object obj) throws JsonProcessingException {
			return Document.parse(mapper.writeValueAsString(obj));
		}

		public static final Document asDocumentWithId(Project doc) throws JsonProcessingException {
			Document toReturn =Document.parse(mapper.writeValueAsString(doc));
			if(doc.getId()!=null)
				toReturn.put(Project.ID,new ObjectId(doc.getId()));
			return toReturn;
		}
		
	// ***** Serialization Exceptions 

	// OBJECT ID 
	private static class ObjectIdSerializer extends JsonSerializer<ObjectId> {
		@Override
		public void serialize(ObjectId objectId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
			if (objectId == null) jsonGenerator.writeNull();
			else jsonGenerator.writeString(objectId.toString());
		}

		@Override
		public Class<ObjectId> handledType() {
			return ObjectId.class;
		}
	}
	private static class ObjectIdDeserializer extends JsonDeserializer<ObjectId> {

		@Override
		public ObjectId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
			String value=jsonParser.getValueAsString();
			if(value==null || value.isEmpty() || value.equals("null"))
				return null;
			else return new ObjectId(value);
		}
		@Override
		public Class<ObjectId> handledType() {
			return ObjectId.class;
		}
	}

	//Comparable Version
	private static class ComparableVersionSerializer extends JsonSerializer<ComparableVersion> {
		@Override
		public void serialize(ComparableVersion comparableVersion, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
			if (comparableVersion == null) jsonGenerator.writeNull();
			else jsonGenerator.writeString(comparableVersion.getCanonical());
		}

		@Override
		public Class<ComparableVersion> handledType() {
			return ComparableVersion.class;
		}
	}
	private static class ComparableVersionDeserializer extends JsonDeserializer<ComparableVersion> {

		@Override
		public ComparableVersion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
			String value=jsonParser.getValueAsString();
			if(value==null || value.isEmpty() || value.equals("null"))
				return null;
			else return new ComparableVersion(value);
		}
		@Override
		public Class<ComparableVersion> handledType() {
			return ComparableVersion.class;
		}
	}

	//Sem Version
	private static class SemverSerializer extends JsonSerializer<Semver> {
		@Override
		public void serialize(Semver semver, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
			if (semver == null) jsonGenerator.writeNull();
			else jsonGenerator.writeString(semver.getValue());
		}

		@Override
		public Class<Semver> handledType() {
			return Semver.class;
		}
	}
	private static class SemverDeserializer extends JsonDeserializer<Semver> {

		@Override
		public Semver deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
			String value=jsonParser.getValueAsString();
			if(value==null || value.isEmpty() || value.equals("null"))
				return null;
			else return new Semver(value);
		}
		@Override
		public Class<Semver> handledType() {
			return Semver.class;
		}
	}
}

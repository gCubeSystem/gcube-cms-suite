package org.gcube.application.cms.implementations.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gcube.application.geoportal.common.model.document.accounting.AccountingInfo;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserUtils {

	public static List<String> DEFAULT_ROLES = new ArrayList<>();

	public static AuthenticatedUser getCurrent() throws SecurityException {
		log.debug("Loading caller info..");

		SecretManager cm = SecretManagerProvider.instance.get();
		String context = cm.getContext();
		if (context == null)
			throw new SecurityException("Cannot determine context");
		Set<String> roles = new HashSet<>();
		org.gcube.common.authorization.utils.user.User user = cm.getUser();

		if (user == null) {
			log.warn("No user found in the session work, context is {}", context);
		} else {
			log.info("Identified caller {} in context {}", user.getUsername(), context);
			roles.addAll(user.getRoles());
		}
		AuthenticatedUser toReturn = new AuthenticatedUser(user, roles, AccessTokenProvider.instance.get(),
				SecurityTokenProvider.instance.get(), context);

		log.info("Current User is {} ", toReturn);
		return toReturn;
	}

	@AllArgsConstructor
	@Getter
	public static class AuthenticatedUser {

		private org.gcube.common.authorization.utils.user.User user;

		private Set<String> roles;

		private String uma_token;

		private String gcube_token;

		private String context;

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("User [user=");
			builder.append(user);
			builder.append(", uma_token=");
			builder.append(uma_token == null ? uma_token : "***");

			builder.append(", gcube_token=");
			builder.append(gcube_token == null ? gcube_token : "***");

			builder.append(", roles=");
			builder.append(roles);

			builder.append(", context=");
			builder.append(context);
			builder.append("]");
			return builder.toString();
		}

		public AccountingInfo asInfo() {
			AccountingInfo info = new AccountingInfo();
			User user = new User();
			try {
				user.setUsername(this.getUser().getUsername());
				user.setRoles(roles);
			} catch (Exception e) {
				log.warn("Unable to determine user id, using FAKE", e);
				user.setUsername("FAKE");
				user.setRoles(new HashSet<>());
				user.getRoles().addAll(DEFAULT_ROLES);
			}

			info.setUser(user);
			info.setInstant(LocalDateTime.now());
			Context c = new Context();
			c.setId(this.context);
			c.setName(context.contains("/") ? context.substring(context.lastIndexOf("/")) : context);
			info.setContext(c);
			return info;
		}

	}
}

package org.gcube.application.cms.plugins.reports;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InitializationReport extends Report{

    public InitializationReport(Status status, String... messages) {
        super(status, messages);
    }
}

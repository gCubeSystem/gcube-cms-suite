package org.gcube.application.cms.implementations;

import java.util.List;

import org.gcube.application.cms.caches.Engine;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.DatabaseConnection;
import org.gcube.application.geoportal.common.utils.ISUtils;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.ServiceEndpoint;

public class DefaultISProvider implements ISInterface, Engine<ISInterface> {


    @Override
    public DatabaseConnection queryForDatabase(String category, String platform, String flagName, String flagValue) throws ConfigurationException {
        return ISUtils.performQueryForDB(category, platform, flagName, flagValue);
    }

    @Override
    public List<ServiceEndpoint.AccessPoint> performGetAP(String category, String platform, String flagName, String flagValue) {
        return ISUtils.performGetAP(category, platform, flagName, flagValue);
    }

    @Override
    public String decryptString(String toDecrypt) {
        return ISUtils.decryptString(toDecrypt);
    }

    @Override
    public String encryptString(String toEncrypt) {
        return ISUtils.encryptString(toEncrypt);
    }

    @Override
    public List<GenericResource> getGenericResource(String secondaryType,String name) {
        return ISUtils.getGenericResources(secondaryType,name);
    }

    @Override
    public GenericResource createUpdateGR(GenericResource resource) {
        return  ISUtils.writeGR(resource);
    }

    // ** ENGINE

    @Override
    public void init() {

    }

    @Override
    public void shutdown() {

    }

    @Override
    public ISInterface getObject() throws ConfigurationException {
        return this;
    }
}

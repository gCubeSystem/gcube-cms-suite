package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.faults.MaterializationException;
import org.gcube.application.cms.plugins.reports.MaterializationReport;
import org.gcube.application.cms.plugins.requests.MaterializationRequest;

public interface MaterializationPlugin extends InitializablePlugin{

    public MaterializationReport materialize(MaterializationRequest request) throws MaterializationException, InvalidPluginRequestException;

    public MaterializationReport dematerialize(MaterializationRequest request) throws MaterializationException, InvalidPluginRequestException;

}

package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.faults.InitializationException;
import org.gcube.application.cms.plugins.faults.ShutDownException;
import org.gcube.application.cms.plugins.reports.InitializationReport;

public interface InitializablePlugin extends Plugin{
    /**
     * To be called once per context
     * @return
     * @throws InitializationException
     */

    public InitializationReport initInContext()throws InitializationException;

    /**
     * To be called for static initialization
     * @return
     * @throws InitializationException
     */
    public InitializationReport init()throws InitializationException;

    /**
     * To be called at application shutdown
     * @throws ShutDownException
     */
    public void shutdown() throws ShutDownException;
}

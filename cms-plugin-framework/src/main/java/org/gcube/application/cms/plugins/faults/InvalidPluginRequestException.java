package org.gcube.application.cms.plugins.faults;

public class InvalidPluginRequestException extends PluginExecutionException {
    public InvalidPluginRequestException() {
    }

    public InvalidPluginRequestException(String message) {
        super(message);
    }

    public InvalidPluginRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPluginRequestException(Throwable cause) {
        super(cause);
    }

    public InvalidPluginRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

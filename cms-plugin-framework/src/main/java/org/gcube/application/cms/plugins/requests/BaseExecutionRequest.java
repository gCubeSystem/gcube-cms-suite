package org.gcube.application.cms.plugins.requests;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class BaseExecutionRequest extends BaseRequest{


    Project document;

    public BaseExecutionRequest(@NonNull UseCaseDescriptor useCaseDescriptor, @NonNull User caller, @NonNull Context context, Project document) {
        super(useCaseDescriptor, caller, context);
        this.document = document;
    }

    public BaseExecutionRequest validate() throws InvalidPluginRequestException {
        super.validate();
        if(document==null) throw new InvalidPluginRequestException("Document cannot be null");
        return this;
    }
}

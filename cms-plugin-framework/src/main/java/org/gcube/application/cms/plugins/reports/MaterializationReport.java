package org.gcube.application.cms.plugins.reports;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.requests.MaterializationRequest;

import lombok.NonNull;


public class MaterializationReport extends DocumentHandlingReport<MaterializationRequest>{

    public MaterializationReport(@NonNull MaterializationRequest theRequest) throws InvalidPluginRequestException {
        super(theRequest);
    }
}

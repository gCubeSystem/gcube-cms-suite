package org.gcube.application.cms.implementations.faults;

public class UnauthorizedAccess extends Exception {

    public UnauthorizedAccess() {
    }

    public UnauthorizedAccess(String message) {
        super(message);
    }

    public UnauthorizedAccess(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedAccess(Throwable cause) {
        super(cause);
    }

    public UnauthorizedAccess(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

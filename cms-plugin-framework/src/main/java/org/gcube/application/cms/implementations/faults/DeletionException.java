package org.gcube.application.cms.implementations.faults;

public class DeletionException extends Exception {

    public DeletionException() {
    }

    public DeletionException(String message) {
        super(message);
    }

    public DeletionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeletionException(Throwable cause) {
        super(cause);
    }

    public DeletionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

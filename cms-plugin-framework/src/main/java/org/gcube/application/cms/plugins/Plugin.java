package org.gcube.application.cms.plugins;

import org.gcube.application.geoportal.common.model.plugins.PluginDescriptor;

public interface Plugin {

    public PluginDescriptor getDescriptor();
}

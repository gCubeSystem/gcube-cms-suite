package org.gcube.application.cms.plugins.events;

public interface EventListener<T extends ItemObservable> {
	
	public void updated(T observerd);

}

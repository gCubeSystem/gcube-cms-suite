package org.gcube.application.cms.implementations.faults;

public class InvalidLockException extends Exception{
    public InvalidLockException() {
    }

    public InvalidLockException(String message) {
        super(message);
    }

    public InvalidLockException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidLockException(Throwable cause) {
        super(cause);
    }

    public InvalidLockException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

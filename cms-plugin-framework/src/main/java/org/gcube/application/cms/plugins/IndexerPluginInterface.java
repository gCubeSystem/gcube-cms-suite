package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.reports.IndexDocumentReport;
import org.gcube.application.cms.plugins.requests.BaseRequest;
import org.gcube.application.cms.plugins.requests.IndexDocumentRequest;
import org.gcube.application.geoportal.common.model.configuration.Index;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public interface IndexerPluginInterface extends InitializablePlugin{

    public IndexDocumentReport index(IndexDocumentRequest request) throws InvalidPluginRequestException;
    public IndexDocumentReport deindex(IndexDocumentRequest request) throws InvalidPluginRequestException;

    public Index getIndex(BaseRequest request) throws ConfigurationException;



}

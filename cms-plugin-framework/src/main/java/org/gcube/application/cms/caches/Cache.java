package org.gcube.application.cms.caches;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public interface Cache<K,V>{

    public V get(K key) throws ConfigurationException;
}

package org.gcube.application.cms.plugins.faults;

public class UnrecognizedStepException extends StepException{
    public UnrecognizedStepException() {
    }

    public UnrecognizedStepException(String message) {
        super(message);
    }

    public UnrecognizedStepException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnrecognizedStepException(Throwable cause) {
        super(cause);
    }

    public UnrecognizedStepException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

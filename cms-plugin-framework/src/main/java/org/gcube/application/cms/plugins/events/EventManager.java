package org.gcube.application.cms.plugins.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportal.common.model.document.Project;

/**
 * The Class EventManager.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 11, 2023
 */
public class EventManager implements EventManagerInterface<ItemObserved<Project>> {

	private HashMap<Event, List<EventListener<ItemObserved<Project>>>> listeners = new HashMap<Event, List<EventListener<ItemObserved<Project>>>>();

	/**
	 * The Class Event.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 29, 2023
	 */
	public static enum Event {

		PROJECT_CREATED("PROJECT_CREATED"), 
		PROJECT_UPDATED("PROJECT_UPDATED"), 
		PROJECT_DELETED("PROJECT_DELETED"),
		LIFECYCLE_STEP_PERFORMED("LIFECYCLE_STEP_PERFORMED");

		String name;

		/**
		 * Instantiates a new event.
		 *
		 * @param name the name
		 */
		Event(String name) {
			this.name = name;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

	}

	// private instance, so that it can be
	// accessed by only by getInstance() method
	private static EventManager instance;

	/**
	 * Instantiates a new event manager.
	 */
	private EventManager() {
		// private constructor
	}

	/**
	 * Gets the single instance of EventManager.
	 *
	 * @return single instance of EventManager
	 */
	public static EventManager getInstance() {
		if (instance == null) {
			// synchronized block to remove overhead
			synchronized (EventManager.class) {
				if (instance == null) {
					// if instance is null, initialize
					instance = new EventManager();
				}

			}
		}
		return instance;
	}

	/**
	 * Subscribe.
	 *
	 * @param eventType the event type
	 * @param listener  the listener
	 */
	public void subscribe(Event eventType, EventListener<ItemObserved<Project>> listener) {
		List<EventListener<ItemObserved<Project>>> list = listeners.get(eventType);
		if (list == null)
			list = new ArrayList<EventListener<ItemObserved<Project>>>();

		list.add(listener);
		listeners.put(eventType, list);
	}

	/**
	 * Unsubscribe.
	 *
	 * @param eventType the event type
	 * @param listener  the listener
	 */
	public void unsubscribe(Event eventType, EventListener<ItemObserved<Project>> listener) {
		List<EventListener<ItemObserved<Project>>> list = listeners.get(eventType);
		if (list == null)
			return;

		list.remove(listener);
		listeners.put(eventType, list);
	}

	/**
	 * Notify.
	 *
	 * @param eventType the event type
	 * @param item      the item
	 */
	public void notify(Event eventType, ItemObserved<Project> item) {
		for (EventManager.Event event : listeners.keySet()) {
			if (event.equals(eventType)) {
				for (EventListener<ItemObserved<Project>> eventListner : listeners.get(eventType)) {
					eventListner.updated(item);
				}
			}
		}
	}

}

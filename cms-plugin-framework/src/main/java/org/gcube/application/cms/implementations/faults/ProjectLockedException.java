package org.gcube.application.cms.implementations.faults;

public class ProjectLockedException extends Exception{
    public ProjectLockedException() {
    }

    public ProjectLockedException(String message) {
        super(message);
    }

    public ProjectLockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProjectLockedException(Throwable cause) {
        super(cause);
    }

    public ProjectLockedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

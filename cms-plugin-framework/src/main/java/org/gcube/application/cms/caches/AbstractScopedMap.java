package org.gcube.application.cms.caches;

import org.gcube.application.cms.implementations.utils.UserUtils;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * TTL Map Context -> T
 *
 */
public abstract class AbstractScopedMap<T> extends TimedMap<String,T> implements Engine<T>{

	public AbstractScopedMap(@NonNull String name) {
		super(name);
	}

	@Override
	public T getObject() throws ConfigurationException {
		UserUtils.AuthenticatedUser user = UserUtils.getCurrent();
		String context = user.getContext();
		log.debug(" {} : Accessing object under context {} ",name,context);
		return get(context);
	}


	@Override
	public void init() {}

	@Override
	public void shutdown() {
		log.warn(name + ": shutting down");
		scopeMap.forEach((String s,TTLObject<T> o)->{
			try{if(o!=null&&o.getTheObject()!=null)
				dispose(o.getTheObject());
			}catch(Throwable t) {
				log.warn(name +": unable to dispose ",t);
			}
		});
	}

}

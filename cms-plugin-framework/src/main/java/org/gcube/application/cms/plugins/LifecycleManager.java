package org.gcube.application.cms.plugins;

import org.gcube.application.cms.plugins.faults.EventException;
import org.gcube.application.cms.plugins.faults.InsufficientPrivileges;
import org.gcube.application.cms.plugins.faults.InvalidPluginRequestException;
import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.cms.plugins.faults.StepException;
import org.gcube.application.cms.plugins.reports.EventExecutionReport;
import org.gcube.application.cms.plugins.reports.StepExecutionReport;
import org.gcube.application.cms.plugins.requests.BaseRequest;
import org.gcube.application.cms.plugins.requests.EventExecutionRequest;
import org.gcube.application.cms.plugins.requests.StepExecutionRequest;
import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;


public interface LifecycleManager extends InitializablePlugin{

    // Lifecycle operations

    public StepExecutionReport performStep(StepExecutionRequest request) throws StepException, InvalidPluginRequestException, InvalidProfileException, ConfigurationException, InsufficientPrivileges;

    public Configuration getCurrentConfiguration(BaseRequest request) throws ConfigurationException;

    public EventExecutionReport onEvent(EventExecutionRequest request) throws EventException, InvalidPluginRequestException;

    public void setPluginManager(PluginManagerInterface manager);
}

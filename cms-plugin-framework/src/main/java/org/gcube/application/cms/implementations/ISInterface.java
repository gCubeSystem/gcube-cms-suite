package org.gcube.application.cms.implementations;

import java.util.List;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.DatabaseConnection;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.ServiceEndpoint;

public interface ISInterface {

    public DatabaseConnection queryForDatabase(String category, String platform,String flagName, String flagValue) throws ConfigurationException;
    public List<ServiceEndpoint.AccessPoint> performGetAP(String category, String platform, String flagName, String flagValue);
    public String decryptString(String toDecrypt);
    public String encryptString(String toEncrypt);

    public List<GenericResource> getGenericResource(String secondaryType,String name);


    public GenericResource createUpdateGR(GenericResource res);
}

package org.gcube.application.cms.plugins.requests;

import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.Context;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class EventExecutionRequest extends BaseExecutionRequest{

    public static class Events{
        public static final String ON_INIT_DOCUMENT="INIT_DOCUMENT";
        public static final String ON_UPDATE_DOCUMENT="UPDATE_DOCUMENT";
        public static final String ON_DELETE_DOCUMENT="DELETE_DOCUMENT";
        public static final String ON_DELETE_FILESET="DELETE_FILESET";
    }

    public EventExecutionRequest(@NonNull UseCaseDescriptor useCaseDescriptor, @NonNull User caller, @NonNull Context context, Project document, String event) {
        super(useCaseDescriptor, caller, context, document);
        this.event = event;
    }

    String event;


}

package org.gcube.application.cms.plugins.faults;

public class UnrecognizedEventException extends EventException{
    public UnrecognizedEventException() {
    }

    public UnrecognizedEventException(String message) {
        super(message);
    }

    public UnrecognizedEventException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnrecognizedEventException(Throwable cause) {
        super(cause);
    }

    public UnrecognizedEventException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

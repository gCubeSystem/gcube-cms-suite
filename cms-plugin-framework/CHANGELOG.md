# Changelog for org.gcube.application.cms-plugin-framework

## [v1.0.6] - 2024-10-01

- Included the file size to reduce/optimize the time to upload files to the storage hub [#28150]
- Checked if the user is `null` in the `UserUtils` class [#28301]

## [v1.0.5] - 2024-07-03

- Implemented Event Broker [#26321]
- Added Geoportal Service Account management

## [v1.0.4] - 2023-09-06

- Using parent range version [#25572]

## [v1.0.3] - 2023-01-10
- UserUtils in framework

## [v1.0.2] 2022-01-17
- Fixes #2435
- Introduced module default-lc-managers

## [v1.0.1] 2022-01-17
- Serialization Features

## [v1.0.0] 2021-09-20
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.cms.sdi-plugins

## [v1.1.3]
- Added apply regex business logic [#26322]
- Added reconnection attempts to DB on DB connection failure [#26322]
- Improved logs
- Added fallback on /geoserver/rest path [#28150#note-8]
- Improved some business logics

## [v1.1.2]
- Using parent version range [#25572]

## [v1.1.1]
- Improved some logs
- Fixed: Centroid Object registered in the Project [#25056]

## [v1.1.0]
- Integrated the field 'geov_link' (Geoportal GisViewer link) in the centroid layer [#24859]

## [v1.0.4] - 2023-03-06
- Fixed the import from joda-time to java.time
- [#24702] Fixed the default-lc-managers dependency
- Fixed log 

## [v1.0.3] - 2023-01-24
- Fixes [#24476](https://support.d4science.org/issues/24476)

## [v1.0.2] - 2023-01-10
- Pom updates

## [v1.0.1] - 2022-12-07
- Pom updates
- Introduced module default-lc-managers

## [v1.0.0] - 2022-02-24
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

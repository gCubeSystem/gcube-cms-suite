gCube CMS Suite : SDI Plugins
--------------------------------------------------

This module contains definition of the following plugins :
- SDIIndexerPlugin : indexes Projects in centroid layers in gCube SDI (requires PostGIS DB)
- SDIMaterializerPlugin : materializes layers in gCube SDI (currenlty supported TIF and SHP)

It uses the SDI in the context of the caller, and requires : 
- GeoServer
- Postgis DB registered in Geoserver (indexing only)


## Built with
* [gCube SDI] (https://gcube.wiki.gcube-system.org/gcube/) - The gCube SDI
* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation
[gCube CMS Suite](../) parent module containing references, documentation, guides ad utilities.


## Change log

See [CHANGELOG.md](CHANGELOG.md).
## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.
 
## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - iMarine(grant no. 283644);
    - EUBrazilOpenBio (grant no. 288754).
- the H2020 research and innovation programme 
    - SoBigData (grant no. 654024);
    - PARTHENOS (grant no. 654119);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - BlueBRIDGE (grant no. 675680);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);


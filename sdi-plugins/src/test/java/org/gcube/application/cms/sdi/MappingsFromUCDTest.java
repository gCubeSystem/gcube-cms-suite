package org.gcube.application.cms.sdi;

import static org.junit.Assume.assumeTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.cms.sdi.engine.PostgisTable;
import org.gcube.application.cms.sdi.faults.SDIInteractionException;
import org.gcube.application.cms.sdi.model.MappingObject;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.model.BasicTests;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

public class MappingsFromUCDTest extends BasicTests {

	@Test
	public void testReadMappingsFromUCD()
			throws SDIInteractionException, ConfigurationException, SQLException, InvalidProfileException {
		assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor ucd = TestProfiles.profiles.get("profiledConcessioni");

		System.out.println("ucd: " + ucd);

		List<MappingObject> mappingObjects = MappingObject.getMappingsFromUCD(ucd, Constants.INDEXER_PLUGIN_ID);

		String toPrint = mappingObjects.stream().map(mo -> String.valueOf(mo)).collect(Collectors.joining("\n"));

		System.out.println(toPrint);

		List<PostgisTable.Field> fields = PostgisTable.Field.fromMappings(mappingObjects);

		for (PostgisTable.Field field : fields) {
			System.out.println(field);
		}
	}

}

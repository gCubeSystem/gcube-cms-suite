package org.gcube.application.cms.sdi.plugins;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;

import org.bson.Document;
import org.gcube.application.cms.plugins.faults.PluginExecutionException;
import org.gcube.application.cms.plugins.reports.MaterializationReport;
import org.gcube.application.cms.plugins.reports.Report;
import org.gcube.application.cms.plugins.requests.MaterializationRequest;
import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.cms.tests.TestDocuments;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.plugins.BasicPluginTest;
import org.gcube.application.geoportal.common.faults.StorageException;
import org.gcube.application.geoportal.common.model.JSONPathWrapper;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.filesets.RegisteredFile;
import org.gcube.application.geoportal.common.model.document.filesets.RegisteredFileSet;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class MaterializerTests extends BasicPluginTest {

    @Test
    public void testShape() throws JsonProcessingException, PluginExecutionException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Project doc= TestDocuments.documentMap.get("registeredGeoTif.json");
       // doc.setProfileID("sdi-tests");
        materialize(doc);

    }

    @Test
    public void testFileNames() throws JsonProcessingException, PluginExecutionException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Project doc= TestDocuments.documentMap.get("timponeFull.json");
        // doc.setProfileID("sdi-tests");
        materialize(doc);
    }

    @Test
    public void download() throws ConfigurationException, IOException, StorageException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Project doc= TestDocuments.documentMap.get("registeredGeoTif.json");
        for(Object o : new JSONPathWrapper(doc.getTheDocument().toJson()).getByPath("$..fileset")){
            RegisteredFileSet fs = Serialization.convert(o,RegisteredFileSet.class);
            for(Object oo: fs.getPayloads()){
                RegisteredFile f = Serialization.convert(oo,RegisteredFile.class);
                File file = Files.downloadFromUrl(f.getName(),f.getLink());
//                Files.deleteIfExists(file.toPath());
            }
        }
    }



    private MaterializationReport materialize(Project project) throws PluginExecutionException, JsonProcessingException {
        MaterializationRequest req=new MaterializationRequest(
                TestProfiles.profiles.get(project.getProfileID()),getCurrentUser(),getTestContext(),project);

        Document parameters = new Document();
        parameters.put("workspace", "tests_workspace");
        req.setCallParameters(parameters);

        SDIMaterializerPlugin p = (SDIMaterializerPlugin) plugins.get(SDIMaterializerPlugin.DESCRIPTOR.getId());
        MaterializationReport toReturn = p.materialize(req);
        assertTrue(toReturn!=null);
        toReturn.validate();
        System.out.println("Response is "+ Serialization.write(toReturn));

        assertTrue(toReturn.getStatus().equals(Report.Status.OK));
        return toReturn;
    }
}

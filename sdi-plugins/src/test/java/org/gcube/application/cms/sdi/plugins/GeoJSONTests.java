package org.gcube.application.cms.sdi.plugins;

import java.io.IOException;

import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GCubeSDILayer;
import org.gcube.application.geoportal.common.model.document.identification.SpatialReference;
import org.geojson.Crs;
import org.geojson.GeoJsonObject;
import org.geojson.LngLatAlt;
import org.geojson.Point;
import org.junit.Test;

public class GeoJSONTests {

    @Test
    public void checkFullCircle() throws IOException {
        Point point = new Point();
        point.setCoordinates(new LngLatAlt(13,12,0));
        point.setCrs(new Crs());
        point.setBbox(GCubeSDILayer.BBOX.WORLD.asGeoJSONArray());

        String value = Serialization.write(point);
        System.out.println("String is  "+value);
        GeoJsonObject obj = Serialization.read(value, GeoJsonObject.class);
        System.out.println("OBJ is "+obj);
        obj = Serialization.convert(point,GeoJsonObject.class);
        System.out.println("Converted obj is "+obj);

        SpatialReference reference =new SpatialReference(Serialization.asDocument(point));
        String referenceString= Serialization.write(reference);
        System.out.println("Serialized reference is "+referenceString);
        reference=Serialization.read(referenceString,SpatialReference.class);

        System.out.println("Deserialized reference is "+reference);
        obj = Serialization.convert(reference.getGeoJson(),GeoJsonObject.class);
        System.out.println("Converted from spatial reference is "+obj);

    }
}

package org.gcube.application.cms.sdi.plugins;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.Arrays;

import org.bson.Document;
import org.gcube.application.cms.plugins.IndexerPluginInterface;
import org.gcube.application.cms.plugins.faults.PluginExecutionException;
import org.gcube.application.cms.plugins.reports.IndexDocumentReport;
import org.gcube.application.cms.plugins.reports.Report;
import org.gcube.application.cms.plugins.requests.BaseRequest;
import org.gcube.application.cms.plugins.requests.IndexDocumentRequest;
import org.gcube.application.cms.sdi.engine.PostgisIndexer;
import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.cms.tests.TestDocuments;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.plugins.BasicPluginTest;
import org.gcube.application.geoportal.common.model.configuration.Index;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.filesets.Materialization;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GCubeSDILayer;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GeoServerPlatform;
import org.gcube.application.geoportal.common.model.document.identification.SpatialReference;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class IndexerTest extends BasicPluginTest {

    @Test
    public void testIndexRequest() throws PluginExecutionException, JsonProcessingException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Project doc= TestDocuments.documentMap.get("dummy.json");
        doc.setProfileID("sdi-tests");

        doc.getTheDocument().put("coordX",10d);
        doc.getTheDocument().put("coordY",11d);

        doc.getTheDocument().put("nome", Arrays.asList("My Little Test","Detto test"));
        testIndexing(doc);
    }


    @Test
    public void testMosiIndexing() throws JsonProcessingException, PluginExecutionException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Project doc= TestDocuments.documentMap.get("mosiDoc.json");
        testIndexing(doc);
    }



    private IndexDocumentReport testIndexing(Project doc) throws PluginExecutionException, JsonProcessingException {
        IndexDocumentRequest request=new IndexDocumentRequest(TestProfiles.profiles.get(doc.getProfileID()),
                getCurrentUser(),getTestContext(),doc);
        Document parameters = new Document();
        parameters.put("workspace", "testing_workspace");
        parameters.put("indexName", "unique_index"+System.currentTimeMillis());
        request.setCallParameters(parameters);

        IndexerPluginInterface plugin = (IndexerPluginInterface) plugins.get(SDIIndexerPlugin.DESCRIPTOR.getId());

        IndexDocumentReport response = plugin.index(request);
        assertTrue(response!=null);
        response.validate();
        System.out.println("Response is "+Serialization.write(response));

        assertTrue(response.getStatus().equals(Report.Status.OK));
        assertTrue(response.prepareResult().getIdentificationReferenceByType(SpatialReference.SPATIAL_REFERENCE_TYPE).size()==1);
        return response;
    }



    @Test
    public void getIndex() throws ConfigurationException, JsonProcessingException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        IndexerPluginInterface plugin = (IndexerPluginInterface) plugins.get(SDIIndexerPlugin.DESCRIPTOR.getId());
        UseCaseDescriptor descriptor=TestProfiles.profiles.get("profiledConcessioni");

        Index index = plugin.getIndex(new BaseRequest(descriptor,getCurrentUser(),getTestContext())
                .setParameter("workspace", Files.fixFilename(GCubeTest.getContext()+"_test-ws"))
                .setParameter("indexName",Files.fixFilename(GCubeTest.getContext()+"test_index")));
        System.out.println("Test Index Is "+index);
        assertEquals(index.getType(), PostgisIndexer.INDEX_TYPE);
        assertNotNull(index.get("layer"));
        assertNotNull(index.get("indexName"));
        GCubeSDILayer layer = Serialization.convert(index.get("layer"),GCubeSDILayer.class);
        assertEquals(GCubeSDILayer.GCUBE_SDY_LAYER_TYPE,layer.getType());

        for (Object pIObj : layer.getPlatformInfo()){
            Document platformDoc = Serialization.asDocument(pIObj);
            assertEquals(GeoServerPlatform.GS_PLATFORM,platformDoc.get(Materialization.TYPE));
        }

        System.out.println("OGC_LINKS : ");
        layer.getOGCLinks().forEach((k,v) ->{
            System.out.println(k + "\t:\t"+v);
        });

    }

}

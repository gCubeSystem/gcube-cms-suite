package org.gcube.application.cms.sdi;

import static org.junit.Assume.assumeTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.cms.sdi.engine.PostgisTable;
import org.gcube.application.cms.sdi.faults.SDIInteractionException;
import org.gcube.application.cms.sdi.model.MappingObject;
import org.gcube.application.cms.sdi.plugins.SDIIndexerPlugin;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

/**
 * The Class MappingObjectRegexTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jan 8, 2024
 */
public class MappingObjectRegexTest {

	String test = "paleolitico inferiore (da 2.000.000 a.C. al 200.000 a.C.)";

	/**
	 * Test apply regex.
	 *
	 * @throws SDIInteractionException the SDI interaction exception
	 * @throws ConfigurationException the configuration exception
	 * @throws SQLException the SQL exception
	 * @throws InvalidProfileException the invalid profile exception
	 * @throws InterruptedException the interrupted exception
	 */
	//@Test
	public void testApplyRegex() throws SDIInteractionException, ConfigurationException, SQLException,
			InvalidProfileException, InterruptedException {
		assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor ucd = TestProfiles.profiles.get("profiledConcessioni");

		System.out.println("ucd: " + ucd);

		List<MappingObject> mappingObjects = MappingObject.getMappingsFromUCD(ucd, Constants.INDEXER_PLUGIN_ID);

		String toPrint = mappingObjects.stream().map(mo -> String.valueOf(mo)).collect(Collectors.joining("\n"));

		System.out.println(toPrint);

		for (MappingObject mappingObject : mappingObjects) {
			Object toSetValue = SDIIndexerPlugin.toSetValueByApplyRegex(mappingObject, test);
			System.out.println("MappingObject '" + mappingObject.getName() + "' toSetValue is: " + toSetValue);
		}

		List<PostgisTable.Field> fields = PostgisTable.Field.fromMappings(mappingObjects);

		for (PostgisTable.Field field : fields) {
			System.out.println(field);
		}
	}

}

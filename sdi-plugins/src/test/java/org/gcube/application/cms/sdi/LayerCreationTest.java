package org.gcube.application.cms.sdi;

import static org.junit.Assume.assumeTrue;

import java.sql.SQLException;
import java.time.Instant;
import java.util.List;

import org.gcube.application.cms.implementations.ISInterface;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.cms.sdi.engine.PostgisIndexer;
import org.gcube.application.cms.sdi.engine.PostgisTable;
import org.gcube.application.cms.sdi.engine.SDIManagerWrapper;
import org.gcube.application.cms.sdi.faults.SDIInteractionException;
import org.gcube.application.cms.sdi.model.MappingObject;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.model.BasicTests;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.DatabaseConnection;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

public class LayerCreationTest extends BasicTests {
    
    @Test
    public void testCreateIndexLayer() throws SDIInteractionException, ConfigurationException, SQLException, InvalidProfileException {
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        UseCaseDescriptor ucd =  TestProfiles.profiles.get("concessioni");
        DatabaseConnection db =  ImplementationProvider.get().getProvidedObjectByClass(ISInterface.class).
                queryForDatabase("Database","postgis", "GNA_DB","Concessioni");

        SDIManagerWrapper wrapper = new SDIManagerWrapper();

        PostgisIndexer indexer = new PostgisIndexer(wrapper, ucd, db);

        List<MappingObject> mappingObjects = MappingObject.getMappingsFromUCD(ucd, Constants.INDEXER_PLUGIN_ID);
        List<PostgisTable.Field> fields = PostgisTable.Field.fromMappings(mappingObjects);

        String indexName = "test_"+PostgisTable.sanitizeFieldName(Instant.now().toString());
        String ws = "testWS";


        indexer.initIndex(indexName,
                fields,
                ws,
                indexName);
        System.out.println("Done "+ws);
    }



}

package org.gcube.application.cms.sdi.model;

import java.util.ArrayList;

import org.gcube.application.geoportal.common.model.document.filesets.RegisteredFile;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
@Getter
@RequiredArgsConstructor
public abstract class SupportedFormat {

    public static ArrayList<SupportedFormat> getByExtension(String... extensions){
        ArrayList<SupportedFormat> toReturn = new ArrayList<>();

        for (String extension : extensions) {
            extension=extension.toLowerCase();
            switch (extension){
                case ".tif" : {toReturn.add(new SupportedFormat(".tif") {
                    @Override
                    public void consider(RegisteredFile f) {
                             if (f.getName().toLowerCase().endsWith(getFileExtension())){
                                 getToUseFileSet().add(f);
                                 isProposedFilesetValid=true;
                             }
                    }
                });
                    break;
                }
                case ".shp":{toReturn.add(new SupportedFormat(".shp") {
                    @Override
                    public void consider(RegisteredFile f) {
                        getToUseFileSet().add(f);
                        if (f.getName().toLowerCase().endsWith(getFileExtension())){
                            isProposedFilesetValid=true;
                        }
                    }
                });
                    break;

                }
            }
        }

        return toReturn;
    }

    @NonNull
    private String fileExtension;

    protected Boolean isProposedFilesetValid=false;

    private ArrayList<RegisteredFile> toUseFileSet = new ArrayList<>();

    public abstract void consider(RegisteredFile f);


    @Override
    public String toString() {
        return fileExtension ;
    }
}

package org.gcube.application.cms.sdi.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GCubeSDILayer;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GeoServerPlatform;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.PlatformInfo;

import lombok.Getter;

public class GCubeSDILayerBuilder {

    public static enum OGC_TYPE{
        wms,wfs,wcs,csw
    }


    public static final String EPSG_4326="EPSG:4326";
    public static final String WGS84_FULL="GEOGCS[\"WGS 84\", DATUM[\"World Geodetic System 1984\", SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]],"+
            "AUTHORITY[\"EPSG\",\"6326\"]], PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]],  UNIT[\"degree\", 0.017453292519943295],"+
            "AXIS[\"Geodetic longitude\", EAST],  AXIS[\"Geodetic latitude\", NORTH],  AUTHORITY[\"EPSG\",\"4326\"]]";



    GCubeSDILayer theObject = new GCubeSDILayer();

    @Getter
    GeoServerPlatform platformInfo= new GeoServerPlatform();

    @Getter
    GCubeSDILayer.BBOX bbox = GCubeSDILayer.BBOX.WORLD;

    @Getter
    Map<OGC_TYPE,Document> ogcLinks = new HashMap<>();

    public GCubeSDILayerBuilder(){
        platformInfo.put(PlatformInfo.TYPE,GeoServerPlatform.GS_PLATFORM);
    }


    public GCubeSDILayer getLayer(){
        theObject.put(GCubeSDILayer.PLATFORM_INFO, Collections.singleton(platformInfo));
        theObject.put(GCubeSDILayer.B_BOX,bbox);

        prepareOGCLinks();
        final Document finalLinkDocument = new Document();
        ogcLinks.forEach( (type,document) -> {
            finalLinkDocument.putAll(document);
        });
        theObject.put(GCubeSDILayer.OGC_LINKS,finalLinkDocument);
        return theObject;
    }


    // @@@@@@@@@@@@@@@@@@  Platform info
    public GCubeSDILayerBuilder setWorkspace(String ws){
        platformInfo.put(GeoServerPlatform.WORKSPACE,ws);
        return this;
    }

    public GCubeSDILayerBuilder setHost(String ws){
        platformInfo.put(PlatformInfo.HOST,ws);
        return this;
    }

    public GCubeSDILayerBuilder setEngineVersion(String ws){
        platformInfo.put(PlatformInfo.ENGINE_VERSION,ws);
        return this;
    }

    public GCubeSDILayerBuilder setLayerName(String ws){
        platformInfo.put(GeoServerPlatform.LAYER_NAME,ws);
        return this;
    }

    public GCubeSDILayerBuilder setPersistencePath(String ws){
        platformInfo.put(GeoServerPlatform.PERSISTENCE_PATH,ws);
        return this;
    }

    public GCubeSDILayerBuilder setStoreName(String ws){
        platformInfo.put(GeoServerPlatform.STORENAME,ws);
        return this;
    }

    public GCubeSDILayerBuilder setFiles(List<String> ws){
        platformInfo.put(GeoServerPlatform.FILES,ws);
        return this;
    }


    private void prepareOGCLinks(){
        if(!ogcLinks.containsKey(OGC_TYPE.wms)){
            addLink(OGC_TYPE.wms,String.format("https://%1$s/geoserver/%2$s/wms?"
                            + "service=WMS&version=1.1.0&request=GetMap&layers=%2$s:%3$s&"
                            + "styles=&bbox=%4$f,%5$f,%6$f,%7$f&srs=%8$s&format=application/openlayers&width=%9$d&height=%10$d",
                    platformInfo.getHost(),
                    platformInfo.getWorkspace(),
                    platformInfo.getLayerName(),
                    bbox.getMinX(),
                    bbox.getMinY(),
                    bbox.getMaxX(),
                    bbox.getMaxY(),
                    EPSG_4326,
                    400,
                    400));
        }
    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@ OGC
    public GCubeSDILayerBuilder addLink(OGC_TYPE type,String link){
        ogcLinks.put(type,new Document(type.name(),link));
        return this;
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BBOX

    public GCubeSDILayerBuilder setBBOX(Double maxX, Double minX, Double maxY, Double minY, Double maxZ, Double minZ){
        bbox=new GCubeSDILayer.BBOX(maxX,minX,maxY,minY,maxZ,minZ);
        return this;
    }

}

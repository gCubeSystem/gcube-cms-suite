package org.gcube.application.cms.sdi.faults;

public class DataParsingException extends Exception {

    public DataParsingException() {
    }

    public DataParsingException(String message) {
        super(message);
    }

    public DataParsingException(Throwable cause) {
        super(cause);
    }

    public DataParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataParsingException(String message, Throwable cause, boolean enableSuppression,
                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);

    }

}

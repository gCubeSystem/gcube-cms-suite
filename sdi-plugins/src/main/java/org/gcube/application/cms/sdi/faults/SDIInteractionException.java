package org.gcube.application.cms.sdi.faults;

public class SDIInteractionException extends Exception {

    public SDIInteractionException() {
    }

    public SDIInteractionException(String message) {
        super(message);
    }

    public SDIInteractionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SDIInteractionException(Throwable cause) {
        super(cause);
    }

    public SDIInteractionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

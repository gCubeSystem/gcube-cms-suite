package org.gcube.application.cms.sdi.engine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface PostgisDBManagerI {

    void commit() throws SQLException;





    void create(PostgisTable toCreate) throws SQLException;
    PreparedStatement prepareInsertStatement(PostgisTable target, boolean createTable, boolean geometryAsText)
            throws SQLException;

    PreparedStatement prepareStatement(String stmt) throws SQLException;


    void deleteTable(String tableName) throws SQLException;
    void truncate(String tableName) throws SQLException;
    int deleteByFieldValue(PostgisTable target, PostgisTable.Field field, Object value) throws SQLException;

    long count(PostgisTable table)throws SQLException;


  //  DatabaseConnection getConnectionDescriptor();

//    PostgisTable.POINT evaluateCentroid(PostgisTable table) throws SQLException, DataParsingException;


    ResultSet queryAll(PostgisTable table) throws SQLException;


}

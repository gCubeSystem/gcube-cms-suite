package org.gcube.application.cms.sdi.engine;

import java.util.ArrayList;

import org.gcube.application.cms.sdi.engine.PostgisTable.Field;
import org.gcube.application.cms.sdi.engine.PostgisTable.FieldType;
import org.gcube.application.cms.sdi.engine.PostgisTable.GeometryType;

public class DBConstants {

    public static enum TYPE{

        Concessioni,Mosi,Mopr
    }

    public static class Defaults{
        public static final String DEFAULT_GEOMETRY_COLUMN_NAME="geom";
        public static final String INTERNAL_ID="internal_id";

        public static final String PROJECT_ID="projectid";

        public static final String DISPLAYED="displayed_project";
        
        public static final String GEOVIEWER_LINK_FIELD="geov_link";


        public static final String XCOORD_FIELD="xcoord";
        public static final String YCOORD_FIELD="ycoord";
    }



    /**
     * nome,anno,regione,xcentroid,ycentroid,csv,shp,
     * geopackage,nome_csv,nome_shp,nome_geo,
     * poligono,punti,linee,
     * nome_progetto, descrizione_progetto,descrizione_contenuto,autore,contributore,
     * titolare_dati,responsabile,editore,
     * finanziamento,soggetto,
     * risorse_correlate,
     * date_scavo,data_archiviazione,
     * versione,licenza,titolare_licenza_copyright,accesso_dati,parole_chiave
     *
     * @author FabioISTI
     *
     */
    public static class Concessioni{

        public static final String PRODUCT_ID="product_id";
        public static final String NOME="nome";
        public static final String ANNO="anno";
        public static final String REGIONE="regione";
        public static final String GEOMETRY= Defaults.DEFAULT_GEOMETRY_COLUMN_NAME;

        //Extension
        public static final String DESCRIZIONE="descrizione";
        public static final String CONTENUTO="contenuto";
        public static final String AUTORE="autore";
        public static final String CONTRIBUTORE="contributore";
        public static final String TITOLARE="titolare";
        public static final String RESPONSABILE="responsabile";
        public static final String EDITORE="editore";
        public static final String FINANZIAMENTO="finanziamento";
        public static final String SOGGETTO="soggetto";
        public static final String RISORSE="risorse";
        public static final String DATE_SCAVO="date_scavo";
        public static final String DATA_ARCHIVIAZIONE="data_archiviazione";
        public static final String VERSIONE="versione";
        public static final String LICENZA="licenza";
        public static final String TITOLARE_LICENZA="titolare_licenza";
        public static final String ACCESSO="accesso";
        public static final String PAROLE_CHIAVE="parole_chiave";


        //



        public static final ArrayList<Field> COLUMNS=new ArrayList<PostgisTable.Field>();
        public static final PostgisTable CENTROIDS=new PostgisTable("centroids_concessioni",
                COLUMNS, GeometryType.POINT);

        static {
            CENTROIDS.getFields().add(new Field(Defaults.INTERNAL_ID,FieldType.AUTOINCREMENT));
            CENTROIDS.getFields().add(new Field(PRODUCT_ID,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(NOME,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(ANNO,FieldType.INT));
            CENTROIDS.getFields().add(new Field(REGIONE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(GEOMETRY,FieldType.GEOMETRY));

            //EXtenions
            CENTROIDS.getFields().add(new Field(DESCRIZIONE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(CONTENUTO,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(AUTORE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(CONTRIBUTORE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(TITOLARE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(RESPONSABILE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(EDITORE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(FINANZIAMENTO,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(SOGGETTO,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(RISORSE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(DATE_SCAVO,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(DATA_ARCHIVIAZIONE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(VERSIONE,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(LICENZA,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(TITOLARE_LICENZA,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(ACCESSO,FieldType.TEXT));
            CENTROIDS.getFields().add(new Field(PAROLE_CHIAVE,FieldType.TEXT));

        }
    }

    public static class MOSI{

        public static final String CODE="code";
        public static final String GEOMETRY= Defaults.DEFAULT_GEOMETRY_COLUMN_NAME;


        public static final ArrayList<Field> COLUMNS=new ArrayList<PostgisTable.Field>();
        public static final PostgisTable CENTROID_MOSI=new PostgisTable("centroids_mosi",
                new ArrayList<Field>(), GeometryType.POINT);

        static {
            CENTROID_MOSI.getFields().add(new Field(Defaults.INTERNAL_ID,FieldType.AUTOINCREMENT));
            CENTROID_MOSI.getFields().add(new Field(GEOMETRY,FieldType.GEOMETRY));
            CENTROID_MOSI.getFields().add(new Field(CODE,FieldType.TEXT));
        }
    }

    public static class MOPR{
        public static final PostgisTable CENTROID_MOPR=new PostgisTable("centroids_mopr",
                new ArrayList<Field>(), GeometryType.POINT);
    }




    public static class INTERNAL{
        public static final String DB_NAME="gna_internal_db";

        public static final String RECORD="RECORD";
//		public static final String CONCESSIONE="CONCESSIONE";
//		public static final String
    }



}

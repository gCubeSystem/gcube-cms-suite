package org.gcube.application.cms.sdi.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;



/**
 * The Class ApplyRegex.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2023
 * 
 *         e.g. 
 *         { 
 *           "apply_regex": 
 *         		{ "type": "replaceAll",
 *         		  "regex": "(\\s)?\\([\\s\\S]*",
 *                "replacement": "" 
 *              }
 *         }
 */
@Data
@Slf4j
public class ApplyRegex {
	
	/**
	 * The Enum REGEX_TYPES.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Dec 21, 2023
	 */
	public static enum REGEX_TYPES {replaceAll, replaceFirst, find}
	String type;
	String regex;
	String replacement = "";

}

package org.gcube.application.cms.sdi.engine.bboxes;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.JSONPathWrapper;
import org.gcube.application.geoportal.common.model.document.filesets.sdi.GCubeSDILayer;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@ToString
@Slf4j
public abstract class BBOXEvaluator {

    @NonNull
    private String name;

    public abstract boolean isConfigured(Document profileConfiguration);
    public abstract GCubeSDILayer.BBOX evaluate(Document profileConfiguration, UseCaseDescriptor useCaseDescriptor, JSONPathWrapper documentNavigator);

}

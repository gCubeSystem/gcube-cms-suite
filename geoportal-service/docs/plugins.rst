.. _plugins:


.. _data_entry: https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app
.. _data_view: https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app

###############
Plugins
###############
**Plugins** implements the very logic of projects lifecycles in a modular way, allowing for composition and configuration of complex :doc:`ucd`.

.. note:: Each plugin exposes their descriptor through the service API in order to check its capabilities. Table below summarize that.

.. include:: _static/plugins_table.rst


******************
Lifecycle Managers
******************
These plugins enable the execution of workflows, triggering all other involved plugins. Check the section :doc:`lifecycle`.


*********************
Manifestation Plugins
*********************
These plugins are expected to enhance :ref:`filesets` with related :ref:`manifestations` for specific consumption.
Side effects vary depending on involved logic and engines.

==================
Default SDI Materializer
==================
This plugin creates a layer in the infrastructure's SDI based on registered FileSets in the Project.

..
	==================
	Image previewer
	==================
	default


*********************
Indexer Plugins
*********************
These plugins are expected to update :ref:`references` section of the project, registering it to catalogues / indexes.
Side effects vary depending on involved logic.


==================
Centroids Plugin
==================
This plugin creates a geometry (POINT) representing the centroid of the Project in a gCube SDI Layer of all elements of the collection.


*****
GUIs
*****
These plugins are designed in order to facilitate users to produce / consume projects.

==================
Data Entry & management
==================
Check source at `data_entry`_.

==================
Projects explorer
==================
Check source at `data_view`_.
.. _ucd:

##########################
Use Case Descriptors (UCD)
##########################

Use Case Descriptors (**UCD**) are documents used by the application in order to manage the entire lifecycle of registered Projects, including its consumption by users.

UCDs define :

* :ref: Schema : The structure of the related Projects' metadata documents (may depend by PHASE)
* :ref: Data Access Policies : Access rules (i.e. read/write all/own) on which base clients are allowed to operate on Projects
* :ref: Relationship Definitions : Relationships appliable to related Projects
* :ref: Handlers configuration : All expected configurations by handlers involved in the Projects lifecycle (both server-side plugins and clients / GUIs)


*********
Schema
*********

The schema defines the expected structure of the document section of a Project.
It is defined as a map of Fields objects like the following example ::

	"_schema":{
			"CD" : {"_min":1, "_label" : "IDENTIFICAZIONE","_children" :{
	      "TSK" : {"_min":1,"_type": "DIRECT-CL", "_values": ["MOSI"],"_label":"Tipo modulo"},
	      "CMD" : {"_min":1,"_label":"Codice modulo"},
	      "ESC" : {"_min":1,"_label":"Ente schedatore"},
	      "ECP" : {"_min":1,"_label":"Ente competente per tutela"},
	      "CBC" : {"_label":"Identificativo scheda bene culturale"},
	      "ACC" : {"_max":-1,"_label":"ALTRO CODICE", "_children": {
	        "ACCE" :  {"_label": "Ente/soggetto responsabile","_type": "DIRECT-CL", "_values": ["MATTM","Regione","Ente Territoriale","GNA"]},
	        "ACCC" :  {"_label": "Codice Identificativo","_type": "DIRECT-CL", "_values": ["MATTM","Regione","Ente Territoriale","GNA"]},
	        "ACCS" :  {"_label" : "Note"}}}}}
		}



********************
Data Access Policies
********************

This section of a UCD defines how users can access / write Projects based on their role in the current context.

These policies are defined as an array of conditions to apply to a certain set of roles.

.. note:: If "_roles" is empyt, the rule is considered the default policy.

Each policy can also define an **enforcer** policy to apply to each operation that the caller tries to perform.

In the following example ::

	"_dataAccessPolicies": [
	    {"_policy": {"_read": "none","_write": "none"}, "_roles": []
	    },
	    {"_policy": {"_read": "any", "_write": "none"},"_roles": ["Guest"],
	      "_enforcer": {
	        "_filter": "{\"_lifecycleInformation._phase\" :  {\"$eq\" :  \"Published\"}}"
	        }},

	    {"_policy": {"_read": "any","_write": "none"}, "_roles": ["Admin"]},
	    {"_policy": {"_read": "any","_write": "any"},"_roles": ["Data-Manager"]}
	  ]
 
* *Admin* can access any Project 
* *Data-Manager* can access and write any Project 
* *Guest* can access any project **with phase = Published** 
* The default policy is to deny access or writes


************************
Relationship Definitions
************************
Relationships are links between Projects. The UCD contains definitions of the links that can be applied to projects like in the following example::

	"_relationshipDefinitions" : [
		{"_id": "precedes", "_label" : "Precede","_reverseRelationId" : "follows"},
	    {"_id": "follows", "_label" : "Segue","_reverseRelationId" : "precedes"}
	  	]


.. note:: Relationships can be applied to Projects related to different UCDs.


***********************
Handlers Configurations
***********************

Hendlers configuration vary a lot in structure in order to allow for the most flwxibility and decoupling of components.
Each entry is expected to be defined as the following ::

	{
      "_id": ...,
      "_type": ...,
      "_configuration": {
        ..
      }
    }

In this way the declaration can be read from other components (e.g. lifecycle managers when looking for indexers) but the configuration is custom managed by the target handler itself.
See :ref:`plugins` for more informations.

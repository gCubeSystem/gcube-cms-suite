.. _suite:

.. _wiki: https://gcube.wiki.gcube-system.org/gcube/GeoPortal
.. _swagger: https://api.d4science.org
.. _gWiki: https://gcube.wiki.gcube-system.org/gcube/GeoPortal
.. _gCube: https://www.gcube-system.org

###############
gCube CMS Suite
###############
.. note:: gCube CMS Suite is a gCube Application. Check out more about `gCube`_.

**gCube CMS Suite** is a distributed full stack application for publication management of complex space-temporal documents called **Projects** in a gCube based infrastructure (see `wiki`_).

The **gCube CMS Suite** is a gCube Application designed to manage the publication lifecycle of complex space-temporal documents called Projects, supporting their materialisation and Indexing in different platforms (Databases, Catalogues, OGC Services) while maximising reusability.
It can manage the entire lifecycle of Projects, from their creation to access including :


The application’s main features are 
  * Documents Publication Management: 
      * CRUD (Create, Read, Update, Delete) operations, publication lifecycle, UCD - based validation;
      * Role-based access to resources and metadata sections;
      * Cooperative and extensible workflows;
  * Automatic Indexing & aggregation based on UCD annotations:
      * GIS indexes (both centroids & multi-polygon), with dynamic aggregation based on zoom level;
      * Text indexes;
  * Extensible Document model:
      * Generic meta-model;
      * Custom extensions of the model;
  * Extensible behaviour
      * Core engine based on a meta-model;
      * Publication management implemented by an extendable set of document Handlers;
      * Workflow configured in UCD by declaring LifeCycle Managers and involved Handlers.
      * Consumption of documents configured in UCD
      * Configurable exploitation of gCube facilities via dedicated Handlers (e.g. SDI Materialisation and Indexing)




.. figure:: _static/imgs/suite.png
   :alt: CMS Suite overall concept

   gCube CMS suite : GUIs interacts with the service which uses plugins to exploit gCube Applications


Project management is based on **Use Case Descriptor (UCD)** documents, which define for each collection:
  * Projects metadata schema
  * Role-based access to projects (and metadata sections) and operations
  * Configuration of involved Project handlers (Lifecycle Management, Event handlers, GUIs..)

Users can define their collection as a UCD in the VRE and then manage their Projects publication lifecycle both via gateway’s GUIs and by interacting with the service’s API (both JAVA and/or REST).
The service provides a set of Handlers which can be declared and configured in the UCD (defaults apply) in order to be invoked in the different phases of the publication lifecycle.

*********
The suite
*********

The suite is composed by the following main components :
 
  * **Geoportal Service** : a REST service designed to manage complex space-temporal Documents. It has been released as a component of the gCube [Assante M. et al. 2018] framework, and SmartGears powers it. It is based on Jersey and implements the core business logic. It offers CRUD interfaces over Projects and UCD while orchestrating the execution managed by Handlers plugins. The service provides a set of Handlers which can be declared and configured in the UCD (defaults apply) in order to be invoked in the different phases of the publication lifecycle.

  * **Plugins** : a number of pre defined Handlers that can be deployed along with the service in order to exploit them in Project lifecycles. Dynamic discovering and loading of extensions allows for 
    * Support of custom implementation of Handlers
    * Service capabilities defined at deployment time (allowing for different solutions for different situations).
    Currently availeble handlers are : 
      * 2 PHASE Lifecycle 
      * 3 PHASE Lifecycle
      * gCube SDI Indexer
      * gCube SDI Materializer

  * **GeoPortal Client** : a java client library developped on top of the gCube Featherweight stack, designed for dynamic model Serialisation / Deserialization of custom-provided Java model classes, allowing for easy integration of different systems.


Contents of this guide :
*************************

.. toctree::
   :maxdepth: 2

   architecture
   project
   lifecycle
   ucd
   plugins
   quickstart
   example
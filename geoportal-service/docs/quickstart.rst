.. _quickstart:


.. _swagger: ../api-docs/ui/index.html

##########
Quickstart
##########

.. seealso:: You can experiment with the application REST API at `swagger`_.



*********
Notebooks
*********
The following Jupyter notebooks can be used to interact with the :doc:`suite`.

.. note:: Jupyter lab can be easily used in d4science VREs. Check out your environment.

.. warning:: All notebooks use functions defined in **Commons** notebook. Be sure to include it in your notebook workspace.



Common guides
=============

- gCube CMS Suite : introductory notebook with the basic operations for project management (:doc:`_notebooks/gcube_cms.ipynb`)



..
	Use case specifics
	==================
	TODO

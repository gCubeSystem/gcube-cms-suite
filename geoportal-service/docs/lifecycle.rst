.. _lifecycle-management:

##############################
Project Management (Lifecycle)
##############################

.. note:: You can experiment with the service by following the :doc:`quickstart`.

From creation to its publication/deletion, documents pass through various ``PHASES`` of their life.
These ``PHASES`` define what operations (called ``STEPS``) can be performed with the **project**, being them automatic or human-driven (e.g. moderated review).
When invoked, operations may trigger collateral ``EVENTS`` (e.g. ``ON_EDIT``).
The result of all these operations *may* change the nature of the target **project**, which contains a specific report for this (see :ref:`lifecycle`)

.. note:: In brief : ``STEPS`` are operations that can be invoked by callers, ``EVENTS`` are collateral operations.

**Projects** lifecycle can vary depending on the configuration declared in its related :doc:`ucd`.
However they may change, each **project** lifecycle starts from the predefined ``PHASE = DRAFT``; what comes after that, depends on the configured lifecycle.

.. figure:: _static/imgs/2phase_lc.png
   :alt: PHASES and STEPS

   A 2-phase lifecycle.

In the figure above is represented a 2-PHASES lifecycle.
In green, STEPS and PHASES defined by the lifecycle itself.
In blue are re, the common operations Edit, Delete and the DRAFT PHASE.
In red the DELETE *PHASE* (which isn't material).

*****************
Common Lifecycles
*****************

The suite comes with a pre-built set of implemented lifecycles with their STEPS, EVENTS and PHASES.

====================
2 - Phases Lifecycle
====================
.. figure:: _static/imgs/2phase_lc.png
   :alt: 2 - PHASES Lifecycle

..
   TODO : steps phases, involved events

====================
3 - Phases Lifecycle
====================
.. figure:: _static/imgs/3phase_lc.png
   :alt: 3 - PHASES Lifecycle

..
   TODO : steps phases, involved events

*****************
Operation on documents
*****************
.. note:: You can experiment with the service API by following the :doc:`quickstart`.

.. warning:: Read and write permissions on *projects* are defined in :doc:`ucd`

While **projects** lifecycle can vary depending on the configuration declared in its related :doc:`ucd`, following operations are common and available for every **project**.

=================
Basic operations
=================

- Create New : The starting operation of every *project*. It expects a JSON metadata document, which will constitute the :ref:`document` section of the **project**.
- Edit : Allows for the replacement of :ref:`document`.
- Delete : Deletes the specified **project**
- List / get by Id : allows access to projects.

========
Querying
========

Querying allows for the filtered access of subsets of **projects**.
The following options are allowed in query operations

.. note:: Query engine is supported by MongoDB and exploits its query language. Check it out to learn more about it.

- filter : condition by witch to select returned projects
- ordering : field set and direction
- paging : limit and offset
- projection : projects fields selection to return in result

====================================
Registering / Unregistering FileSets
====================================

:ref:`filesets` are sets of files representing a single entity, which need to be handled together (e.g. a dataset and its index, a file and its crc).

Each :ref:`filesets` is (un)registered at a particular path of the :ref:`document` with requests with the following information :

.. warning:: Filesets fields need to be defined in :doc:`ucd` in order to be registered.

- field Name : **fileset** target field name
- parent path : the path of the target parent for the *fileset*
- field Definition path  : the path of the field definition inside schema
- streams : url or gCube storage Volatile IDs of payloads and related filenames

.. code:: json

    {
    "fieldDefinitionPath":"$.section._children[?(@.fileset)]",
    "parentPath":"$.section",
    "fieldName":"fileset",
    "streams":[{"url":"https://www.cnr.it/sites/all/themes/custom/cnr03_theme/img/cnrlogo.svg","filename":"myLogo.svg"}]
    }


.. note:: Note that paths can be explicit or express a query. Refer to jsonpath lib.

.. warning:: Paths are required to match a single field for fileset registration.

===============
Executing STEPS
===============

In order to expose a common API while serving varying / customizable possible scenarios, the service expose only a single method for STEP execution.

The method **perform STEP** allows for the execution of the configured STEPs by serving requests with the following information:

- STEP ID : must be supported by the configured lifecycle
- options [optional]: a JSON document with all expected parameters for the execution of the specified STEP

.. note:: Outcome of STEPS and operations is reported in :ref`lifecycle` section of the project.

Steps execution may trigger EVENTS and sometimes other automatic STEPS. The following represents how this behaviour is implemented.


.. figure:: _static/imgs/step_execution.png
   :alt: Locking and cascade execution of triggered operations


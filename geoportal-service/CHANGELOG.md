# Changelog for org.gcube.application.geoportal-service

## [v1.2.2] - 2024-10-16

- Included the health check at https://{geoporta_endpoint}/health via `smallrye-health` (that implements the https://microprofile.io/specifications/microprofile-health/) [#28301]

## [v1.2.1] - 2024-10-02

- Included the file size to reduce/optimize the time to upload files to the storage hub [#28150]

## [v1.2.0] - 2024-09-24

- Integrated the EventManager of the cms-plugin-framework [#26321]
- Added optional message when performing lifecycle step [#27192]
- Rewritten calledMethod to allow aggregation [#26707]

## [v1.1.1] - 2023-09-06

- Fixed data accounting issue [#25571]
- Moved to maven-parent 1.2.0 and using parent version range [#25572]

## [v1.1.0]

- Prepared the PATCH method [#24985]

## [v1.0.14]

- Just integrating the new facilities, see [#24791]
- Fixed deploying of multiple versions of the default-lc-managers [#24875]
- Bug fixing the 'deleteFileSet' function [#24902]

## [v1.0.13] - 2023-02-23
- Integrating new facilities and bug fixing released in the Plugins

## [v1.0.12] - 2023-01-10
- Refactored UserUtils into framework plugin

## [v1.0.11] - 2022-12-07
- Pom updates
- Introduced module default-lc-managers

## [v1.0.10] 2022-01-17
- Plugin Management
- Profiled Document : FileSet Registration 

## [v1.0.9] 2021-09-20
- Fixes [#23381](https://support.d4science.org/issues/23381)
- Fixes [#23485](https://support.d4science.org/issues/23485)



## [v1.0.8] 2021-09-20
- Logging
- Fixes [#22193](https://support.d4science.org/issues/22193)
- Fixes [#22280](https://support.d4science.org/issues/22280)
- Fixes [#20755](https://support.d4science.org/issues/20755)
- Profiled Documents


## [v1.0.6] 2021-09-20
- Refactored repositories
- Fixes #22193
- Fixes #22217

## [v1.0.5-SNAPSHOT] 2021-07-23
- Upgrade to gcube-smartgears-bom 2.1.0
- Fix register postgis table layer
- Added PostgisIndexRecordManager

## [v1.0.4] 2020-11-11
- Mongo integration with Concessione
- Project interface
- TempFile management
- WorkspaceContent and publication for Concessioni-over-mongo 

## [v1.0.3] 2020-11-11
- Fixed HTTP method

## [v1.0.2] 2020-11-11
- Delete method
- Excluded upper bound release gCube 5


## [v1.0.1] 2020-11-11
- Project interface

## [v1.0.0] 2020-11-11
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

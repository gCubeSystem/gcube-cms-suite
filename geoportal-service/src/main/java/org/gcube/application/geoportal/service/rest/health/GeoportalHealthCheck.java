package org.gcube.application.geoportal.service.rest.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class GeoportalHealthCheck.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 22, 2024
 */
@Readiness
@Liveness
@Slf4j
public class GeoportalHealthCheck implements HealthCheck {

	private static final String SERVICE_NAME = "geooportal-service";

	/**
	 * Call.
	 *
	 * @return the health check response
	 */
	@Override
	public HealthCheckResponse call() {
		log.info(GeoportalHealthCheck.class.getSimpleName() + " call");
		HealthCheckResponse response = HealthCheckResponse.named(SERVICE_NAME).state(true).build();
		return response;
	}

}

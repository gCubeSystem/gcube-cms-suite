package org.gcube.application.geoportal.service.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.bson.Document;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.cms.implementations.utils.UserUtils;
import org.gcube.application.cms.plugins.events.EventManager;
import org.gcube.application.cms.plugins.events.EventManager.Event;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.cms.plugins.events.ItemObserved.OPTIONAL_FIELD;
import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.document.accounting.AccountingInfo;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation.Status;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.PerformStepRequest;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.rest.RegisterFileSetRequest;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.service.accounting.CalledMethodHandler;
import org.gcube.application.geoportal.service.engine.mongo.ProfiledMongoManager;
import org.gcube.application.geoportal.service.engine.providers.ConfigurationCache;
import org.gcube.application.geoportal.service.engine.providers.ProjectAccessImpl;
import org.gcube.application.geoportal.service.http.PATCH;
import org.gcube.application.geoportal.service.util.AuthorizedThread;

import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class ProfiledDocuments.
 * 
 * @author created by Fabio Sinibaldi
 * @author new manager/developer and mantainer - Francesco Mangiacrapa at
 *         ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 18, 2023
 */
@Path(InterfaceConstants.Methods.PROJECTS + "/{" + InterfaceConstants.Parameters.UCID + "}")
@Slf4j
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
		@RequestHeader(name = "Content-Type", description = "application/json") })
public class ProfiledDocuments {

	public static final String HIDDEN_PATH = "hidden";
	private ProfiledMongoManager manager;
	private EventManager eventManager = EventManager.getInstance(); // as singleton

	/**
	 * Instantiates a new profiled documents.
	 *
	 * @param profileID the profile ID
	 * @throws ConfigurationException the configuration exception
	 */
	public ProfiledDocuments(@PathParam(InterfaceConstants.Parameters.UCID) String profileID)
			throws ConfigurationException {
		log.info("Accessing profile " + profileID);
		manager = new GuardedMethod<ProfiledMongoManager>() {
			@Override
			protected ProfiledMongoManager run() throws Exception {
				return new ProfiledMongoManager(profileID);
			}
		}.execute().getResult();
	}

	/**
	 * Gets the configuration.
	 *
	 * @param profileID the profile ID
	 * @return the configuration
	 */
	@GET
	@Path(InterfaceConstants.Methods.CONFIGURATION_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Configuration getConfiguration(@PathParam(InterfaceConstants.Parameters.UCID) String profileID) {

		String path = CalledMethodHandler.buildCalledResource(HttpMethod.GET,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + profileID);
		CalledMethodHandler.setCalledMethod(path + "/" + InterfaceConstants.Methods.CONFIGURATION_PATH);

		return new GuardedMethod<Configuration>() {

			@Override
			protected Configuration run() throws Exception, WebApplicationException {
				return ImplementationProvider.get().getProvidedObjectByClass(ConfigurationCache.ConfigurationMap.class)
						.get(profileID);
			}
		}.execute().getResult();
	}

	/**
	 * Creates the new.
	 *
	 * @param d the d
	 * @return the project
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Project createNew(Document d) {

		String path = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(path + "/createNew");

		Project theNewProject = new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Creating new Project ({})", manager.getUseCaseDescriptor().getId());
				Project toReturn = manager.registerNew(d);
				log.info("Created new Project ({}, ID {})", manager.getUseCaseDescriptor().getId(), toReturn.getId());
				return toReturn;
			}
		}.execute().getResult();

		Status status = theNewProject.getLifecycleInformation().getLastOperationStatus();
		// If latest operation status is not ERROR, notify PROJECT_CREATED event
		if (status != null && !status.equals(LifecycleInformation.Status.ERROR)) {
			// notifying the Event.PROJECT_CREATED;
			AuthorizedThread autThr = null;
			try {
				autThr = new AuthorizedThread(UserUtils.getCurrent()) {
					public void run() {
						ItemObserved<Project> item = new ItemObserved<Project>();
						EventManager.Event event = Event.PROJECT_CREATED;
						AccountingInfo user = UserUtils.getCurrent().asInfo();
						item.setUserCaller(user.getUser());
						item.setContext(user.getContext());
						item.setOptional(null);
						item.setEvent(event);
						item.setProject(theNewProject);
						item.setUseCaseDescriptor(manager.getUseCaseDescriptor());
						log.info("By notifying event ({}, on Project ID {}) by thread id {}", event, item.getProjectId(),
								this.getId());
						eventManager.notify(event, item);
					}
				};

				autThr.start();
			} catch (Exception e) {
				log.warn("Error occurred when notifying event " + Event.PROJECT_CREATED, e);
			} finally {
				if (autThr != null)
					autThr.resetContext();
			}
		}

		return theNewProject;
	}

	/**
	 * Update.
	 *
	 * @param documentId the document id
	 * @param d          the d
	 * @return the project
	 */
	@PUT
	@Path("{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Project update(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String documentId, Document d) {

		String path = CalledMethodHandler.buildCalledResource(HttpMethod.PUT,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(path + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}");

		Project theUpdatedProject = new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Updating Project ({}, ID {})", manager.getUseCaseDescriptor().getId(), documentId);
				return manager.update(documentId, d);
			}
		}.execute().getResult();

		Status status = theUpdatedProject.getLifecycleInformation().getLastOperationStatus();
		// If latest operation status is not ERROR, notify PROJECT_UPDATED event
		if (status != null && !status.equals(LifecycleInformation.Status.ERROR)) {

			AuthorizedThread autThr = null;
			try {
				autThr = new AuthorizedThread(UserUtils.getCurrent()) {
					public void run() {
						ItemObserved<Project> item = new ItemObserved<Project>();
						EventManager.Event event = Event.PROJECT_UPDATED;
						AccountingInfo user = UserUtils.getCurrent().asInfo();
						item.setUserCaller(user.getUser());
						item.setContext(user.getContext());
						item.setOptional(null);
						item.setEvent(event);
						item.setProject(theUpdatedProject);
						item.setUseCaseDescriptor(manager.getUseCaseDescriptor());
						log.info("By notifying event ({}, on Project ID {}) by thread id {}", event, item.getProjectId(),
								this.getId());
						eventManager.notify(event, item);
					}
				};

				autThr.start();
			} catch (Exception e) {
				log.warn("Error occurred when notifying event " + Event.PROJECT_UPDATED, e);
			} finally {
				if (autThr != null)
					autThr.resetContext();
			}
		}

		return theUpdatedProject;
	}

	/**
	 * Patch.
	 *
	 * @param documentId the document id
	 * @param d          the d
	 * @return the project
	 */
	@PATCH
	@Path("{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Project patch(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String documentId, Document d) {

		String path = CalledMethodHandler.buildCalledResource("PATCH",
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(path + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Patching Project ({}, ID {})", manager.getUseCaseDescriptor().getId(), documentId);
				// return manager.patch(documentId,d);
				throw new WebApplicationException("This method has not yet been implemented!");
			}
		}.execute().getResult();
	}

	/**
	 * Delete.
	 *
	 * @param id    the id
	 * @param force the force
	 * @return the boolean
	 * 
	 *         Added by Francesco M.
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Boolean delete(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.FORCE) Boolean force) {
		return delete(id, force, false);
	}

	/**
	 * Delete.
	 *
	 * @param id    the id
	 * @param force the force
	 * @return the boolean
	 * 
	 * @Ignore means excluded by API doc Updated by Francesco M.
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Parameters.PROJECT_ID + "}" + "/" + HIDDEN_PATH)
	@Ignore
	public Boolean delete(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.FORCE) Boolean force,
			@QueryParam(InterfaceConstants.Parameters.IGNORE_ERRORS) Boolean ignoreErrors) {

		String path = CalledMethodHandler.buildCalledResource(HttpMethod.DELETE,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(path + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}");

		Boolean deleted = new GuardedMethod<Boolean>() {
			@Override
			protected Boolean run() throws Exception, WebApplicationException {
				log.info("Deleting Project ({}, ID {}). Force is {},  Ignore_Errors is {}",
						manager.getUseCaseDescriptor().getId(), id, force, ignoreErrors);
				manager.delete(id, force, ignoreErrors);
				return true;
			}
		}.execute().getResult();

		// If deleted, notify PROJECT_DELETED event
		if (deleted) {
			AuthorizedThread autThr = null;
			try {
				autThr = new AuthorizedThread(UserUtils.getCurrent()) {
					public void run() {
						ItemObserved<Project> item = new ItemObserved<Project>();
						EventManager.Event event = Event.PROJECT_DELETED;
						AccountingInfo user = UserUtils.getCurrent().asInfo();
						item.setUserCaller(user.getUser());
						item.setContext(user.getContext());
						item.setEvent(event);
						item.setUseCaseDescriptor(manager.getUseCaseDescriptor());
						// Referencing delete project
						Project deletedProject = new Project();
						deletedProject.setId(id);
						deletedProject.setProfileID(manager.getUseCaseDescriptor().getId());
						item.setProject(deletedProject);
						log.info("By notifying event ({}, on Project ID {}) by thread id {}", event, item.getProjectId(),
								this.getId());
						eventManager.notify(event, item);
					}
				};

				autThr.start();
			} catch (Exception e) {
				log.warn("Error occurred when notifying event " + Event.PROJECT_DELETED, e);
			} finally {
				if (autThr != null)
					autThr.resetContext();
			}
		}

		return deleted;
	}

	/**
	 * Register file set.
	 *
	 * @param id      the id
	 * @param request the request
	 * @return the project
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.REGISTER_FILES_PATH + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Project registerFileSet(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			RegisterFileSetRequest request) {

		String path = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(path + "/" + InterfaceConstants.Methods.REGISTER_FILES_PATH + "/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("UCD {} : Project {} Registering Fileset. Request is {}",
						manager.getUseCaseDescriptor().getId(), id, request);
				request.validate();
				return manager.registerFileSet(id, request);
			}
		}.execute().getResult();
	}

	/**
	 * Delete file set. the Authorization must be a VRE token
	 * 
	 * @param id    the id
	 * @param force the force
	 * @param path  the path must be passed as text in the body
	 * @return the project
	 * 
	 *         Added by Francesco M
	 */
	@RequestHeaders({
			@RequestHeader(name = "Authorization", description = "VRE Bearer token, see https://dev.d4science.org/how-to-access-resources"),
			@RequestHeader(name = "Content-Type", description = "application/json") })
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.DELETE_FILES_PATH + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Project deleteFileSet(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.FORCE) Boolean force, String path) {
		return deleteFileSet(id, force, false, path);
	}

	/**
	 * Delete file set. the Authorization must be a VRE token
	 * 
	 * @param id    the id
	 * @param force the force
	 * @param path  the path must be passed as text in the body
	 * @return the project
	 * 
	 * @Ignore means that is excluded by API doc Updated by Francesco M.
	 */
	@RequestHeaders({
			@RequestHeader(name = "Authorization", description = "VRE Bearer token, see https://dev.d4science.org/how-to-access-resources"),
			@RequestHeader(name = "Content-Type", description = "application/json") })
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.DELETE_FILES_PATH + "/" + HIDDEN_PATH + "/{"
			+ InterfaceConstants.Parameters.PROJECT_ID + "}")
	@Ignore
	public Project deleteFileSet(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.FORCE) Boolean force,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.IGNORE_ERRORS) Boolean ignore_errors,
			String path) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.DELETE_FILES_PATH
				+ "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Deleting FileSet of Project ({}, ID {}) at path {}. Force is {}. Ignore_errors is {}",
						manager.getUseCaseDescriptor().getId(), id, path, force, ignore_errors);
				return manager.deleteFileSet(id, path, force, ignore_errors);
			}
		}.execute().getResult();
	}

	/**
	 * Perform step.
	 *
	 * @param id      the id
	 * @param request the request
	 * @return the project
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.STEP + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Project performStep(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			PerformStepRequest performStepRequest) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.STEP + "/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}");

		Project theProject = new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Executing step {} on Project ({},ID,{}) with options {}", performStepRequest.getStepID(),
						manager.getUseCaseDescriptor().getId(), id, performStepRequest.getOptions());
				return manager.performStep(id, performStepRequest.getStepID(), performStepRequest.getOptions());
			}
		}.execute().getResult();

		Status status = theProject.getLifecycleInformation().getLastOperationStatus();
		// If latest operation status is not ERROR, notify LIFECYCLE_STEP_PERFORMED
		// event
		if (status != null && !status.equals(LifecycleInformation.Status.ERROR)) {

			AuthorizedThread autThr = null;
			try {
				autThr = new AuthorizedThread(UserUtils.getCurrent()) {
					public void run() {
						ItemObserved<Project> item = new ItemObserved<Project>();
						EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
						AccountingInfo user = UserUtils.getCurrent().asInfo();
						item.setUserCaller(user.getUser());
						item.setContext(user.getContext());
						item.setEvent(event);
						item.setOptional(OPTIONAL_FIELD.message, performStepRequest.getMessage());
						item.setUseCaseDescriptor(manager.getUseCaseDescriptor());
						item.setProject(theProject);
						log.info("By notifying event ({}, on Project ID {}) by thread id {}", event, item.getProjectId(),
								this.getId());
						eventManager.notify(event, item);
					}
				};

				autThr.start();
			} catch (Exception e) {
				log.warn("Error occurred when notifying event " + Event.LIFECYCLE_STEP_PERFORMED, e);
			} finally {
				if (autThr != null)
					autThr.resetContext();
			}
		}

		return theProject;
	}

	/**
	 * Force unlock.
	 *
	 * @param id the id
	 * @return the project
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.FORCE_UNLOCK + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Project forceUnlock(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.PUT,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.FORCE_UNLOCK + "/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {

			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.warn("UCD {}, forcing unlock for Project ID {}", manager.getUseCaseDescriptor().getId(), id);
				return manager.forceUnlock(id);
			}
		}.execute().getResult();
	}

	/**
	 * Sets the access policy.
	 *
	 * @param id    the id
	 * @param toSet the to set
	 * @return the project
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.SET_PROJECT_ACCESS_POLICY + "/{" + InterfaceConstants.Parameters.PROJECT_ID
			+ "}")
	public Project setAccessPolicy(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id, Access toSet) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.PUT,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler
				.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.SET_PROJECT_ACCESS_POLICY + "/{"
						+ InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {

			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.warn("UCD {}, setting Policy {} Project ID {}", manager.getUseCaseDescriptor().getId(), toSet, id);
				return manager.setAccessPolicy(id, toSet);
			}
		}.execute().getResult();
	}

	// ********************************** READ

	/**
	 * List.
	 *
	 * @return the iterable
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Iterable<?> list() {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.GET,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource);

		return new GuardedMethod<Iterable<?>>() {
			protected Iterable<?> run() throws Exception, WebApplicationException {
				return manager.query(new QueryRequest());
			};
		}.execute().getResult();
	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	// BY ID
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Parameters.PROJECT_ID + "}")
	public Project getById(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.GET,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/{" + InterfaceConstants.Parameters.PROJECT_ID + "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				return manager.getByID(id);
			}
		}.execute().getResult();
	}

	/**
	 * Search.
	 *
	 * @param filter the filter
	 * @return the string
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.SEARCH_PATH)
	public String search(String filter) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.SEARCH_PATH);

		return new GuardedMethod<String>() {
			@Override
			protected String run() throws Exception, WebApplicationException {
				QueryRequest req = new QueryRequest();
				req.setFilter(Document.parse(filter));
				return Serialization.write(manager.query(req));
			}
		}.execute().getResult();
	}

	/**
	 * Query.
	 *
	 * @param queryString the query string
	 * @return the iterable
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + InterfaceConstants.Methods.QUERY_PATH)
	public Iterable<?> query(String queryString) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.POST,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/" + InterfaceConstants.Methods.QUERY_PATH);

		return new GuardedMethod<Iterable<?>>() {
			@Override
			protected Iterable<?> run() throws Exception, WebApplicationException {
				return manager.query(Serialization.parseQuery(queryString));
			}
		}.execute().getResult();
	}

	// Relationships

	/**
	 * Gets the relationship chain.
	 *
	 * @param id             the id
	 * @param relationshipId the relationship id
	 * @param deep           the deep
	 * @return the relationship chain
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{" + InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{"
			+ InterfaceConstants.Parameters.RELATIONSHIP_ID + "}")
	public String getRelationshipChain(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@PathParam(InterfaceConstants.Parameters.RELATIONSHIP_ID) String relationshipId,
			@DefaultValue("false") @QueryParam(InterfaceConstants.Parameters.DEEP) Boolean deep) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.GET,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{" + InterfaceConstants.Parameters.RELATIONSHIP_ID
				+ "}");

		return new GuardedMethod<String>() {
			@Override
			protected String run() throws Exception, WebApplicationException {
				return Serialization.write(ProjectAccessImpl
						.getRelationshipChain(manager.getUseCaseDescriptor().getId(), id, relationshipId, deep));
			}
		}.execute().getResult();
	}

	/**
	 * Sets the relation.
	 *
	 * @param id             the id
	 * @param relationshipId the relationship id
	 * @param targetId       the target id
	 * @param targetUCD      the target UCD
	 * @return the project
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{" + InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{"
			+ InterfaceConstants.Parameters.RELATIONSHIP_ID + "}")
	public Project setRelation(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@PathParam(InterfaceConstants.Parameters.RELATIONSHIP_ID) String relationshipId,
			@QueryParam(InterfaceConstants.Parameters.TARGET_ID) String targetId,
			@QueryParam(InterfaceConstants.Parameters.TARGET_UCD) String targetUCD) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.PUT,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{" + InterfaceConstants.Parameters.RELATIONSHIP_ID
				+ "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Set relation from Project ({} : {}) [{}]->  ({} : {})",
						manager.getUseCaseDescriptor().getId(), id, relationshipId, targetUCD, targetId);
				String toUseTargetUCD = targetUCD;
				if (toUseTargetUCD == null || toUseTargetUCD.isEmpty()) {
					log.debug("Target UCD is null, forcing same UCD () as source ",
							manager.getUseCaseDescriptor().getId());
					toUseTargetUCD = manager.getUseCaseDescriptor().getId();
				}
				return manager.setRelation(id, relationshipId, toUseTargetUCD, targetId);
			}
		}.execute().getResult();
	}

	/**
	 * Delete relation.
	 *
	 * @param id             the id
	 * @param relationshipId the relationship id
	 * @param targetId       the target id
	 * @param targetUCD      the target UCD
	 * @return the project
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{" + InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{"
			+ InterfaceConstants.Parameters.RELATIONSHIP_ID + "}")
	public Project deleteRelation(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			@PathParam(InterfaceConstants.Parameters.RELATIONSHIP_ID) String relationshipId,
			@QueryParam(InterfaceConstants.Parameters.TARGET_ID) String targetId,
			@QueryParam(InterfaceConstants.Parameters.TARGET_UCD) String targetUCD) {

		String pathCalledResource = CalledMethodHandler.buildCalledResource(HttpMethod.DELETE,
				"/" + InterfaceConstants.Methods.PROJECTS + "/" + manager.getUseCaseDescriptor().getId());
		CalledMethodHandler.setCalledMethod(pathCalledResource + "/{" + InterfaceConstants.Methods.RELATIONSHIP + "}/{"
				+ InterfaceConstants.Parameters.PROJECT_ID + "}" + "/{" + InterfaceConstants.Parameters.RELATIONSHIP_ID
				+ "}");

		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				log.info("Deleting relation from Project ({} : {}) [{}]->  ({} : {})",
						manager.getUseCaseDescriptor().getId(), id, relationshipId, targetUCD, targetId);
				return manager.deleteRelation(id, relationshipId, targetUCD, targetId);
			}
		}.execute().getResult();
	}

}
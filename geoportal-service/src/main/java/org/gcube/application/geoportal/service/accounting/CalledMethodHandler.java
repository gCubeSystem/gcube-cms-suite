package org.gcube.application.geoportal.service.accounting;

import org.gcube.common.authorization.library.provider.CalledMethodProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalledMethodHandler {

	private final static Logger logger = LoggerFactory.getLogger(CalledMethodHandler.class);

	public static void setCalledMethod(String method) {
		logger.debug("setCalledMethod as {}", method);
		CalledMethodProvider.instance.set(method);
	}
	
	public static String buildCalledResource(String httpMethod, String path) {
		if(!path.startsWith("/"))
			path = "/"+path;
		
		return String.format("%s %s", httpMethod.toString(), path);
	}

}
package org.gcube.application.geoportal.service.rest.health;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;
import org.gcube.application.cms.implementations.ISInterface;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.DatabaseConnection;
import org.gcube.application.geoportal.service.ServiceConstants;
import org.gcube.common.scope.api.ScopeProvider;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class DatabaseHealthCheck.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 23, 2024
 */
@Readiness
@Liveness
@Slf4j
public class DatabaseHealthCheck implements HealthCheck {

	private String context;
	public static final String SERVICE_NAME = "database";

	private static final int CONNECTION_TIMEOUT = 30;

	/**
	 * Call.
	 *
	 * @return the health check response
	 */
	@Override
	public HealthCheckResponse call() {
		return checkDatabase(context);
	}

	/**
	 * Instantiates a new database health check.
	 *
	 * @param context the context
	 */
	public DatabaseHealthCheck(String context) {
		this.context = context;
	}

	/**
	 * Check database.
	 *
	 * @param context the context
	 * @return the health check response
	 */
	private HealthCheckResponse checkDatabase(String context) {
		log.debug("checkMongo in the context: {}", context);
		HealthCheckResponseBuilder buildHCRBuilder = HealthCheckResponse.named(SERVICE_NAME);

		ScopeProvider.instance.set(context);

		try {

			DatabaseConnection databaseConnection = null;
			ISInterface isInterface = ImplementationProvider.get().getProvidedObjectByClass(ISInterface.class);

			try {
				if (isInterface == null)
					throw new Exception(ISInterface.class.getSimpleName() + " configuration is null for "
							+ DatabaseConnection.class.getSimpleName());

				databaseConnection = isInterface.queryForDatabase(ServiceConstants.SE_GNA_DB_CATEGORY,
						ServiceConstants.SE_GNA_DB_PLATFORM, ServiceConstants.SE_GNA_DB_FLAGNAME,
						ServiceConstants.SE_GNA_DB_FLAGVALUE);

				if (databaseConnection == null)
					throw new Exception(DatabaseConnection.class.getSimpleName() + " configuration is null");
			} catch (Exception e) {
				log.error("Error on checking DB configuration: ", e);
				buildHCRBuilder.state(false);
				return buildHCRBuilder.build();
			}

			boolean connectionStatus = checkDatabaseConnection(databaseConnection);
			buildHCRBuilder = appendDBInfo(buildHCRBuilder, databaseConnection);
			buildHCRBuilder.state(connectionStatus);
			log.info("checkDatabase is OK in the context: {}. State is {}", context, connectionStatus);
			return buildHCRBuilder.build();

		} catch (Exception e) {
			log.error("Error on checkDatabase: ", e);
			log.warn("checkDatabase is KO in the context: {}", context);
			buildHCRBuilder.state(false);
			return buildHCRBuilder.build();
		} finally {
			ScopeProvider.instance.reset();
		}
	}

	/**
	 * Append DB info.
	 *
	 * @param buildHCRBuilder the build HCR builder
	 * @param connection      the connection
	 * @return the health check response builder
	 */
	private HealthCheckResponseBuilder appendDBInfo(HealthCheckResponseBuilder buildHCRBuilder,
			DatabaseConnection connection) {
		buildHCRBuilder.withData("host", connection.getUrl() + "");

		// anonymize the DB username
		String userNotClear = "***";
		if (connection.getUser() != null && connection.getUser().length() > 3) {
			userNotClear = connection.getUser().substring(0, 3) + userNotClear;
		}
		buildHCRBuilder.withData("user ", userNotClear);
		buildHCRBuilder.withData("pwd ", "****");
		return buildHCRBuilder;
	}

	/**
	 * Check database connection.
	 *
	 * @param connectionParameters the connection parameters
	 * @return true, if successful
	 */
	private boolean checkDatabaseConnection(DatabaseConnection connectionParameters) {

		try {

			if (connectionParameters == null)
				throw new ConfigurationException("connectionParameters is null");

			// Getting connection
			Connection connection = DriverManager.getConnection(connectionParameters.getUrl(),
					connectionParameters.getUser(), connectionParameters.getPwd());
			// Check if the connection is valid (timeout 30 seconds)
			if (connection != null && connection.isValid(CONNECTION_TIMEOUT)) {
				log.debug("Connection to DB " + connectionParameters.getUrl() + " is OK!");
				return true;
			} else {
				log.debug("Connection to DB " + connectionParameters.getUrl() + " is KO!");
				return false;
			}
		} catch (SQLException e) {
			log.warn("Error on connecting to DB: " + connectionParameters, e);
			return false;
		} catch (ConfigurationException e1) {
			log.warn("Error on reading connection configuration: " + connectionParameters, e1);
			return false;
		}
	}

}

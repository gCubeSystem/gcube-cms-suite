package org.gcube.application.geoportal.service;

public class ServiceConstants {

	// SE DB flagName
	public static final String SE_GNA_DB_FLAGNAME = "GNA_DB";
	// SE DB flagValue
	public static final String SE_GNA_DB_FLAGVALUE = "Concessioni";
	// SE DB category
	public static final String SE_GNA_DB_CATEGORY = "Database";
	// SE DB platform
	public static final String SE_GNA_DB_PLATFORM = "postgis";

	public static final String MONGO_SE_PLATFORM = "mongodb";
	public static final String MONGO_SE_GNA_FLAG = "internal-db";

}

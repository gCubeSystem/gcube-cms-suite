package org.gcube.application.geoportal.service.engine.providers;

import org.gcube.application.cms.caches.Engine;
import org.gcube.application.cms.implementations.ProjectAccess;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public class ProjectAccessProvider implements Engine<ProjectAccess> {


    @Override
    public void init() {

    }

    @Override
    public void shutdown() {

    }

    @Override
    public ProjectAccess getObject() throws ConfigurationException {
        return new ProjectAccessImpl();
    }


}

package org.gcube.application.geoportal.service.engine.mongo;

import org.gcube.application.cms.implementations.faults.RegistrationException;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

public interface UCDManagerI {

    public Iterable<UseCaseDescriptor> query(QueryRequest request) throws ConfigurationException;

    public void deleteById(String id,boolean force) throws RegistrationException, ConfigurationException;

   public UseCaseDescriptor put(UseCaseDescriptor descriptor) throws RegistrationException, ConfigurationException;

    public UseCaseDescriptor getById(String id) throws ConfigurationException, RegistrationException;
}

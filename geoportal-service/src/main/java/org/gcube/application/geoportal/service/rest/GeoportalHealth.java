package org.gcube.application.geoportal.service.rest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponse.State;
import org.gcube.application.geoportal.service.rest.health.DatabaseHealthCheck;
import org.gcube.application.geoportal.service.rest.health.GeoportalHealthCheck;
import org.gcube.application.geoportal.service.rest.health.HealthCheckResponseSerializer;
import org.gcube.application.geoportal.service.rest.health.MongoHealthCheck;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class GeoportalHealth.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 22, 2024
 */
@Path("/health")
@Slf4j
public class GeoportalHealth {

	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * Instantiates a new geoportal health.
	 */
	public GeoportalHealth() {
		SimpleModule module = new SimpleModule();
		module.addSerializer(HealthCheckResponse.class, new HealthCheckResponseSerializer());
		mapper.registerModule(module);
	}

	/**
	 * Service check.
	 *
	 * @return the response compliant to `microprofile-health` specification. 200 if
	 *         is OK. Otherwise it fails.
	 * @throws JsonProcessingException the json processing exception
	 */
	@GET
	@Path("")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response serviceCheck() throws JsonProcessingException {
		log.debug("serviceCheck called");
		HealthCheckResponse response = new GeoportalHealthCheck().call();
		String json = healthCheckSerializer(response);
		log.info("serviceCheck response is {}", json);
		return Response.ok().entity(json).build();
	}

	/**
	 * Mongo check.
	 *
	 * @param context             the gcube context
	 * @param include_collections if the check has to include the mongo collections
	 *                            in the response
	 * @return the response compliant to `microprofile-health` specification
	 * @throws JsonProcessingException the json processing exception
	 */
	@GET
	@Path("/mongo")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response mongoCheck(@QueryParam("context") String context,
			@DefaultValue("false") @QueryParam("include_collections") Boolean includeCollections)
			throws JsonProcessingException {
		log.debug("mongoCheck called in the context {}, includeCollections {}", context, includeCollections);
		if (context == null) {
			HealthCheckResponse response = HealthCheckResponse.named(MongoHealthCheck.SERVICE_NAME)
					.withData("context", "is required parameter (e.g. context=/gcube/devsec/devVRE)").down().build();
			String json = healthCheckSerializer(response);
			log.info("mongoCheck error response is {}", json);
			// Bad request
			return Response.status(400).entity(json).build();
		}

		HealthCheckResponse response = new MongoHealthCheck(context, includeCollections).call();
		ResponseBuilder responseBuilder = Response.ok();
		if (response.getState().equals(State.DOWN)) {
			responseBuilder = responseBuilder.status(503);
		}
		String json = healthCheckSerializer(response);
		log.info("mongoCheck response is {}", json);
		return responseBuilder.entity(json).build();
	}

	/**
	 * Database check.
	 *
	 * @param context the gcube context
	 * @return the response compliant to `microprofile-health` specification
	 * @throws JsonProcessingException the json processing exception
	 */
	@GET
	@Path("/database")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response databaseCheck(@QueryParam("context") String context) throws JsonProcessingException {
		log.debug("databaseCheck called in the context {}", context);
		if (context == null) {
			HealthCheckResponse response = HealthCheckResponse.named(DatabaseHealthCheck.SERVICE_NAME)
					.withData("context", "is required parameter (e.g. context=/gcube/devsec/devVRE)").down().build();
			String json = healthCheckSerializer(response);
			log.info("databaseCheck error response is {}", json);
			// Bad request
			return Response.status(400).entity(json).build();
		}

		HealthCheckResponse response = new DatabaseHealthCheck(context).call();
		ResponseBuilder responseBuilder = Response.ok();
		if (response.getState().equals(State.DOWN)) {
			responseBuilder = responseBuilder.status(503);
		}
		String json = healthCheckSerializer(response);
		log.info("databaseCheck response is {}", json);
		return responseBuilder.entity(json).build();
	}

	/**
	 * Health check serializer.
	 *
	 * @param response the response
	 * @return the string
	 * @throws JsonProcessingException the json processing exception
	 */
	private String healthCheckSerializer(HealthCheckResponse response) throws JsonProcessingException {
		// Serializes HealthCheckResponse in JSON with custom
		// HealthCheckResponseSerializer
		return mapper.writeValueAsString(response);
	}

}

package org.gcube.application.geoportal.service.engine.providers.ucd;

import java.util.HashMap;

import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProfileMap extends HashMap<String, UseCaseDescriptor> {
}

package org.gcube.application.geoportal.service.engine.providers;

import org.gcube.application.cms.caches.AbstractScopedMap;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.utils.StorageUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StorageClientProvider extends AbstractScopedMap<StorageUtils> {

	
	
	public StorageClientProvider() {
		super("Storage client cache");
//		setTTL(Duration.of(10, ChronoUnit.MINUTES));
	}
	
	@Override
	protected StorageUtils retrieveObject(String Context) throws ConfigurationException {
		try{
			return new StorageUtils();
		}catch(Throwable t){
			throw new ConfigurationException("unable to get Storage",t);
		}
//		return new StorageClient(InterfaceConstants.SERVICE_CLASS, InterfaceConstants.SERVICE_NAME, ContextUtils.getCurrentCaller(), AccessType.SHARED, MemoryType.VOLATILE).getClient();
	}
	
	@Override
	protected void dispose(StorageUtils toDispose) {
		try {
			toDispose.forceClose();
		}catch (NullPointerException e) {
			// expected if closed without uploading
		}catch(Throwable t) {
			log.warn(" unable to dispose "+toDispose,t); 
		}
	}

	@Override
	public void init() {
		
	}
	
}

package org.gcube.application.geoportal.service.rest.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.geoportal.common.model.configuration.MongoConnection;
import org.gcube.application.geoportal.service.model.internal.db.Mongo;
import org.gcube.common.scope.api.ScopeProvider;

import com.mongodb.client.MongoIterable;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class MongoHealthCheck.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 22, 2024
 */
@Readiness
@Liveness
@Slf4j
public class MongoHealthCheck implements HealthCheck {

	private String context;
	private Boolean includeCollections = false;
	public static final String SERVICE_NAME = "mongo";

	/**
	 * Call.
	 *
	 * @return the health check response
	 */
	@Override
	public HealthCheckResponse call() {
		return checkMongo(context);
	}

	/**
	 * Instantiates a new mongo health check.
	 *
	 * @param context the context
	 * @param includeCollections the include collections
	 */
	public MongoHealthCheck(String context, Boolean includeCollections) {
		this.context = context;
		this.includeCollections = includeCollections;
	}

	/**
	 * Check mongo.
	 *
	 * @param context the context
	 * @return the health check response
	 */
	private HealthCheckResponse checkMongo(String context) {
		log.debug("checkMongo in the context: {}", context);
		HealthCheckResponseBuilder buildHCRBuilder = HealthCheckResponse.named(SERVICE_NAME);
		Mongo mongo = null;
		try {
			ScopeProvider.instance.set(context);
			mongo = ImplementationProvider.get().getProvidedObjectByClass(Mongo.class);
			buildHCRBuilder = appendMongoInfo(buildHCRBuilder, mongo.getConnection());
			buildHCRBuilder.state(true);
			if (includeCollections!=null && includeCollections) {
				MongoIterable<String> collections = mongo.getTheClient()
						.getDatabase(mongo.getConnection().getDatabase()).listCollectionNames();
				log.info("listCollectionNames is null: {}", collections == null);
				int i = 1;
				for (String coll : collections) {
					log.debug("adding collection: {}", coll);
					buildHCRBuilder.withData("collection_" + i, coll);
					i++;
				}
			}
			log.info("checkMongo is OK in the context: {}", context);
			return buildHCRBuilder.build();
		} catch (Exception e) {
			log.error("Error on checkMongo: ", e);
			log.warn("checkMongo is KO in the context: {}", context);
			buildHCRBuilder.state(false);
			if (mongo != null) {
				MongoConnection connection = null;
				try {
					connection = mongo.getConnection();
					buildHCRBuilder = appendMongoInfo(buildHCRBuilder, connection);
				} catch (Exception e1) {
					buildHCRBuilder.withData("hosts", connection.getHosts() + "");
				}

			}
			return buildHCRBuilder.build();
		} finally {
			ScopeProvider.instance.reset();
		}
	}

	/**
	 * Append mongo info.
	 *
	 * @param buildHCRBuilder the build HCR builder
	 * @param connection the connection
	 * @return the health check response builder
	 */
	private HealthCheckResponseBuilder appendMongoInfo(HealthCheckResponseBuilder buildHCRBuilder,
			MongoConnection connection) {
		buildHCRBuilder.withData("hosts", connection.getHosts() + "");
		buildHCRBuilder.withData("db_name ", connection.getDatabase());
		return buildHCRBuilder;
	}

}

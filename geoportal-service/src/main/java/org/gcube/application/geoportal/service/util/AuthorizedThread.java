package org.gcube.application.geoportal.service.util;

import org.gcube.application.cms.implementations.utils.UserUtils.AuthenticatedUser;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class AuthorizedThread.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jul 2, 2024
 */
@Slf4j
public class AuthorizedThread extends Thread {

	private AuthenticatedUser authenticatedUser;
	private String umaToken;
	private String gcubeToken;

	/**
	 * Instantiates a new authorized thread.
	 *
	 * @param authenticatedUser the authenticated user
	 */
	public AuthorizedThread(AuthenticatedUser authenticatedUser) {
		this.authenticatedUser = authenticatedUser;
		setContext();
	}

	/**
	 * Sets the context.
	 */
	private void setContext() {

		if (authenticatedUser == null)
			return;

		log.debug("{} id {} setting context {}", AuthorizedThread.class.getSimpleName(), this.getId(),
				authenticatedUser.getContext());

		// Setting scope
		ScopeProvider.instance.set(authenticatedUser.getContext());

		// Setting umaToken
		umaToken = authenticatedUser.getUma_token();
		if (umaToken != null) {
			log.debug("{} id {} setting UMA token {}", AuthorizedThread.class.getSimpleName(), this.getId(),
					umaToken.substring(0, 20) + "_MASKED_TOKEN");
			AccessTokenProvider.instance.set(umaToken);
		}

		// Setting gcubeToken
		gcubeToken = authenticatedUser.getGcube_token();
		if (gcubeToken != null) {
			log.debug("{} id {} legacy token {}", AuthorizedThread.class.getSimpleName(), this.getId(),
					gcubeToken.substring(0, 20) + "_MASKED_TOKEN");
			SecurityTokenProvider.instance.set(gcubeToken);
		}

	}

	/**
	 * Reset context.
	 */
	public void resetContext() {
		log.debug("{} id {} resetting context called", AuthorizedThread.class.getSimpleName(), this.getId());

		ScopeProvider.instance.reset();

		if (umaToken != null)
			AccessTokenProvider.instance.reset();
		if (gcubeToken != null)
			SecurityTokenProvider.instance.reset();
	}

}

package org.gcube.application.geoportal.service.rest.health;

import java.io.IOException;
import java.util.Map;

import org.eclipse.microprofile.health.HealthCheckResponse;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HealthCheckResponseSerializer extends JsonSerializer<HealthCheckResponse> {

	@Override
	public void serialize(HealthCheckResponse response, JsonGenerator gen, SerializerProvider serializers)
			throws IOException {
		gen.writeStartObject();
		if (response.getName() != null)
			gen.writeStringField("name", response.getName());
		if (response.getState() != null)
			gen.writeStringField("state", response.getState().toString());

		response.getData().ifPresent(data -> {
			try {
				gen.writeObjectFieldStart("data");
				for (Map.Entry<String, Object> entry : data.entrySet()) {
					gen.writeObjectField(entry.getKey(), entry.getValue());
				}
				gen.writeEndObject();
			} catch (IOException e) {
				log.warn("Error on serializing the data field", e);
			}
		});

		gen.writeEndObject();
	}
}
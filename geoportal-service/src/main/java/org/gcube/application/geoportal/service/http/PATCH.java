package org.gcube.application.geoportal.service.http;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.HttpMethod;

/**
 * The Interface PATCH.
 * JAX-RS API 2.0.1 doesn't have PATCH defining it.
 * 
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 17, 2023
 */
@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@HttpMethod("PATCH")
public @interface PATCH {
}

package org.gcube.application.geoportal.service.health;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.eclipse.microprofile.health.HealthCheckResponse;
import org.gcube.application.geoportal.service.rest.health.HealthCheckResponseSerializer;

public class HealthCheckSerializer {

    public static void main(String[] args) throws Exception {
        HealthCheckResponse response = HealthCheckResponse.named("geooportal-service")
                                                          .state(true)
                                                          .withData("status", "healthy")
                                                          .build();
        
        // Configura ObjectMapper con il serializer personalizzato
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(HealthCheckResponse.class, new HealthCheckResponseSerializer());
        mapper.registerModule(module);
        
        // Serializza l'oggetto HealthCheckResponse in JSON
        String json = mapper.writeValueAsString(response);
        
        // Stampa il JSON serializzato
        System.out.println(json);
    }
    
    
    
}
package org.gcube.application.geoportal.service.engine.caches;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.caches.AbstractScopedMap;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

import java.time.LocalDateTime;

@Slf4j
public class DummyCache extends AbstractScopedMap<LocalDateTime> {


    public DummyCache() {
        super("Dummy cache");
    }

    @Override
    protected LocalDateTime retrieveObject(String context) throws ConfigurationException {
        return LocalDateTime.now();
    }

    @Override
    protected void dispose(LocalDateTime toDispose) {
    }

    @Override
    public void init() {
    }



}

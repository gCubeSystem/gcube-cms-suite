package org.gcube.application.geoportal.service.framework.lifecycleManagers;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.plugins.implementations.RoleManager;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.geoportal.common.model.plugins.LifecycleManagerDescriptor;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.junit.Test;

import java.util.List;

@Slf4j
public class RoleManagerTests {

    @Test
    public void testRead(){
        TestProfiles.profiles.forEach((s,u)->{
            System.out.println("Checking "+s);
            List<HandlerDeclaration> descriptors =u.getHandlersByType(LifecycleManagerDescriptor.LIFECYCLE_MANAGER_TYPE);
            descriptors.stream().forEach(h->{
                RoleManager r = null;
                try {
                    System.out.println("Role manager : "+ new RoleManager(h));
                } catch (ConfigurationException e){
                    log.error("Not supported "+ s,e);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }


            });
        });
    }

}

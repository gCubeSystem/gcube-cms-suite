package org.gcube.application.geoportal.service.engine.profiles;

import org.gcube.application.cms.tests.TestProfiles;
import org.junit.Test;

public class GenericTests {


    @Test
    public void getFields(){
        TestProfiles.profiles.forEach((k,v)->{
            System.out.println(k+"\t"+v);
            if(v.getSchema()!=null&&!v.getSchema().isLeaf()) {
                v.getSchema().getChildren().forEach(
                        i -> System.out.println(i.getClass() + "\t" + i)
                );
            }
        });
    }




}

package org.gcube.application.geoportal.service;

import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.application.geoportal.service.engine.providers.StorageClientProvider;

import java.io.IOException;

public class StorageTests {

    public static void main(String[] args) throws ConfigurationException, IOException {
        TokenSetter.set("/gcube/devsec/devVRE");
        StorageClientProvider storage= ImplementationProvider.get().getProvidedObjectByClass(StorageClientProvider.class);

        String id="614de23b647cef06aecdfb28";
        System.out.println("FROM PROVIDER " + storage.getObject().getURL(id));
        System.out.println("FROM CLASS" +new StorageUtils().getURL(id));
    }
}

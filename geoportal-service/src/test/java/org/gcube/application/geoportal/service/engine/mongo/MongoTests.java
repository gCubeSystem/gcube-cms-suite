package org.gcube.application.geoportal.service.engine.mongo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.Block;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.gcube.application.cms.implementations.ImplementationProvider;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.model.rest.ConfigurationException;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.gcube.application.geoportal.service.engine.providers.MongoClientProvider;
import org.gcube.application.geoportal.service.model.internal.db.Mongo;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assume.assumeTrue;

public class MongoTests {


	@BeforeClass
	public static final void init() {
		assumeTrue(GCubeTest.isTestInfrastructureEnabled());

		ImplementationProvider.get().setEngine(new MongoClientProvider() {
			@Override
			public Mongo getObject() throws ConfigurationException {
				TokenSetter.set(GCubeTest.getContext());
				return super.getObject();
			}
		},Mongo.class);
	}

	
	Block<Document> printBlock = new Block<Document>() {
	       @Override
	       public void apply(final Document document) {
	           System.out.println(document.toJson());
	       }
	};
	
	@Test
	public void listProfiles() throws JsonProcessingException, IOException, ConfigurationException {
//		MongoManager manager=new MongoManager();
//		UseCaseDescriptor f=Serialization.mapper.readerFor(UseCaseDescriptor.class).readValue(
//				Files.getFileFromResources("fakeProfile.json"));
//
//		manager.iterate(new Document(),f).forEach(printBlock);
	}

	@Test
	public void bruteForce(){

	}

	@Test
	public void queries() throws ConfigurationException {
		assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		MongoTester tester=MongoTester.getLegacy();
		System.out.println("Using builders..");
		tester.getCollection().find(Document.parse("{\"report.status\" : \"WARNING\"}")).
				projection(Projections.include("nome")).forEach(printBlock);
		System.out.println("Deserializing documents");
		tester.getCollection().find(Document.parse("{\"report.status\" : \"WARNING\"}")).
				projection(Document.parse("{\"nome\" : 1}")).forEach(printBlock);
	}

	@Test
	public void checkQuerySerialization(){
		System.out.println(Projections.include("nome"));
	}

//	@Test
//	public void writeProject() {
//		MongoManager manager=new MongoManager();
//		Concessione f=Serialization.mapper.readerFor(Concessione.class).readValue(
//				Files.getFileFromResources("fakeProfile.json"));
//	}
}

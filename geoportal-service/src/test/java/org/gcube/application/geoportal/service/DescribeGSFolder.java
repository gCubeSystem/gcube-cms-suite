package org.gcube.application.geoportal.service;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.data.transfer.library.DataTransferClient;
import org.gcube.data.transfer.library.faults.RemoteServiceException;
import org.gcube.data.transfer.library.faults.ServiceNotFoundException;
import org.gcube.data.transfer.library.faults.UnreachableNodeException;
import org.gcube.data.transfer.model.RemoteFileDescriptor;



@Slf4j
public class DescribeGSFolder {

    public static void main(String[] args) throws UnreachableNodeException, ServiceNotFoundException, RemoteServiceException {

        TokenSetter.set("/gcube/devsec/devVRE");

        String gsEndpoint="https://geoserver-218.dev.d4science.org/";
        String gsBasePath="geoserver/GNA/6157089e596924b380a174b7_1633101414296/";
        DataTransferClient client=DataTransferClient.getInstanceByEndpoint(gsEndpoint);
//        System.out.println(getFileDescriptor(gsBasePath,client.getWebClient()));
        print(client,gsBasePath,"");
    }


    public static void print(DataTransferClient client, String path, String pad) throws RemoteServiceException {
        RemoteFileDescriptor desc=client.getWebClient().getInfo(path);
        System.out.println(pad+desc.getFilename() + "["+desc.getSize()+"]");
        if(desc.isDirectory())
            desc.getChildren().forEach(f->{
                try {
                    print(client,path+"/"+f,pad+"\t");
                } catch (RemoteServiceException e) {
                    e.printStackTrace();
                }
            });

    }


}

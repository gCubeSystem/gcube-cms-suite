package org.gcube.application.geoportal.service;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.gcube.application.geoportal.service.profiledDocuments.AbstractProfiledDocumentsTests;
import org.junit.Test;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

public class InexistentUseCaseDescriptorTests extends AbstractProfiledDocumentsTests {

    private static String inexistentUCID= UUID.randomUUID().toString();

    @Override
    protected WebTarget baseTarget() {
        String testProfileId=inexistentUCID;
        return target(InterfaceConstants.Methods.PROJECTS).path(testProfileId);
    }

    // GET
    @Test
    public void testMissingProfile(){
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        Response resp = target(InterfaceConstants.Methods.PROJECTS)
                .path(inexistentUCID).request().get();
        assertEquals(404,resp.getStatus());
    }

}

package org.gcube.application.geoportal.service.profiledDocuments;

import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.DataAccessPolicy;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.WebTarget;

public class RolesTests extends AbstractProfiledDocumentsTests{


    @BeforeClass
    public static void setup(){
        //NB Profile is only in local resources
        USE_LOCAL_FOLDER=true;
    }

    @Override
    protected WebTarget baseTarget() {
        String testProfileId="rolesTesting";
        return target(InterfaceConstants.Methods.PROJECTS).path(testProfileId);
    }


    @Test
    public void testRoles(){
        // Insert for each
        UseCaseDescriptor ucd = TestProfiles.profiles.get("rolesTesting");


    }


    private void test(String role, DataAccessPolicy.Policy expected){
        // Collect behaviour
        // try access not owned
        // check against expected.getRead()
        // try access owned
        // check against expected.getRead()
        // try access any
        // check against expected.getRead()


        // Try create
        // check against expected.getWrite()
        // Try edit owned
        // check against expected.getWrite()
        // Try edit not owned
        // check against expected.getWrite()
    }
}

package org.gcube.application.geoportal.service.ws;

import org.gcube.application.cms.implementations.WorkspaceManager;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public class MoveWSFolder {

    public static void main(String[] args) throws StorageHubException {
        // Get original folder id
                // Set original VRE token

        TokenSetter.set("...");

                // Get appBaseFolder ID
                StorageHubClient shc = new StorageHubClient();
                FolderContainer toMoveFolder= WorkspaceManager.getApplicationBaseFolder(shc);

        System.out.println("TO MOVE FOLDR ID IS "+toMoveFolder.getId());
        // Set token for target VRE
        // MV
        String destinationFolderID = "...";
        FolderContainer destinationFolder = shc.open(destinationFolderID).asFolder();

        System.out.println("MOVING NOW ..");
        toMoveFolder.move(destinationFolder);
        System.out.println("DONE");
    }
}

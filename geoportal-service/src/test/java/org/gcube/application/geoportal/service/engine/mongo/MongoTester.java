package org.gcube.application.geoportal.service.engine.mongo;

import org.gcube.application.geoportal.common.model.rest.ConfigurationException;

public class MongoTester extends MongoManager {

    public static final MongoTester getLegacy() throws ConfigurationException {
        return new MongoTester("legacyConcessioni",ID);}



    private String theID=null;



    public MongoTester(String profile) throws ConfigurationException {
        this(profile,ID);
    }
    public MongoTester(String profile,String id) throws ConfigurationException {
        init(profile);
        theID=id;
    }

    @Override
    protected String mongoIDFieldName() {
        return theID;
    }
}

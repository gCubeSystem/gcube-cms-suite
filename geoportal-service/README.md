gCube CMS Suite : GeoPortal Service
--------------------------------------------------

gCube CMS Service is a gCube REST-like Application developed on top of gCube SmartGears.

It implements the core business logic of gCube CMS Suite, exposing methods for the management of Projects, Use Case Descriptors and available server plugins.

It is built on top of Jersey Framework, it uses a MongoDB Document Store for JSON archiving and Querying.
It exploits the infrastructure StorageHub for archiving FileSets and the Information System in order to discover available resources.

Its main goal is to receive client calls and orchestrate plugins implementation in order to serve them according to the UCD defined in the caller context.
 
## Built with
* [gCube SDI] (https://gcube.wiki.gcube-system.org/gcube/) - The gCube SDI
* [gCube SmartGears] (https://gcube.wiki.gcube-system.org/gcube/SmartGears) - The gCube SmartGears framework 
* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [JAX-RS](https://github.com/eclipse-ee4j/jaxrs-api) - Java™ API for RESTful Web Services
* [Jersey](https://jersey.github.io/) - JAX-RS runtime
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation
[gCube CMS Suite](../) parent module containing references, documentation, guides ad utilities.


## Change log

See [CHANGELOG.md](CHANGELOG.md).

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.
 
## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - iMarine(grant no. 283644);
    - EUBrazilOpenBio (grant no. 288754).
- the H2020 research and innovation programme 
    - SoBigData (grant no. 654024);
    - PARTHENOS (grant no. 654119);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - BlueBRIDGE (grant no. 675680);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);


package geoportal_to_catalogue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.gcube.application.cms.cataloguebinding.doaction.CatalogueCaller;
import org.gcube.application.cms.cataloguebinding.freemarker.MappingToCatalogue;
import org.gcube.application.geoportal.common.model.configuration.Archive;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.ORDER;
import org.gcube.application.geoportalcommon.shared.WhereClause;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import test.TestContextConfig;

/**
 * The Class GeoportalToCatalogueBatchPublisher.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 25, 2024
 */
@Slf4j
public class GeoportalToCatalogueBatchPublisher {

	private static final int SLEEPING_TIME = 5000;

	public final static String profileID = "profiledConcessioni";
	//public final static String profileID = "concessioni-estere";

	public final static Integer MAX_ITEMS = 100;
	public static int limit = 10;
	public static int offset = 0;

	static PrintWriter reportPrintWriter;
	static PrintWriter errorPrintWriter;
	private static long startTime;

	private static final String CSV_DELIMITER = ";";

	private static boolean useGeoportalServiceAccount = true;

	// if true append the data published or error in the report files.
	// Otherwise create new files.
	private static boolean appendErrors = true;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	//@Test
	public void testPublish() {

		procedureToPublishProjectsOnCatalogue();

		// String projectId = " 6663016a312dc236d217be5c";
		// checkMappingToJSONForProjectByID(projectId);

	}

	public static void checkMappingToJSONForProjectByID(String projectID) {
		TestContextConfig.readContextSettings();
		String sourceScope = TestContextConfig.CONTEXT;
		String sourceToken = TestContextConfig.TOKEN;

		ScopeProvider.instance.set(sourceScope);
		SecurityTokenProvider.instance.set(sourceToken);
		ProjectsCaller clientProjects = GeoportalClientCaller.projects();
		Projects<Project> client = (Projects<Project>) clientProjects.getClient(profileID);

		try {
			UseCaseDescriptor ucd = GeoportalClientCaller.useCaseDescriptors().getUCDForId(profileID);
			Project theProject = client.getById(projectID);
			log.info("\n\nConverting the project {}", theProject.getId());
			String toCatalogueJSON = mapToCatalogue(theProject, ucd, sourceScope, sourceToken);
			String jsonPretty = prettyPrintUsingGson(toCatalogueJSON);
			log.info(jsonPretty);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static void procedureToPublishProjectsOnCatalogue() {
		TestContextConfig.readContextSettings();
		String sourceScope = TestContextConfig.CONTEXT;
		String sourceToken = TestContextConfig.TOKEN;

		// Setting empty SecretManager for test case
		// SecretManagerProvider.instance.set(new SecretManager());

		String reportFile = TestContextConfig.CONTEXT.replaceAll("/", "_") + "_report_.csv";
		String errorFile = TestContextConfig.CONTEXT.replaceAll("/", "_") + "_error_.csv";
		try {
			if (!appendErrors) {
				Files.deleteIfExists(Paths.get(reportFile));
				Files.deleteIfExists(Paths.get(errorFile));
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			FileWriter reportWriter = new FileWriter(reportFile, appendErrors);
			FileWriter errorWriter = new FileWriter(errorFile, appendErrors);
			BufferedWriter reportBW = new BufferedWriter(reportWriter);
			BufferedWriter errorBW = new BufferedWriter(errorWriter);
			reportPrintWriter = new PrintWriter(reportBW);
			reportPrintWriter.println("NB." + CSV_DELIMITER + " PROJECT_ID" + CSV_DELIMITER + " MAPPING_TO_CATALOGUE"
					+ CSV_DELIMITER + "");
			errorPrintWriter = new PrintWriter(errorBW);
			errorPrintWriter.println("NB." + CSV_DELIMITER + " PROJECT_ID" + CSV_DELIMITER + " MAPPING_TO_CATALOGUE"
					+ CSV_DELIMITER + " ERROR" + CSV_DELIMITER + "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Procedure starting at: " + getFormattedDateTime());
		startTime = System.currentTimeMillis();

		Integer totalProjects = null;
		try {
			ScopeProvider.instance.set(sourceScope);
			SecurityTokenProvider.instance.set(sourceToken);
			totalProjects = getTotalProjects(profileID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (totalProjects == null) {
			log.error("No document found!!!");
			return;
		}

		log.info("Total projects are: " + totalProjects);
		boolean documentFound = true;
		boolean maxItemsReached = false;

		int counter = 0;
		try {
			while (documentFound && !maxItemsReached) {
				try {

					/**
					 * 
					 * ##### NB!!!! USING SOURCE SCOPE
					 * 
					 */
					ScopeProvider.instance.set(sourceScope);
					SecurityTokenProvider.instance.set(sourceToken);
					List<Project> list = getListProjects(limit, offset);
					log.info("limit {}, offset {}", limit, offset);

					if (list.size() > 0) {
						UseCaseDescriptor ucd = GeoportalClientCaller.useCaseDescriptors().getUCDForId(profileID);
						for (int i = 0; i < list.size(); i++) {
							counter++;
							log.info("\n\nConuter is: " + counter);
							if (MAX_ITEMS != null && counter > MAX_ITEMS) {
								maxItemsReached = true;
								log.info("\n###### MAX_ITEMS reached: " + maxItemsReached);
								break;
							}

							Project theProject = list.get(i);
							log.info("\n\nConverting the project {}", theProject.getId());
							String toCatalogueJSON = "";
							try {
								toCatalogueJSON = mapToCatalogue(theProject, ucd, sourceScope, sourceToken);

								/**
								 * 
								 * ##### NB!!!! USING TARGET SCOPE
								 * 
								 */
								String targetScope = "/gcube/devsec/devVRE";
								String targetToken = "";

								if (targetScope == null || targetToken == null) {
									throw new Exception("Check targetScope and/or targetToken!!!");
								}

								ScopeProvider.instance.set(targetScope);
								SecurityTokenProvider.instance.set(targetToken);

								SecretManager secretManager = new SecretManager();
								Secret secret = new GCubeSecret(targetToken);
								secretManager.addSecret(secret);
								SecretManagerProvider.instance.set(secretManager);

								log.info("\n publishOnCatalogue the PROJECT N. " + counter + " with id: "
										+ theProject.getId());

								publishOnCatalogue(theProject, toCatalogueJSON);

								log.info("\n end publishOnCatalogue the PROJECT N. " + counter);

								writeReport(counter + CSV_DELIMITER + " " + theProject.getId() + CSV_DELIMITER + "'"
										+ toCatalogueJSON + "'" + CSV_DELIMITER);

								log.info("\n###### sleeping " + counter);
								Thread.sleep(SLEEPING_TIME);

							} catch (Exception e) {
								log.error("ERROR on publishing project with id: " + theProject.getId());
								writeError(counter + CSV_DELIMITER + " " + theProject.getId() + CSV_DELIMITER + " '"
										+ toCatalogueJSON + "'" + CSV_DELIMITER + " '" + e.getMessage() + "'"
										+ CSV_DELIMITER);
							}
						}
					} else {
						documentFound = false;
					}

					offset = offset + limit;

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					documentFound = false;
				}

			}
		} catch (Exception e) {
			log.error("External error");
			e.printStackTrace();
		} finally {
			if (reportPrintWriter != null)
				reportPrintWriter.close();

			if (errorPrintWriter != null)
				errorPrintWriter.close();

			System.out.println("\n\nFINISHED!!!");
			System.out.println("Procedure terimated at: " + getFormattedDateTime());
			long millis = System.currentTimeMillis() - startTime;
			long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
			System.out.println("Done! In ms: " + (millis) + ", minutes: " + minutes);
		}

	}

	/**
	 * Publish on catalogue.
	 *
	 * @param theProject      the the project
	 * @param toCatalogueJSON the to catalogue JSON
	 * @throws Exception the exception
	 */
	public static void publishOnCatalogue(Project theProject, String toCatalogueJSON) throws Exception {

		new CatalogueCaller().createOrUpdateTheDatasetOnCatalogue(theProject.getId(), toCatalogueJSON,
				useGeoportalServiceAccount);
	}

	/**
	 * Map to catalogue.
	 *
	 * @param theProject  the the project
	 * @param ucd         the ucd
	 * @param sourceScope the source scope
	 * @param sourceToken the source token
	 * @return the string
	 * @throws TemplateException the template exception
	 * @throws IOException       Signals that an I/O exception has occurred.
	 */
	public static String mapToCatalogue(Project theProject, UseCaseDescriptor ucd, String sourceScope,
			String sourceToken) throws TemplateException, IOException {
		log.info("\n\nMapping the PROJECT with id: " + theProject.getId());
		ScopeProvider.instance.set(sourceScope);
		SecurityTokenProvider.instance.set(sourceToken);
		Template fmTemplate = getLocalFreemarkerTemplate();
		String toCatalogueJSON = MappingToCatalogue.apply(fmTemplate, theProject, ucd, sourceScope);

		log.trace("mapping is: " + toCatalogueJSON);
		return toCatalogueJSON;

	}

	/**
	 * Gets the list projects.
	 *
	 * @return the list projects
	 * @throws Exception the exception
	 */
	public static List<Project> getListProjects() throws Exception {
		ProjectsCaller clientProjects = GeoportalClientCaller.projects();
		return clientProjects.getListForProfileID(profileID);
	}

	/**
	 * Gets the list projects.
	 *
	 * @param limit  the limit
	 * @param offset the offset
	 * @return the list projects
	 * @throws Exception the exception
	 */
	public static List<Project> getListProjects(int limit, int offset) throws Exception {
		ProjectsCaller clientProjects = GeoportalClientCaller.projects();
		SearchingFilter filter = new SearchingFilter();
		filter.setOrder(ORDER.DESC);
		List<ItemFieldDV> orederByFields = new ArrayList<ItemFieldDV>();
		// Order By
		ItemFieldDV fieldUpdated = new ItemFieldDV("Updated", Arrays.asList("_info._lastEditInfo._instant"), "$or",
				true, true, true);
		orederByFields.add(fieldUpdated);

		// Where Conditions
		List<WhereClause> conditions = new ArrayList<WhereClause>();
		Map<String, Object> searchInto = new HashMap<String, Object>();
		searchInto.put("_lifecycleInformation._phase", "Published");
		conditions.add(new WhereClause(LOGICAL_OP.OR, searchInto));
		filter.setConditions(conditions);

		filter.setOrderByFields(orederByFields);
		Iterator<Project> iterator = clientProjects.queryOnMongo(profileID, 300, offset, limit, filter);
		List<Project> listP = new ArrayList<Project>();
		while (iterator.hasNext()) {

			listP.add(iterator.next());
		}

		return listP;

	}

	/**
	 * Gets the total projects.
	 *
	 * @param profileID the profile ID
	 * @return the total projects
	 * @throws Exception the exception
	 */
	public static Integer getTotalProjects(String profileID) throws Exception {
		System.out.println("getTotalDocument called for profileID: " + profileID);
		ProjectsCaller clientProjects = GeoportalClientCaller.projects();
		Projects<Project> client = (Projects<Project>) clientProjects.getClient(profileID);
		org.gcube.application.geoportal.common.model.configuration.Configuration config = client.getConfiguration();
		List<Archive> listArchives = config.getArchives();

		for (Archive archive : listArchives) {
			String theType = archive.getString("_type");
			if (theType.equalsIgnoreCase(ProjectsCaller.DOCUMENT_STORE_COLLECTION)) {
				String totalDocumentAre = archive.get("count").toString();
				int total = Integer.parseInt(totalDocumentAre);
				log.info("total docs for profileID: {}, are: {}", profileID, total);
				return total;
			}
		}

		return null;
	}

	/**
	 * Fmt json util.
	 *
	 * @param json the json
	 * @return the map
	 * @throws JsonParseException   the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException          Signals that an I/O exception has occurred.
	 */
	public static Map<String, Object> fmtJsonUtil(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper OBJECT_MAPPER = new ObjectMapper();
		return OBJECT_MAPPER.readValue(json, new TypeReference<HashMap<String, Object>>() {
		});

	}

	/**
	 * Gets the local freemarker template.
	 *
	 * @return the local freemarker template
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static Template getLocalFreemarkerTemplate() throws IOException {

		// Create your Configuration instance, and specify if up to what FreeMarker
		// version (here 2.3.32) do you want to apply the fixes that are not 100%
		// backward-compatible. See the Configuration JavaDoc for details.
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		// Specify the source where the template files come from. Here I set a
		// plain directory for it, but non-file-system sources are possible too:
		cfg.setDirectoryForTemplateLoading(
				new File(System.getProperty("user.dir") + "/src/test/java/geoportal_to_catalogue"));
		// Set the preferred charset template files are stored in. UTF-8 is
		// a good choice in most applications:
		cfg.setDefaultEncoding("UTF-8");

		Template template = cfg.getTemplate("d4gna_to_catalogue_template.ftl");
		return template;
	}

	/**
	 * Pretty print using gson.
	 *
	 * @param uglyJson the ugly json
	 * @return the string
	 */
	public static String prettyPrintUsingGson(String uglyJson) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement jsonElement = new JsonParser().parse(uglyJson);
		String prettyJsonString = gson.toJson(jsonElement);
		return prettyJsonString;
	}

	/**
	 * Write report.
	 *
	 * @param newline the newline
	 */
	private static synchronized void writeReport(String newline) {
		reportPrintWriter.println(newline);
		reportPrintWriter.flush();
	}

	/**
	 * Write error.
	 *
	 * @param newline the newline
	 */
	private static synchronized void writeError(String newline) {
		errorPrintWriter.println(newline);
		errorPrintWriter.flush();
	}

	/**
	 * Gets the formatted date time.
	 *
	 * @return the formatted date time
	 */
	public static String getFormattedDateTime() {
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
		Date resultdate = new Date(yourmilliseconds);
		return sdf.format(resultdate);
	}

	/*
	 * public static void main(String[] args) {
	 * 
	 * try {
	 * 
	 * TestContextConfig.readContextSettings();
	 * 
	 * ScopeProvider.instance.set(TestContextConfig.CONTEXT);
	 * SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);
	 * 
	 * ProjectsCaller clientProjects = GeoportalClientCaller.projects(); Project
	 * theProject = clientProjects.getProjectByID(profileID, projectID); Document
	 * asDocument = SerializationUtil.asDocument(theProject);
	 * 
	 * UseCaseDescriptor ucd =
	 * GeoportalClientCaller.useCaseDescriptors().getUCDForId(profileID);
	 * 
	 * System.out.println("Source JSON:");
	 * System.out.println(prettyPrintUsingGson(asDocument.toJson()));
	 * applyMappingToCatalogue(theProject, ucd);
	 * 
	 * } catch (Exception e) { // TODO: handle exception }
	 * 
	 * }
	 */

}

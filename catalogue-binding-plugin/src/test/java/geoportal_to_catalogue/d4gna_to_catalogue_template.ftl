<#assign jsonProj=doc>
<#assign theDocument=jsonProj._theDocument>

<#assign resources=[]>
<#assign groups=[]>
<#assign tags=[]>
<#assign extras=[]>

<#function sanitizeString(value)>
<#if value??>
<#-- Removing new lines and slashes -->
  <#return value?replace('\n','')?replace('\"','')?trim>
</#if>
<#return "">  
</#function>

<#function onlyAlphanumericString(value)>
<#if value??>
<#-- Removing new lines, slashes and not alphanumeric characters except for '-','_','.' -->
  <#return value?replace('\n','')?replace('\"','')?replace("[^\\w^.^-^_]+", " ", "r")?trim>
</#if>
<#return "">  
</#function>

<#-- Macro to assign Tags -->
<#macro assignTag in_tags>
<#if in_tags??>
	<#if in_tags?is_sequence>
	  <#list in_tags as my_tag>
		<#assign tags = tags +  [onlyAlphanumericString(my_tag)] />
	  </#list>
	 <#else>
	  	<#assign tags = tags +  [onlyAlphanumericString(in_tags)] />
	 </#if>
	</#if>
</#macro>

<#-- Macro to assign a Resource -->
<#macro assignResource name url description format>
<#if description??>
<#else>
<#assign description = ""/>
</#if>
<#if url??>
	<#assign resource = {"name": name, "url": url, "description": sanitizeString(description), "format": format}>
	<#assign resources = resources + [resource]/>
</#if>
</#macro>

<#-- Macro to assign Groups -->
<#macro assignGroup in_groups>
<#if in_groups??>
	<#if in_groups?is_sequence>
	  <#list in_groups as my_group>
		<#assign groups = groups +  [my_group] />
	 </#list>
	 <#else>
	  	<#assign groups = groups +  [in_groups] />
	 </#if>
	</#if>
</#macro>

<#-- Macro to assign Extras field -->
<#macro assignExtraField key value asObject>
<#if key??>
<#assign checkedValue = value/>
<#if key != "spatial">
<#assign checkedValue = sanitizeString(value)/>
</#if>
	<#assign extra = {"key": sanitizeString(key), "value": checkedValue, "asObject": asObject}>
	<#assign extras = extras + [extra]/>
</#if>
</#macro>

<#-- Macro to build the tags as [{},{},..{}] -->
<#macro buildTags the_tags>
<#if the_tags??>
 "tags": [
  <#list the_tags as tag>
	{
	"name": "${tag}"
	}
	<#if tag?is_last==false>,</#if>
 </#list>
 ],
</#if>
</#macro>

<#-- Macro to build the groups as [{},{},..{}] -->
<#macro buildGroups the_groups>
<#if the_groups??>
 "groups": [
  <#list the_groups as the_group>
	{
	"name": "${the_group}"
	}
	<#if the_group?is_last==false>,</#if>
 </#list>
 ],
</#if>
</#macro>

<#-- Macro to build the resources as [{},{},..{}] -->
<#macro buildResources the_resources>
<#if the_resources??>
 "resources": [
  <#list the_resources as resource>
  	{
	 "name": "${resource.name}",
	 "url": "${resource.url}",
	 "description": "${resource.description}", 
	 "format": "${resource.format}"  
	}
	<#if resource?is_last==false>,</#if>
 </#list>
 ],
</#if>
</#macro>

<#-- Macro to build the extras as [{},{},..{}] -->
<#macro buildExtrasFields the_extras>
<#if the_extras??>
 "extras": [
  <#list the_extras as extra>
  	<#if extra.asObject?? && extra.asObject>
  	{
	 "key": "${extra.key}",
	 "value": ${extra.value}
	}
	<#else>
	{
	 "key": "${extra.key}",
	 "value": "${extra.value}" 
	}
	</#if>
	<#if extra?is_last==false>,</#if>
 </#list>
 ]
</#if>
</#macro>



<#-- 
############################################################
#
# 
#  Starting document mapping to Catalogue
#
#
############################################################
-->
<#compress>
{
 "name": "${jsonProj._id}",
 "title": "${sanitizeString(theDocument.nome)}",
 "license_id": "CC-BY-SA-4.0",
 "private": false,
 <#assign sanitizedNotes = sanitizeString(theDocument.introduzione) />
 "notes": "${sanitizedNotes}",

<#-- 
######################

  RESOURCES
  
######################
-->

<#-- Mapping "Relazione Scavo" as resource -->
<#if theDocument.relazioneScavo?? && theDocument.relazioneScavo.fileset?? && theDocument.relazioneScavo.fileset._payloads??>
<#assign payloads = theDocument.relazioneScavo.fileset._payloads>
<#if payloads?size!=0>
<#-- Getting the first payload -->
<#assign payload = payloads[0]>
<@assignResource name=payload._name url=payload._link description=theDocument.relazioneScavo.titolo format=payload._mimetype></@assignResource>
</#if>
</#if>

<#-- Mapping "Immagine Rappresentative" as resource -->
<#if theDocument.immaginiRappresentative??>
	<#if theDocument.immaginiRappresentative?is_sequence && theDocument.immaginiRappresentative?size gt 0>
	  <#list theDocument.immaginiRappresentative as the_image>
	  	<#if the_image.fileset?? && the_image.fileset._payloads??>
	  		<#assign payloads = the_image.fileset._payloads>
			<#if payloads?size!=0>
			<#-- Getting the first payload -->
			<#assign payload = payloads[0]>
			<@assignResource name=payload._name url=payload._link description=the_image.didascalia format=payload._mimetype></@assignResource>
			</#if>
	  	</#if>
	  </#list>
	</#if>
</#if>

<#-- Building Resources -->
<@buildResources the_resources=resources></@buildResources>


<#-- 
######################

  TAGS
  
######################
-->

<#-- Mapping "paroleChiaveLibere" as tag -->
<#if theDocument.paroleChiaveLibere??>
<@assignTag in_tags=theDocument.paroleChiaveLibere></@assignTag>
</#if>

<#-- Building tags -->
<@buildTags the_tags=tags></@buildTags>


<#-- 
######################

  GROUPS
  
######################
-->

<#-- Mapping the year of the "dataInizioProgetto" as group -->
<#assign dateString = theDocument.dataInizioProgetto?trim>
<#assign dataInizioYear = dateString?split("-")[0]>
<#-- <@assignGroup in_groups=dataInizioYear></@assignGroup> -->
<#-- Building groups -->
<@buildGroups the_groups=groups></@buildGroups>


<#-- 
######################

  EXTRAS
  
######################
-->

<#-- Mapping extras fields -->

 <#-- profiled-concessioni -->
<#if theDocument.doi??>
<@assignExtraField key="DOI" value=theDocument.doi asObject=false></@assignExtraField>
</#if>

<#if theDocument.contributore??>
<@assignExtraField key="Contributore" value=theDocument.contributore asObject=false></@assignExtraField>
</#if>

<#if theDocument.responsabile??>
<@assignExtraField key="Responsabile dei contenuti" value=theDocument.responsabile asObject=false></@assignExtraField>
</#if>

<#if theDocument.editore??>
<@assignExtraField key="Ente responsabile del progetto" value=theDocument.editore asObject=false></@assignExtraField>
</#if>

<#-- profiled-concessioni -->
<#if theDocument.ufficioMic??>
<@assignExtraField key="Ufficio MiC competente per territorio" value=theDocument.ufficioMic asObject=false></@assignExtraField>
</#if>

<#-- concessioni-estere -->
<#if theDocument.ufficioCompetente??>
<@assignExtraField key="Ufficio estero competente per territorio" value=theDocument.ufficioCompetente asObject=false></@assignExtraField>
</#if>

<#if theDocument.funzionarioResponsabile??>
<@assignExtraField key="Funzionario Responsabile" value=theDocument.funzionarioResponsabile asObject=false></@assignExtraField>
</#if>

<#if theDocument.fontiFinanziamento??>
<#if theDocument.fontiFinanziamento?is_sequence>
  <#list theDocument.fontiFinanziamento as my_extra>
	<@assignExtraField key="Fonte del finanziamento" value=my_extra asObject=false></@assignExtraField>
  </#list>
 <#else>
  	<@assignExtraField key="Fonte del finanziamento" value=theDocument.fontiFinanziamento asObject=false></@assignExtraField>
 </#if>
</#if>

<#if theDocument.risorseCorrelate??>
<#if theDocument.risorseCorrelate?is_sequence>
  <#list theDocument.risorseCorrelate as my_extra>
	<@assignExtraField key="Risorse Correlate" value=my_extra asObject=false></@assignExtraField>
  </#list>
 <#else>
  	<@assignExtraField key="Risorse Correlate" value=theDocument.risorseCorrelate asObject=false></@assignExtraField>
 </#if>
</#if>

<#if theDocument.dataInizioProgetto??>
<@assignExtraField key="Data inizio Campagna" value=theDocument.dataInizioProgetto?trim asObject=false></@assignExtraField>
</#if>

<#if theDocument.dataFineProgetto??>
<@assignExtraField key="Data fine Campagna" value=theDocument.dataFineProgetto?trim asObject=false></@assignExtraField>
</#if>

<#if theDocument.modalitaIndividuazione??>
<#if theDocument.modalitaIndividuazione?is_sequence>
  <#list theDocument.modalitaIndividuazione as my_extra>
	<@assignExtraField key="Modalità di individuazione" value=my_extra asObject=false></@assignExtraField>
  </#list>
 <#else>
  	<@assignExtraField key="Modalità di individuazione" value=theDocument.modalitaIndividuazione asObject=false></@assignExtraField>
 </#if>
</#if>

<#if theDocument.contestoIndagine??>
<#if theDocument.contestoIndagine?is_sequence>
  <#list theDocument.contestoIndagine as my_extra>
	<@assignExtraField key="Contesto Indagine" value=my_extra asObject=false></@assignExtraField>
  </#list>
 <#else>
  	<@assignExtraField key="Contesto Indagine" value=theDocument.contestoIndagine asObject=false></@assignExtraField>
 </#if>
</#if>

<#if theDocument.statoAttuale??>
<@assignExtraField key="Stato attuale" value=theDocument.statoAttuale asObject=false></@assignExtraField>
</#if>

<#if theDocument.accessibilita??>
<@assignExtraField key="Accessibilità" value=theDocument.accessibilita asObject=false></@assignExtraField>
</#if>

<#if theDocument.cronologiaMacrofase??>
<#if theDocument.cronologiaMacrofase?is_sequence>
  <#list theDocument.cronologiaMacrofase as my_extra>
	<@assignExtraField key="Cronologia Macrofase" value=my_extra asObject=false></@assignExtraField>
  </#list>
 <#else>
  	<@assignExtraField key="Cronologia Macrofase" value=theDocument.cronologiaMacrofase asObject=false></@assignExtraField>
 </#if>
</#if>

<#-- spatial field -->
<#if jsonProj._identificationReferences?? && jsonProj._identificationReferences?size!=0 && jsonProj._identificationReferences[0].geoJSON??>
<#assign geoJSON  = jsonProj._identificationReferences[0].geoJSON>
<#-- if coordinates exists, managing it as a Point -->
<#if geoJSON.coordinates?? && geoJSON.coordinates?is_sequence && geoJSON.coordinates?size gt 2>
	<#assign point_Coordinates = [geoJSON.coordinates[0], geoJSON.coordinates[1]]>
	<#assign spatialField  = '{\\"type\\": \\"${geoJSON.type}\\", \\"coordinates\\": [${point_Coordinates?join(", ")}]}'>
	<@assignExtraField key="spatial" value=spatialField asObject=false></@assignExtraField>
</#if>
</#if>

<#-- Adding extra field "Anno" to add it as group -->
<@assignExtraField key="Anno" value=dataInizioYear asObject=false></@assignExtraField>

<@buildExtrasFields the_extras=extras></@buildExtrasFields>
}
</#compress>

package geoportal_to_catalogue;

import org.gcube.application.cms.cataloguebinding.doaction.CatalogueCaller;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.json.JSONArray;
import org.json.JSONException;

import test.TestContextConfig;

public class CataloguePurgeDatasets {
	
	private static final int SLEEPING_TIME = 7000;
	private static boolean useGeoportalServiceAccount = true;
	
	public static void main(String[] args) {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		TestContextConfig.readContextSettings();

		System.out.println("CONTEXT: " + TestContextConfig.CONTEXT);
		System.out.println("TOKEN: " + TestContextConfig.TOKEN);
		
		TokenSetter.set(GCubeTest.getContext());

		int limit = 10;
		int offset = 0;

		boolean resultExists = true;
		

		while (resultExists) {
			System.out.println("offset: " + offset + ", limit: " + limit);
			ScopeProvider.instance.set(TestContextConfig.CONTEXT);
			SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);
			String items = new CatalogueCaller().list(limit, offset, useGeoportalServiceAccount);
			System.out.println("items: " + items);
			if (items != null) {
				resultExists = true;
				JSONArray array;
				try {
					array = new JSONArray(items);

					if (array.length() > 0) {
						purgeDatasets(array);
					} else
						resultExists = false;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				offset = limit + offset;
			} else
				resultExists = false;

		}

		System.out.println("done!");
	}

	public static void purgeDatasets(JSONArray array) {

		try {

			int index = 0;
			for (int i = 0; i < array.length(); i++) {
				index++;
				String datasetName = array.getString(i);
				System.out.println(index + ") Deleting dataset name : " + datasetName);
				ScopeProvider.instance.set(TestContextConfig.CONTEXT);
				SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);
				new CatalogueCaller().deleteDatasetOnCatalogue(datasetName, useGeoportalServiceAccount);
				System.out.println("sleeping..." + SLEEPING_TIME);
				Thread.sleep(SLEEPING_TIME);
			}

			System.out.println("done!");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void purgeDatasets(int limit, int offset) {

		TestContextConfig.readContextSettings();
		try {
			ScopeProvider.instance.set(TestContextConfig.CONTEXT);
			SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);
			String items = new CatalogueCaller().list(limit, offset, useGeoportalServiceAccount);
			System.out.println("items: " + items);

			JSONArray array = new JSONArray(items);
			for (int i = 0; i < array.length(); i++) {
				String datasetName = array.getString(i);
				System.out.println(i + ") name : " + datasetName);
				ScopeProvider.instance.set(TestContextConfig.CONTEXT);
				SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);
				new CatalogueCaller().deleteDatasetOnCatalogue(datasetName, useGeoportalServiceAccount);

				Thread.sleep(SLEEPING_TIME);
			}

			System.out.println("done!");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

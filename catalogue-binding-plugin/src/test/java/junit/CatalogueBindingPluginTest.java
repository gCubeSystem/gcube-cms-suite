package junit;

import org.gcube.application.cms.cataloguebinding.CatalogueBindingPlugin;
import org.gcube.application.cms.cataloguebinding.config.CatalogueBindingPluginConfigModel;
import org.gcube.application.cms.implementations.utils.UserUtils;
import org.gcube.application.cms.plugins.events.EventManager;
import org.gcube.application.cms.plugins.events.EventManager.Event;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.cms.plugins.faults.InitializationException;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.plugins.BasicPluginTest;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.AccountingInfo;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Test;

import test.TestContextConfig;

/**
 * The Class CatalogueBindingPluginTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 20, 2024
 */
public class CatalogueBindingPluginTest extends BasicPluginTest {

	public final static String profileID = "profiledConcessioni";
	public final static String projectID = "661d2c6f8804530afb90b132";

	/**
	 * Check plugin.
	 */
	// @Test
	public void checkPlugin() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
//
//		try {
//			plugin.init();
//		} catch (InitializationException e1) {
//			e1.printStackTrace();
//		}
//		try {
//			plugin.initInContext();
//		} catch (InitializationException e1) {
//			e1.printStackTrace();
//		}

		System.out.println("Plugin check: " + plugin);

	}

	/**
	 * Check plugin config.
	 */
	//@Test
	public void checkPluginConfig() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		try {
			plugin.init();
		} catch (InitializationException e1) {
			e1.printStackTrace();
		}
		try {
			plugin.initInContext();
		} catch (InitializationException e1) {
			e1.printStackTrace();
		}

		try {
			CatalogueBindingPluginConfigModel pluginBindingModel = plugin
					.readEventsSubscribedFromConfigurationInTheUCD(descriptor);
			System.out.println("CatalogueBindingPluginConfigModel: " + pluginBindingModel);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Check notify APPROV E SUBMITTE D on catalogue.
	 */
	//@Test
	public void checkNotify_APPROVE_SUBMITTED_onCatalogue() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		CatalogueBindingPluginConfigModel pluginBindingModel;
		try {
			pluginBindingModel = plugin.readEventsSubscribedFromConfigurationInTheUCD(descriptor);
			System.out.println("Events: " + pluginBindingModel);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ItemObserved<Project> item;
		try {
			EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
			item = mockItemObserverd(event);

			// Setting creator
			User creator = new User();
			creator.setUsername("francesco.mangiacrapa");
			item.getProject().getInfo().getCreationInfo().setUser(creator);

			LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();

			// test PUBLISH ON CATALOGUE
			lifecycleInfo.setPhase("Published");
			lifecycleInfo.setLastInvokedStep("APPROVE-SUBMITTED");

			System.out.println("By notifying event " + event + " project " + item.getProjectId());
			EventManager.getInstance().notify(event, item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Check notify UNPUBLIS H on catalogue.
	 */
	@Test
	public void checkNotify_UNPUBLISH_onCatalogue() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		CatalogueBindingPluginConfigModel pluginBindingModel;
		try {
			pluginBindingModel = plugin.readEventsSubscribedFromConfigurationInTheUCD(descriptor);
			System.out.println("Events: " + pluginBindingModel);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ItemObserved<Project> item;
		try {
			EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
			item = mockItemObserverd(event);

			// Setting creator
			User creator = new User();
			creator.setUsername("francesco.mangiacrapa");
			item.getProject().getInfo().getCreationInfo().setUser(creator);

			LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();

			// Test delete the dataset on CATALOGUE
			lifecycleInfo.setPhase("DRAFT");
			lifecycleInfo.setLastInvokedStep("UNPUBLISH");

			System.out.println("By notifying event " + event + " project " + item.getProjectId());
			EventManager.getInstance().notify(event, item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Check notify PROJEC T CREATE D on catalogue.
	 */
	// @Test
	public void checkNotify_PROJECT_CREATED_onCatalogue() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		CatalogueBindingPluginConfigModel pluginBindingModel;
		try {
			pluginBindingModel = plugin.readEventsSubscribedFromConfigurationInTheUCD(descriptor);
			System.out.println("Events: " + pluginBindingModel);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ItemObserved<Project> item;
		try {
			EventManager.Event event = Event.PROJECT_CREATED;
			item = mockItemObserverd(event);

			// Setting creator
			User creator = new User();
			creator.setUsername("francesco.mangiacrapa");
			item.getProject().getInfo().getCreationInfo().setUser(creator);

			LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();

			// Test delete the dataset on CATALOGUE
			lifecycleInfo.setPhase("DRAFT");

			System.out.println("By notifying event " + event + " project " + item.getProjectId());
			EventManager.getInstance().notify(event, item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Check notify PROJEC T DELETE D on catalogue.
	 */
	//@Test
	public void checkNotify_PROJECT_DELETED_onCatalogue() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		CatalogueBindingPlugin plugin = (CatalogueBindingPlugin) plugins.get(CatalogueBindingPlugin.DESCRIPTOR.getId());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		CatalogueBindingPluginConfigModel pluginBindingModel;
		try {
			pluginBindingModel = plugin.readEventsSubscribedFromConfigurationInTheUCD(descriptor);
			System.out.println("Events: " + pluginBindingModel);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ItemObserved<Project> item;
		try {
			EventManager.Event event = Event.PROJECT_DELETED;
			item = mockItemObserverd(event);

			// Setting creator
			User creator = new User();
			creator.setUsername("francesco.mangiacrapa");
			item.getProject().getInfo().getCreationInfo().setUser(creator);

			LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();

			// Test delete the dataset on CATALOGUE
			lifecycleInfo.setPhase("DRAFT");

			System.out.println("By notifying event " + event + " project " + item.getProjectId());
			EventManager.getInstance().notify(event, item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Mock item observerd.
	 *
	 * @param event the event
	 * @return the item observed
	 * @throws Exception the exception
	 */
	private static ItemObserved<Project> mockItemObserverd(EventManager.Event event) throws Exception {

		UseCaseDescriptor descriptor = TestProfiles.profiles.get(profileID);

		// notifying the Event.PROJECT_CREATED;
		ItemObserved<Project> item = new ItemObserved<Project>();
		AccountingInfo user = UserUtils.getCurrent().asInfo();
//		System.out.println(user.getUser());
//		sysOut(user);
		System.out.println("User is: " + user.getUser());
		System.out.println("Context is: " + user.getContext());
		item.setUserCaller(user.getUser());
		item.setContext(user.getContext());
		item.setEvent(event);
		item.setUseCaseDescriptor(descriptor);

		TestContextConfig.readContextSettings();

		ScopeProvider.instance.set(TestContextConfig.CONTEXT);
		SecurityTokenProvider.instance.set(TestContextConfig.TOKEN);

		ProjectsCaller clientProjects = GeoportalClientCaller.projects();
		Project theProject = clientProjects.getProjectByID(profileID, projectID);

		item.setProject(theProject);

		return item;
	}

}

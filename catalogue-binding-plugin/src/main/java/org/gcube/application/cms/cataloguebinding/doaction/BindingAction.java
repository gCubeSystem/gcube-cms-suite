package org.gcube.application.cms.cataloguebinding.doaction;

import java.io.IOException;
import java.util.List;

import org.gcube.application.cms.cataloguebinding.EventSubscribed;
import org.gcube.application.cms.cataloguebinding.config.BindingWhen;
import org.gcube.application.cms.cataloguebinding.freemarker.FreemarkerConfig;
import org.gcube.application.cms.cataloguebinding.freemarker.MappingToCatalogue;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BindingAction {

	private static final String TARGET_PHASE_DRAFT = "DRAFT";
	private static final String TARGET_PHASE_PUBLISHED = "Published";
	private ItemObserved<Project> itemObserved;
	private EventSubscribed eventSubscribed;
	private String templateHost;
	private String templatePath;

	public BindingAction(ItemObserved<Project> itemObserved, EventSubscribed eventSubscribed, String templateHost,
			String templatePath) {
		this.itemObserved = itemObserved;
		this.eventSubscribed = eventSubscribed;
		this.templateHost = templateHost;
		this.templatePath = templatePath;
	}

	public void doAction() {
		log.info("Do action called on: {}", itemObserved.getEvent());

		// The project id is the dataset name
		String datasetName = itemObserved.getProjectId();

		try {

			switch (eventSubscribed.getEvent()) {
			case PROJECT_CREATED: {
				log.info("Going to create item with id {} on the catalogue...", datasetName);
				// Create or Update the item on the catalogue
				String catalogueItemJSON = toCatalogueItem();
				if (catalogueItemJSON != null)
					new CatalogueCaller().createOrUpdateTheDatasetOnCatalogue(datasetName, catalogueItemJSON, true);
				break;
			}
			case PROJECT_DELETED: {
				log.info("Going to delete item with name {} on the catalogue...", datasetName);
				new CatalogueCaller().deleteDatasetOnCatalogue(datasetName, true);
				break;
			}
			case PROJECT_UPDATED: {
				log.info("Going to update item with id {} on the catalogue...", datasetName);
				// Create or Update the item on the catalogue
				String catalogueItemJSON = toCatalogueItem();
				if (catalogueItemJSON != null)
					new CatalogueCaller().createOrUpdateTheDatasetOnCatalogue(datasetName, catalogueItemJSON, true);
				break;
			}
			case LIFECYCLE_STEP_PERFORMED: {

				List<BindingWhen> listBindingWhen = eventSubscribed.getWhen();
				if (listBindingWhen == null || listBindingWhen.size() == 0) {
					log.warn("No binding found, so returning!!");
					return;
				}

				for (BindingWhen bindingWhen : listBindingWhen) {
					log.info("Checking configuration matching on: {}", bindingWhen);

					if (bindingWhen.getTarget_phase() == null || bindingWhen.getTarget_phase().size() == 0) {
						log.warn("No binding on target_phase, so returning!!");
						continue;
					}

					if (bindingWhen.getLast_invoked_step() == null
							|| bindingWhen.getLast_invoked_step().length() == 0) {
						log.warn("No binding on last invoked step, so returning!!");
						continue;
					}

					// Reading 'phase' and 'lastInvokedStep' properties from itemObserved
					LifecycleInformation lInfo = itemObserved.getProject().getLifecycleInformation();
					String itemPhase = lInfo.getPhase();
					String lastInvokedStep = lInfo.getLastInvokedStep();
					log.info("ItemObserved phase is: {}, lastInvokedStep is: {}", itemPhase, lastInvokedStep);

					// If there is matching between the plugin configuration and item observed on
					// phase' and 'lastInvokedStep' fields
					if (bindingWhen.getTarget_phase().contains(itemPhase)
							&& bindingWhen.getLast_invoked_step().equalsIgnoreCase(lastInvokedStep)) {
						log.info(
								"The item observerd has phase '{}' and lastInvokedStep '{}' like the plugin configuration model",
								itemPhase, lastInvokedStep);

						if (itemPhase.equalsIgnoreCase(TARGET_PHASE_PUBLISHED)) {
							String catalogueItemJSON = toCatalogueItem();
							if (catalogueItemJSON != null) {
								log.info("Going to create item with name {} on the catalogue...", datasetName);
								new CatalogueCaller().createOrUpdateTheDatasetOnCatalogue(datasetName, catalogueItemJSON,
										true);
							}

						} else if (itemPhase.equalsIgnoreCase(TARGET_PHASE_DRAFT)) {
							// Delete the item on the catalogue
							log.info("Going to delete item with name {} on the catalogue...", datasetName);
							new CatalogueCaller().deleteDatasetOnCatalogue(datasetName, true);
						}
					} else {
						log.info(
								"Skipping the operation!! No mathing {} status with plugin configuration model [bindingWhen phase '{}', lastInvokedStep '{}']",
								ItemObserved.class.getSimpleName(), itemPhase, lastInvokedStep);
					}

				}

				break;
			}

			default:
				break;
			}

		} catch (Exception e) {
			log.error("Error on applying binding action: ", e);
		}
	}

	public String toCatalogueItem() {

		String toCatalogueJSON = null;

		try {
			FreemarkerConfig fmc = new FreemarkerConfig(templateHost);
			Template fmTemplate = fmc.getTemplate(templatePath);

			toCatalogueJSON = MappingToCatalogue.apply(fmTemplate, itemObserved.getProject(),
					itemObserved.getUseCaseDescriptor(), itemObserved.getContext().getId());

		} catch (TemplateException | IOException e) {
			log.error("Error: ", e);
		}

		return toCatalogueJSON;
	}

}

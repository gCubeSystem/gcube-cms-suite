/*
 * 
 */
package org.gcube.application.cms.cataloguebinding.doaction;

import java.net.MalformedURLException;

import javax.ws.rs.WebApplicationException;

import org.gcube.application.cms.serviceaccount.GeoportalServiceAccount;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.client.Item;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class CatalogueCaller.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 3, 2024
 */
@Slf4j
public class CatalogueCaller {

	/**
	 * Creates the or update the dataset on catalogue.
	 *
	 * @param datasetName                the dataset name
	 * @param datasetJSON                the dataset JSON
	 * @param useGeoportalServiceAccount the use geoportal service account
	 * @throws Exception the exception
	 */
	public void createOrUpdateTheDatasetOnCatalogue(String datasetName, String datasetJSON,
			boolean useGeoportalServiceAccount) throws Exception {
		log.info("createOrUpdateTheDatasetOnCatalogue with name {} called. Uses GeoportalServiceAccount: {}",
				datasetName, useGeoportalServiceAccount);

		SecretManager secretManager = SecretManagerProvider.instance.get();
		boolean datasetExists = false;
		Secret geoportalSecret = null;
		try {

			// Starting geoportal secret session
			if (useGeoportalServiceAccount) {
				geoportalSecret = getGeoportalClientSecret();
				secretManager.startSession(geoportalSecret);
			}

			try {
				String dataset = new Item().read(datasetName);
				datasetExists = dataset != null ? true : false;
			} catch (Exception e) {
				log.warn("Error on reading dataset for name: " + datasetName + ". Does not exist? Exception is: "
						+ e.getMessage());
			}

			log.info("datasetExists is {}", datasetExists);

			if (!datasetExists) {

				log.info("calling create dataset...");
				String itemCreated = new Item().create(datasetJSON, false);
				if (itemCreated != null)
					log.info("Dataset created with success");
				else
					log.warn("Dataset not created!");

			} else {

				log.info("calling update dataset...");
				String itemCreated = new Item().update(datasetName, datasetJSON);
				if (itemCreated != null)
					log.info("Dataset updated with success!");
				else
					log.warn("Dataset not updated!");

			}

		} catch (WebApplicationException | MalformedURLException e) {
			log.error("Error occurred on creating the dataset " + datasetName + " on the catalogue", e);
			throw e;
		} finally {
			if (useGeoportalServiceAccount && geoportalSecret != null) {
				secretManager.endSession();
			}

		}
	}

	/**
	 * Delete dataset on catalogue.
	 *
	 * @param datasetName                the dataset name
	 * @param useGeoportalServiceAccount the use geoportal service account
	 */
	public void deleteDatasetOnCatalogue(String datasetName, boolean useGeoportalServiceAccount) {
		log.info("deleteDatasetOnCatalogue with name {} called. Uses GeoportalServiceAccount: {}", datasetName,
				useGeoportalServiceAccount);

		SecretManager secretManager = SecretManagerProvider.instance.get();
		boolean datasetExists = false;
		Secret geoportalSecret = null;

		try {

			// Starting geoportal secret session
			if (useGeoportalServiceAccount) {
				geoportalSecret = getGeoportalClientSecret();
				secretManager.startSession(geoportalSecret);
			}

			try {

				String dataset = new Item().read(datasetName);
				datasetExists = dataset != null ? true : false;

				log.info("datasetExists is {}", datasetExists);
			} catch (Exception e) {
				log.warn("Error on checking datasetExists for name: " + datasetName, e);
			}

			log.info("datasetExists is {}", datasetExists);

			if (datasetExists) {

				log.info("calling delete dataset and purge");
				new Item().delete(datasetName, true);
				log.info("Dataset deleted and purged with success!");
			} else {
				log.warn("Dataset does not exist, returning..");
				return;
			}

		} catch (Exception e) {
			log.error("Error occurred on deleting the dataset " + datasetName + " on the catalogue", e);
		} finally {
			// Terminating geoportal secret session
			if (useGeoportalServiceAccount && geoportalSecret != null) {
				secretManager.endSession();
			}
		}
	}

	/**
	 * List.
	 *
	 * @param limit                      the limit
	 * @param offset                     the offset
	 * @param useGeoportalServiceAccount the use geoportal service account
	 * @return the string
	 */
	public String list(int limit, int offset, boolean useGeoportalServiceAccount) {
		log.info("list called. Uses GeoportalServiceAccount: {}", useGeoportalServiceAccount);

		SecretManager secretManager = SecretManagerProvider.instance.get();
		Secret geoportalSecret = null;
		try {
			// Starting geoportal secret session
			if (useGeoportalServiceAccount) {
				geoportalSecret = getGeoportalClientSecret();
				secretManager.startSession(geoportalSecret);
			}

			String theList = new Item().list(limit, offset);
			return theList;
		} catch (Exception e) {
			log.error("Error occurred on listing datasets", e);
		} finally {
			// Terminating geoportal secret session
			if (useGeoportalServiceAccount && geoportalSecret != null) {
				secretManager.endSession();
			}
		}

		return null;

	}

	/**
	 * Gets the geoportal client secret.
	 *
	 * @return the geoportal client secret
	 */
	private Secret getGeoportalClientSecret() {
		log.debug("get GeoportalClientSecret");
		try {
			return GeoportalServiceAccount.getGeoportalSecret();

		} catch (Exception e) {
			log.error("Error while getting GeoportalServiceAccount SecretManager", e);
		}

		return null;
	}

}

package org.gcube.application.cms.cataloguebinding;

import java.util.List;

import org.gcube.application.cms.cataloguebinding.config.BindingWhen;
import org.gcube.application.cms.plugins.events.EventManager.Event;

import lombok.Data;

@Data
public class EventSubscribed {
	Event event;
	List<BindingWhen> when;
}

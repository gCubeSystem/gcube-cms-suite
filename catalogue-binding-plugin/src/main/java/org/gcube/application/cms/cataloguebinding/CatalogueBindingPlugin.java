package org.gcube.application.cms.cataloguebinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.Document;
import org.gcube.application.cms.cataloguebinding.config.CatalogueBindingPluginConfigModel;
import org.gcube.application.cms.cataloguebinding.config.SubscribeEventsConfig;
import org.gcube.application.cms.cataloguebinding.doaction.BindingAction;
import org.gcube.application.cms.cataloguebinding.util.SerializationUtil;
import org.gcube.application.cms.implementations.utils.UserUtils;
import org.gcube.application.cms.plugins.EventListenerPluginInterface;
import org.gcube.application.cms.plugins.events.EventListener;
import org.gcube.application.cms.plugins.events.EventManager;
import org.gcube.application.cms.plugins.events.EventManager.Event;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.cms.plugins.faults.InitializationException;
import org.gcube.application.cms.plugins.faults.InvalidProfileException;
import org.gcube.application.cms.plugins.faults.MaterializationException;
import org.gcube.application.cms.plugins.faults.ShutDownException;
import org.gcube.application.cms.plugins.implementations.AbstractPlugin;
import org.gcube.application.cms.plugins.reports.InitializationReport;
import org.gcube.application.cms.plugins.reports.Report;
import org.gcube.application.cms.plugins.reports.Report.Status;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.plugins.PluginDescriptor;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;

import com.vdurmont.semver4j.Semver;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class CatalogueBindingPlugin.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 19, 2024
 */
@Slf4j
public class CatalogueBindingPlugin extends AbstractPlugin implements EventListenerPluginInterface {

	public static final String SUBSCRIBE_EVENTS_CONFIG = "subscribeEvents";
	public static final String PLUGIN_ID = "Catalogue-Binding-Plugin";
	public static final String PLUGIN_TYPE = "EventListener";

	public static final PluginDescriptor DESCRIPTOR = new PluginDescriptor(PLUGIN_ID, PLUGIN_TYPE);
	private static final String FREEMARKER_TEMPLATE_PATH = "freemarker_template_path";
	private static final String FREEMARKER_TEMPLATE_HOST = "freemarker_template_host";
	private static final String CONFIG_ENABLED = "enabled";

	static {
		DESCRIPTOR.setVersion(new Semver("1.0.0"));
		DESCRIPTOR.setDescription("Manage the data binding from geoportal to catalogue");
	}

	/**
	 * Gets the descriptor.
	 *
	 * @return the descriptor
	 */
	@Override
	public PluginDescriptor getDescriptor() {

		return DESCRIPTOR;
	}

	/**
	 * Inits the.
	 *
	 * @return the initialization report
	 * @throws InitializationException the initialization exception
	 */
	@Override
	public InitializationReport init() throws InitializationException {
		log.debug("Called init");
		InitializationReport report = null;
		try {
			// Creating all listeners
			EventListener<ItemObserved<Project>> listenerCreated = new EventListener<ItemObserved<Project>>() {

				@Override
				public void updated(ItemObserved<Project> observerd) {
					log.info("listenerCreated fired on item: {} " + observerd);

					boolean subscribed = checkIfSubscribedEvent(observerd);

					if (subscribed) {
						doAction(observerd);
					}

				}
			};

			EventListener<ItemObserved<Project>> listenerUpdated = new EventListener<ItemObserved<Project>>() {

				@Override
				public void updated(ItemObserved<Project> observerd) {
					log.info("listenerUpdated fired on item: {} " + observerd);
					boolean subscribed = checkIfSubscribedEvent(observerd);

					if (subscribed) {
						doAction(observerd);
					}

				}
			};

			EventListener<ItemObserved<Project>> listenerDeleted = new EventListener<ItemObserved<Project>>() {

				@Override
				public void updated(ItemObserved<Project> observerd) {
					log.info("listenerDeleted fired on item: {} " + observerd);
					boolean subscribed = checkIfSubscribedEvent(observerd);

					if (subscribed) {
						doAction(observerd);
					}
				}
			};

			EventListener<ItemObserved<Project>> listenerLCStepPerformed = new EventListener<ItemObserved<Project>>() {

				@Override
				public void updated(ItemObserved<Project> observerd) {
					log.info("listenerLCStepPerformed fired on item: {} " + observerd);
					boolean subscribed = checkIfSubscribedEvent(observerd);

					if (subscribed) {
						doAction(observerd);
					}
				}
			};

			// Subscribing all events
			EventManager eventMngInst = EventManager.getInstance();
			eventMngInst.subscribe(Event.PROJECT_CREATED, listenerCreated);
			eventMngInst.subscribe(Event.PROJECT_UPDATED, listenerUpdated);
			eventMngInst.subscribe(Event.PROJECT_DELETED, listenerDeleted);
			eventMngInst.subscribe(Event.LIFECYCLE_STEP_PERFORMED, listenerLCStepPerformed);

			report = new InitializationReport(Status.OK, PLUGIN_ID + " init performed");
		} catch (Exception e) {
			InitializationException exc = new InitializationException("Unable to initialize " + DESCRIPTOR.getId(), e);
			log.error("init error: {} ", exc);
			throw exc;
		}

		return report;

	}

	/**
	 * Inits the in context.
	 *
	 * @return the initialization report
	 * @throws InitializationException the initialization exception
	 */
	@Override
	@Synchronized
	public InitializationReport initInContext() throws InitializationException {
		InitializationReport report = new InitializationReport();
		try {
			String context = UserUtils.getCurrent().getContext();
			if (getCatalogueBindingMapPerContext() == null) {
				log.info("Initializing in " + context);
				catalogueBindingMap.put(context, new LinkedHashMap<String, CatalogueBindingPluginConfigModel>());
			}
			report.setStatus(Report.Status.OK);
			report.putMessage("Initialized " + DESCRIPTOR.getId() + " in the " + context);
		} catch (Exception e) {
			InitializationException exc = new InitializationException("Unable to initialize " + DESCRIPTOR.getId(), e);
			log.error("initInContext error: {} ", exc);
			throw exc;
		}
		return report;
	}

	// Map<Scope<Map<UDC_ID, CatalogueBindingPluginConfigModel>>
	protected Map<String, Map<String, CatalogueBindingPluginConfigModel>> catalogueBindingMap = null;

	/**
	 * Gets the catalogue binding map per context.
	 *
	 * @return the catalogue binding map per context
	 */
	protected Map<String, CatalogueBindingPluginConfigModel> getCatalogueBindingMapPerContext() {
		String context = UserUtils.getCurrent().getContext();
		log.debug("Getting {} from cache map for context {}", CatalogueBindingPlugin.PLUGIN_ID, context);
		if (catalogueBindingMap == null) {
			catalogueBindingMap = new LinkedHashMap<String, Map<String, CatalogueBindingPluginConfigModel>>();
		}

		// Map<UDC_ID, EventSubscribed>
		Map<String, CatalogueBindingPluginConfigModel> map = catalogueBindingMap.get(context);
		return map == null ? new LinkedHashMap<String, CatalogueBindingPluginConfigModel>() : map;
	}

	/**
	 * Read events subscribed from configuration in the UCD.
	 *
	 * @param useCaseDescriptor the use case descriptor
	 * @return the catalogue binding plugin config model
	 * @throws Exception the exception
	 */
	public CatalogueBindingPluginConfigModel readEventsSubscribedFromConfigurationInTheUCD(
			UseCaseDescriptor useCaseDescriptor) throws Exception {

		log.debug("Reading subscribed events from UCD");

		CatalogueBindingPluginConfigModel cbpcm = null;

		if (useCaseDescriptor == null)
			throw new Exception("Error reading UCD null found");

		try {

			String context = UserUtils.getCurrent().getContext();

			final Map<String, CatalogueBindingPluginConfigModel> mapCatalogBindingPerContext = getCatalogueBindingMapPerContext();

			cbpcm = mapCatalogBindingPerContext.get(useCaseDescriptor.getId());

			if (cbpcm == null) {

				cbpcm = new CatalogueBindingPluginConfigModel();

				List<EventSubscribed> eventsSubscrInTheUCD = new ArrayList<EventSubscribed>();

				Document profileConfiguration = getConfigurationFromProfile(useCaseDescriptor).getConfiguration();
				log.debug("UseCaseDescriptor Configuration is {} ", profileConfiguration);
				
				boolean configEnabled = profileConfiguration.getBoolean(CONFIG_ENABLED, false);
				log.info("Is Config enabled? {} ", CONFIG_ENABLED, configEnabled);
				
				if(configEnabled) {

					for (Object fsConfigObj : profileConfiguration.get(SUBSCRIBE_EVENTS_CONFIG, List.class)) {
						log.debug("Managing {} ", fsConfigObj);
						SubscribeEventsConfig fsConfig = SerializationUtil.convert(fsConfigObj,
								SubscribeEventsConfig.class);
						log.debug("Converted config {}", fsConfig);
						String theEventSubsribed = fsConfig.getEvent();
						if (theEventSubsribed == null || theEventSubsribed.isEmpty())
							throw new MaterializationException(
									"Invalid Field Definition path in configuration [NO MATCH] : " + fsConfig.getEvent());
	
						EventSubscribed eventSub = new EventSubscribed();
	
						Event event = SerializationUtil.convert(theEventSubsribed, Event.class);
						log.debug("Added event {} ", event);
	
						eventSub.setEvent(event);
	
						if (fsConfig.getWhen() != null)
							eventSub.setWhen(fsConfig.getWhen());
	
						eventsSubscrInTheUCD.add(eventSub);
	
					}
				}

				String freemarkerTPath = profileConfiguration.get(FREEMARKER_TEMPLATE_PATH, String.class);
				String freemakerTHost = profileConfiguration.get(FREEMARKER_TEMPLATE_HOST, String.class);

				cbpcm.setContext(context);
				cbpcm.setFreemarker_template_host(freemakerTHost);
				cbpcm.setFreemarker_template_path(freemarkerTPath);
				cbpcm.setListEventSubscribed(eventsSubscrInTheUCD);

				// updating the cache
				mapCatalogBindingPerContext.put(useCaseDescriptor.getId(), cbpcm);
				catalogueBindingMap.put(context, mapCatalogBindingPerContext);

				log.info("Events subscribed read from config {} ", eventsSubscrInTheUCD);

			}
		} catch (InvalidProfileException e) {
			log.warn("Unable to read configuration for {} in the UCD {}", CatalogueBindingPlugin.PLUGIN_ID,
					useCaseDescriptor.getId());
		} catch (Exception e) {
			log.error("Unable to read configuration ", e);
		} catch (Throwable t) {
			log.error("Exception, Unable to read configuration ", t);
		}

		return cbpcm;
	}

	/**
	 * Check if subscribed event.
	 *
	 * @param observerd the observerd
	 * @return true, if successful
	 */
	public boolean checkIfSubscribedEvent(ItemObserved<Project> observerd) {
		log.info("Checking if {} is an subscribed event", observerd.getEvent());
		try {
			CatalogueBindingPluginConfigModel cbpcm = readEventsSubscribedFromConfigurationInTheUCD(
					observerd.getUseCaseDescriptor());

			List<EventSubscribed> listEvents = cbpcm.getListEventSubscribed();
			log.info("List events is {}", listEvents);

			if (listEvents == null) {
				log.info("no event subscribed, returing false");
				return false;
			}

			if (listEvents.stream().filter(e -> e.getEvent().equals(observerd.getEvent())).count() > 0) {
				log.info("the event {} is subscribed from config ", observerd.getEvent());
				return true;
			}

			log.info("the event {} is not subscribed from config ", observerd.getEvent());
			return false;

		} catch (Exception e) {
			log.error("Exception, Error on checking subscribed events", e);
			return false;
		}

	}

	/**
	 * Shutdown.
	 *
	 * @throws ShutDownException the shut down exception
	 */
	@Override
	public void shutdown() throws ShutDownException {
		// TODO Auto-generated method stub

	}

	/**
	 * Do action.
	 *
	 * @param itemObserved the item observed
	 */
	@Override
	public void doAction(ItemObserved<Project> itemObserved) {
		log.debug("doAction called...");

		boolean isSubscribedEvent = checkIfSubscribedEvent(itemObserved);

		log.info("Is the event {} subscribed by " + CatalogueBindingPlugin.class.getSimpleName()
				+ " configured in the UCD? {} ", itemObserved.getEvent(), isSubscribedEvent);

		if (isSubscribedEvent) {

			// Map (UCD_ID, CatalogueBindingPluginConfigModel)
			Map<String, CatalogueBindingPluginConfigModel> mapCatalogBindingPerContext = getCatalogueBindingMapPerContext();

			// CatalogueBindingPluginConfigModel per UCD
			CatalogueBindingPluginConfigModel cbm = mapCatalogBindingPerContext.get(itemObserved.getUCD_Id());

			List<EventSubscribed> list = cbm.getListEventSubscribed().stream()
					.filter(e -> e.getEvent().equals(itemObserved.getEvent())).collect(Collectors.toList());

			if (list.size() > 0) {
				// expected one configuration for event type
				EventSubscribed eventSubscribed = list.get(0);
				BindingAction ba = new BindingAction(itemObserved, eventSubscribed, cbm.getFreemarker_template_host(),
						cbm.getFreemarker_template_path());
				ba.doAction();
			}
		}

	}

}

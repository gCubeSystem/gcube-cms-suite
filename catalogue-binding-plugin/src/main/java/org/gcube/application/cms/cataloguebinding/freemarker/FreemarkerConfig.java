package org.gcube.application.cms.cataloguebinding.freemarker;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FreemarkerConfig {

	private String urlPathToTemplate;
	private RemoteTemplateLoader rtl;
	private Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);

	public FreemarkerConfig(String urlPathToTemplate) throws MalformedURLException, TemplateException {
		log.debug("Init "+FreemarkerConfig.class.getSimpleName()+" with root url: "+urlPathToTemplate);
		this.urlPathToTemplate = urlPathToTemplate;
		this.rtl = new RemoteTemplateLoader(new URL(urlPathToTemplate));
		freeMarkerConfigurer();
	}

	private Configuration freeMarkerConfigurer() throws TemplateException {
		Properties properties = new Properties();
		properties.setProperty("localized_lookup", "false");
		this.cfg.setSettings(properties);
		this.cfg.setTemplateLoader(rtl);
		this.cfg.setDefaultEncoding("UTF-8");
		return cfg;
	}

	public Template getTemplate(String templateName) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {
		return cfg.getTemplate(templateName);
	}
	
	public String getUrlPathToTemplate() {
		return urlPathToTemplate;
	}

}

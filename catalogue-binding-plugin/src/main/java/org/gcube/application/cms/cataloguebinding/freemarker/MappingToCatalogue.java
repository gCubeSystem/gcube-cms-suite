package org.gcube.application.cms.cataloguebinding.freemarker;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.gcube.application.cms.cataloguebinding.gis.GisClient;
import org.gcube.application.cms.cataloguebinding.util.JSONObjectOrdered;
import org.gcube.application.cms.cataloguebinding.util.SerializationUtil;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.RESOLVE_AS;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.TARGET_GEOPORTAL_APP;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class MappingToCatalogue.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 20, 2024
 */
@Slf4j
public class MappingToCatalogue {

	private static final String SYSTEM_TYPE = "system:type";
	private static final String GIS_LINK = "Gis Link";

	/**
	 * Apply.
	 *
	 * @param fmTemplate the fm template
	 * @param theProject the the project
	 * @param ucd        the ucd
	 * @param scope      the scope
	 * @return the string
	 * @throws TemplateException
	 * @throws IOException
	 */
	public static String apply(Template fmTemplate, Project theProject, UseCaseDescriptor ucd, String scope)
			throws TemplateException, IOException {
		log.info("Apply template {} to project with id '{}' and ucd id '{}' called", fmTemplate.getName(),
				theProject.getId(), ucd.getId());

		try {
			// Getting project as document
			Document asDocument = SerializationUtil.asDocument(theProject);
			// Transforming the document as json
			String json = asDocument.toJson();
			// Getting map from json object
			Map toJsonMap = fmtJsonUtil(json);

			// Putting the map as doc to apply the freemarker template
			StringWriter stringWriter = new StringWriter();
			Map root = new HashMap();
			root.put("doc", toJsonMap);
			// applying the freemarker template
			fmTemplate.process(root, stringWriter);
			log.info("template applied with success..");

			String toCatalogueDatasetJSON = stringWriter.toString();
			if (log.isTraceEnabled()) {
				log.trace("Template applyed with result: " + toCatalogueDatasetJSON);
			}

			// Parsing the Catalogue Dataset json as Document Context
			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();
			DocumentContext theDoc = JsonPath.parse(toCatalogueDatasetJSON, configuration);
			log.info("Adding service fields..");

			// Adding GIS LINK to doc
			// TODO ACTIVATE THIS FOR GENERATING GIS LINK

			JSONObject theGisLinkObj = JSONObjectOrdered.instance();
			try {
				String link = GisClient.gisLink(RESOLVE_AS.PUBLIC, ucd.getId(), theProject.getId(), scope,
						TARGET_GEOPORTAL_APP.GEO_DV, true);

				theGisLinkObj.put("key", GIS_LINK);
				theGisLinkObj.put("value", link);
				theDoc.add("$.extras", theGisLinkObj);
			} catch (JSONException e) {
				String error = "Error occurrend on adding " + GIS_LINK;
				log.warn(error, e);
			}

			// Adding system:type to doc
			try {
				JSONObject theSystemType = JSONObjectOrdered.instance();
				theSystemType.put("key", SYSTEM_TYPE);
				theSystemType.put("value", ucd.getName());
				theDoc.add("$.extras", theSystemType);
			} catch (JSONException e) {
				String error = "Error occurrend on adding " + SYSTEM_TYPE;
				log.warn(error, e);
			}

			String catalogueDatasetJSON = prettyPrintJsonUtil(theDoc.jsonString(), false);
			log.info("To catalogue JSON: \n" + catalogueDatasetJSON);
			
			log.info("returning catalogue dataset, json lenght is: " + catalogueDatasetJSON.length());
			return catalogueDatasetJSON;

		} catch (IOException e) {
			log.error(IOException.class.getName() + " on apply mapping to catalogue: ", e);
			throw e;
		} catch (TemplateException e) {
			log.error(TemplateException.class.getName() + " on apply mapping to catalogue: ", e);
			throw e;
		}
	}

	/**
	 * Fmt json util.
	 *
	 * @param json the json
	 * @return the map
	 * @throws JsonParseException   the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException          Signals that an I/O exception has occurred.
	 */
	public static Map<String, Object> fmtJsonUtil(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper OBJECT_MAPPER = new ObjectMapper();
		return OBJECT_MAPPER.readValue(json, new TypeReference<HashMap<String, Object>>() {
		});

	}

	/**
	 * Pretty print json util.
	 *
	 * @param uglyJsonString the ugly json string
	 * @return the string
	 */
	public static String prettyPrintJsonUtil(String uglyJsonString, boolean indent) {
		ObjectMapper mapper = new ObjectMapper();

		if (indent)
			mapper.enable(SerializationFeature.INDENT_OUTPUT);

		Object jsonObject;
		String prettyJson = "";
		try {
			if (indent)
				jsonObject = mapper.readValue(uglyJsonString, Object.class);
			else
				jsonObject = mapper.readValue(uglyJsonString, JsonNode.class);

			prettyJson = mapper.writeValueAsString(jsonObject);
		} catch (IOException e) {
			return uglyJsonString;
		}
		return prettyJson;
	}

}

package org.gcube.application.cms.cataloguebinding.config;

import java.util.List;

import org.gcube.application.cms.cataloguebinding.EventSubscribed;

import lombok.Data;

@Data
public class CatalogueBindingPluginConfigModel {

	String context; // the scope

	List<EventSubscribed> listEventSubscribed;

	String freemarker_template_path;
	String freemarker_template_host;

	Boolean enabled = false;

}

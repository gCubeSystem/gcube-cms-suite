package org.gcube.application.cms.cataloguebinding.config;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Instantiates a new notification when.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BindingWhen {

	List<String> target_phase;
	String last_invoked_step;

}

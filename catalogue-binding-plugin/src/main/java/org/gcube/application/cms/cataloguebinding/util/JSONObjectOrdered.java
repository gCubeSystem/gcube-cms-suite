package org.gcube.application.cms.cataloguebinding.util;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

import org.json.JSONObject;

/**
 * The Class JSONObjecOrdered.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 10, 2022
 */
public final class JSONObjectOrdered {

	/**
	 * Instance.
	 *
	 * @return the JSON object
	 */
	public static JSONObject instance() {
		JSONObject jsonObject = new JSONObject();
		try {
			Field changeMap = jsonObject.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(jsonObject, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} catch (IllegalAccessException | NoSuchFieldException e) {
		}
		return jsonObject;
	}
}

package org.gcube.application.cms.cataloguebinding.config;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubscribeEventsConfig {
	private String event;
	@JsonProperty("when")
	private List<BindingWhen> when;
}

package org.gcube.application.cms.cataloguebinding.freemarker;

import java.net.MalformedURLException;
import java.net.URL;

import freemarker.cache.URLTemplateLoader;


/**
 * The Class RemoteTemplateLoader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 19, 2024
 */
public class RemoteTemplateLoader extends URLTemplateLoader {
	private URL root;

	/**
	 * Instantiates a new remote template loader.
	 *
	 * @param root the root
	 */
	public RemoteTemplateLoader(URL root) {
		super();
		this.root = root;
	}

	/**
	 * Gets the url.
	 *
	 * @param template the template
	 * @return the url
	 */
	@Override
	protected URL getURL(String template) {
		try {
			return new URL(root, "/" + template);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
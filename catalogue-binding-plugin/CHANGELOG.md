# Changelog for org.gcube.application.cms.catalogue-binding-plugin

## [v1.0.0] - 2024-07-23

- First release, implemented the plugin [#26454]

- This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


# Changelog for org.gcube.application.cms.use-cases

## [v1.0.4] - 2023-01-10
- Pom updates

## [v1.0.3] - 2022-12-07
- Pom updates

## [v1.0.2] - 2022-02-24
- Updated parent

## [v1.0.1] - 2021-09-20
- Updated parent

## [v1.0.0] - 2021-09-20
- First release, extracted from client

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
package org.gcube.application.cms.usecases.legacyConcessioni;

import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.cms.usecases.mocks.ConcessionPublisherThread;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportal.common.utils.StorageUtils;

import java.io.File;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.mongoConcessioni;

public class EditFileSet {

    public static void main(String[] args) throws Exception {
        // params
        String context= "/org/gcube/devsec/devVRE";
        String publishOption="true";

        String toUpdateId="6131f42502ad3d2580412da7";
        String toEditPath= Concessione.Paths.piantaByIndex(0);
        String folderPath="/Users/fabioisti/Documents/GNA_Ferrandina_2020_inserimento/New Folder With Items/topografia/Piante";

        //Check params
        Boolean publish = Boolean.parseBoolean(publishOption);
        File sourceFolder=new File(folderPath);
        if(!sourceFolder.canRead()) throw new Exception("Cannot read from "+folderPath);

        System.out.println("!!!!!!! SETTING CONTEXT "+context);
        TokenSetter.set(context);


        //Prepare Fileset
        System.out.println("Preparing request..");
        StorageUtils storage=new StorageUtils();
        AddSectionToConcessioneRequest request= ConcessionPublisherThread.prepareRequest(storage,toEditPath,sourceFolder);

        MongoConcessioni client=mongoConcessioni().build();

        // Unpublish
        System.out.println("Unpublishing "+toUpdateId);
        client.unPublish(toUpdateId);

        // update Fileset
        System.out.println("Removing old fileset.. ");
        client.cleanFileSet(toUpdateId,toEditPath);

        System.out.println("Sending new Fileset ..");
        Concessione result= client.registerFileSet(toUpdateId,request);

        if(publish)
            result = client.publish(toUpdateId);

        System.out.println("Done ");
        System.out.println("Result : "+result);


    }



}

package org.gcube.application.cms.usecases.legacyConcessioni;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.common.model.legacy.Concessione;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.statefulMongoConcessioni;

@Slf4j
public class ClearConcessioni {

    public static void main(String[] args) throws Exception {
      //  String context="/gcube/devsec/devVRE";

        String context="/pred4s/preprod/preVRE";

        TokenSetter.set(context);

        ConcessioniManagerI manager=statefulMongoConcessioni().build();


        ArrayList<String> toSkipIds=new ArrayList<>();
//        toSkipIds.add("6102c8dd02ad3d05b5f81df4");
//        toSkipIds.add("610415af02ad3d05b5f81ee3");

        AtomicLong count=new AtomicLong(0);
        AtomicLong nullCount=new AtomicLong(0);
        AtomicLong errCount=new AtomicLong(0);
        AtomicLong found=new AtomicLong(0);

        Iterator<Concessione> it=null;
        it=manager.getList();
//        it=manager.search("{\"nome\" : {$regex : \"Mock .*\"}, \"creationTime\" :{$gt : \"2021-10-18T13:58:53.326\"}}");

        ExecutorService service = Executors.newFixedThreadPool(3);


        it.forEachRemaining((Concessione c)->{
            found.incrementAndGet();
            service.submit(new Runnable() {
                @Override
                public void run() {
                    try{
                        TokenSetter.set(context);
                        String currentId=c.getMongo_id();
                        if(currentId==null) {
                            System.out.println("ID IS NULL " + c);
                            nullCount.incrementAndGet();
                        }
                        else
                        if(toSkipIds.contains(currentId))
                            System.out.println("Skipping "+currentId);
                        else {
                            System.out.println("Deleting " + c.getMongo_id());
                            manager.deleteById(c.getMongo_id(),true);
                        }
                    }catch(Throwable throwable){
                        System.err.println(throwable);
                        errCount.incrementAndGet();
                        try {Thread.sleep(1000);} catch (InterruptedException i) {}
                    }finally {
                        count.incrementAndGet();
                    }
                }
            });
        });

        while (!service.awaitTermination(1, TimeUnit.MINUTES)) {
            log.info("Waiting .. completed {}, out of {} ",count.get(),found.get());
            if(found.get()==count.get()) service.shutdown();
        }

        System.out.println("Done "+count.get()+" [null : "+nullCount.get()+", err : "+errCount.get()+"]");

    }
}

package org.gcube.application.cms.usecases;

import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.client.UseCaseDescriptors;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportal.common.rest.UseCaseDescriptorsI;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.projects;
import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;
import static org.junit.Assume.assumeTrue;

public class GenericChecks {


    UseCaseDescriptorsI getClient(){
        assumeTrue(GCubeTest.isTestInfrastructureEnabled());
        TokenSetter.set(GCubeTest.getContext());
        return useCaseDescriptors().build();
    }

    @Test
    public void getAll() throws Exception {

            UseCaseDescriptorsI client =getClient();

            QueryRequest request = new QueryRequest();
            // All

            client.query(request).forEachRemaining(u -> {
                try {
                    System.out.println("Obtained UCD "+u.getId());

                    client.getById(u.getId()).getId();

                    Projects pClient = (Projects)projects(u.getId()).build();

                    AtomicLong counter=new AtomicLong(0);
                    pClient.query(new QueryRequest()).forEachRemaining( M -> counter.incrementAndGet());
                    System.out.println("Found "+counter.get()+" elements");

                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    Assert.fail("Unable to check "+u.getId());
                }
            });


    }

}

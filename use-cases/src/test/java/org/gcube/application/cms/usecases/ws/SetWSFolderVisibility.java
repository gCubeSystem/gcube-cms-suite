package org.gcube.application.cms.usecases.ws;

import org.gcube.application.cms.implementations.WorkspaceManager;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public class SetWSFolderVisibility {

    public static void main(String[] args) throws StorageHubException {

        String context="/gcube/devsec/devVRE";
        Boolean setVisible=true;



        TokenSetter.set(context);
        StorageHubClient shc = new StorageHubClient();
        FolderContainer folderContainer = WorkspaceManager.getApplicationBaseFolder(shc);
        if(setVisible)
            folderContainer.setVisible();
        else
            folderContainer.setHidden(); //will not appear in the workspace GUI
        System.out.println("Done, children count  "+folderContainer.list().getItems().size());

    }
}

package org.gcube.application.cms.usecases;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.tests.TestSchemas;
import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

@Slf4j
public class CheckSchemas {

    @Test
    public void check(){
        TestSchemas.schemas.forEach((s, document) -> {
            log.info("Found {} : {}",s,document);
            assertNotNull(document);
            assertTrue(document.size()>0);
        });
    }

}

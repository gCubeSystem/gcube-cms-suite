package org.gcube.application.cms.usecases;

import org.bson.Document;
import org.gcube.application.cms.tests.TestDocuments;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.rest.StepExecutionRequest;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.projects;

public class SingleTest {

    public static void main(String[] args) throws IOException {
        TokenSetter.set(GCubeTest.getContext());

        Projects<Project> client=projects("profiledConcessioni").build();

        String toTestDocument = "/Users/fabioisti/git/gcube-cms-suite/test-data/profiledDocuments/sampleFiles/timpone.json";

        Project p =client.createNew(Document.parse(Files.readFileAsString(
                toTestDocument, Charset.defaultCharset())));

        final StepExecutionRequest step = new StepExecutionRequest();
        step.setStepID("SUBMIT-FOR-REVIEW");
        p = client.performStep(p.getId(),step);

        System.out.println(Serialization.write(p));
        System.out.println("DONE");
    }

}

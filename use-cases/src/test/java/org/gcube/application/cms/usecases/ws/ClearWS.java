package org.gcube.application.cms.usecases.ws;

import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;

import java.util.List;

public class ClearWS {

    public static void main(String[] args) throws StorageHubException {
        String context="/gcube/devsec/devVRE";
        String folderID="0518e4ad-0ef9-4cba-8d4f-8e315146acdd";
        Boolean recursive = false;

        TokenSetter.set(context);
        final StorageHubClient shc = new StorageHubClient();
        shc.openVREFolder().get().getDescription();

        FolderContainer folder=shc.open(folderID).asFolder();

        List<? extends Item> items = folder.list().getItems();
        System.out.println("Folder "+folder.get().getPath()+" has elements : "+ items.size());

        folder.delete();

//
//        items.forEach(item -> {
//            try {
//                System.out.println("Deleting "+item.getPath());
//                shc.open(item.getId()).asFolder().delete();
//            } catch (StorageHubException e) {
//                e.printStackTrace(System.err);
//            }
//        });



    }

}

package org.gcube.application.cms.usecases.legacyConcessioni;

import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.statefulMongoConcessioni;

@Slf4j
/*
Pushes concessioni JSON from folder to geoportal-service
 */
public class ExportMongoConcessioni {


    public static void main(String[] args) {

        File dir= new File("/Users/fabioisti/git/gcube-cms-suite/import1632326294149");
//        String targetContext="/d4science.research-infrastructures.eu/D4OS/GeoNA-Prototype";
        String targetContext= "/gcube/devsec/devVRE";



        ArrayList<Concessione> found=new ArrayList<>();
        for(File elementFolder:dir.listFiles())
            for(File jsonFile:elementFolder.listFiles((dir1, name) -> {return name.endsWith(".json");}))
                try {
                    log.info("Reading "+jsonFile.getAbsolutePath());
                    String json= Files.readFileAsString(jsonFile.getAbsolutePath(), Charset.defaultCharset());
                    found.add(Serialization.read(json,Concessione.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }

        System.out.println("Loaded "+found.size()+" elements from "+dir.getAbsolutePath());

            TokenSetter.set(targetContext);
            ConcessioniManagerI targetManager = statefulMongoConcessioni().build();
            StorageUtils storage = new StorageUtils();

            AtomicLong count = new AtomicLong(0);
            AtomicLong warnCount = new AtomicLong(0);
            AtomicLong errCount = new AtomicLong(0);

            for (Concessione c : found) {
                try {
                    log.info("Using {} {}",c.getNome(),c.getMongo_id());

//                    Objec
//
//                    if (!result.getReport().getStatus().equals(ValidationReport.ValidationStatus.PASSED))
//                        warnCount.incrementAndGet();
                } catch (Throwable throwable) {
                    System.err.println(throwable);
                    errCount.incrementAndGet();
                } finally {
                    count.incrementAndGet();
                }
            }

        System.out.println("Done "+count.get()+" [warn : "+warnCount.get()+", err : "+errCount.get()+"]");
    }



    private static TempFile[] fromPath(String path, StorageUtils storage) throws FileNotFoundException {
        Path baseDir= Paths.get(path);
        ArrayList<TempFile> toReturn=new ArrayList<>();
        for(File f : baseDir.toFile().listFiles()){
            toReturn.add(storage.putOntoStorage(new FileInputStream(f),f.getName()));
        }
        log.info("Loaded {} files from {} ",toReturn.size(),path);
        return toReturn.toArray(new TempFile[toReturn.size()]);
    }


    private static Document getCleanDocument(Concessione c){
       Document toReturn = new Document();
        throw new RuntimeException("Implement this");
    }

}

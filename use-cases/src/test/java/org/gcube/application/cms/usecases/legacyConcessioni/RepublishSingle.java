package org.gcube.application.cms.usecases.legacyConcessioni;

import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.mongoConcessioni;

public class RepublishSingle {

    public static void main(String[] args) throws Exception {
       // TokenSetter.setUma();


        String id="6229364595184b06331b860b";

        MongoConcessioni manager=mongoConcessioni().build();

        manager.unPublish(id);

        System.out.println("Going to publish... ");
        Concessione c=manager.publish(id);

        System.out.println("Result is "+c);
        System.out.println("REPORT STATUST : "+c.getReport().getStatus());
    }
}

package org.gcube.application.cms.usecases.mocks;

import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.gcube.application.cms.tests.Tests;
import org.gcube.application.cms.tests.TokenSetter;
import org.gcube.application.geoportal.common.model.legacy.Concessione;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Slf4j
public class MockFromFolder {

    public static void main(String[] args) throws Exception {
        //PARAMS
//        String context= "/gcube/devsec/devVRE";
        String context = "/pred4s/preprod/preVRE";


        File descriptorsBaseFolder=new File(Tests.BASE_FOLDER,"packages");

        // config csv -> base folder
        Map<String,String> configurations=new HashMap<>();


        //Concessioni 04-03
        configurations.put("concessioni04-03.csv","/Users/fabioisti/Documents/Concessioni 04-03/");
//        configurations.put("concessioni04-03_filtered.csv","/Users/fabioisti/Documents/Concessioni 04-03/");
        //DATASET_GNA_01
        configurations.put("DATASET_GNA_01.csv","/Users/fabioisti/Documents/DATASET_GNA_01");
        //DATASET_GNA_02
        configurations.put("DATASET_GNA_02.csv","/Users/fabioisti/Documents/DATASET_GNA_02");
        // invio_08_02
        configurations.put("invio_08_05.csv","/Users/fabioisti/Documents/invio_08_05");
//        // concessioni 23_04
        configurations.put("concessioni_23_04.csv","/Users/fabioisti/Documents/Concessioni_23_04");

        //reinvii
           configurations.put("reinvii.csv","/Users/fabioisti/Documents/reinvii");

           TokenSetter.set(context);

        long start= System.currentTimeMillis();

        for(Map.Entry<String,String> entry : configurations.entrySet()){
            String packageBaseDir=entry.getValue();
            String csvDescriptor=entry.getKey();
            System.out.println("Reading "+csvDescriptor);


            File baseDir=new File(packageBaseDir);

            CSVReader reader = new CSVReader(new FileReader(new File(descriptorsBaseFolder,csvDescriptor)));
            String [] nextLine;
            //reads one line at a time
            while ((nextLine = reader.readNext()) != null) {
                String projectName = nextLine[0];
                String positionPath = nextLine[1];
                ConcessionPublisherThread.submit(new ConcessionPublisherThread(baseDir,positionPath,projectName));
            }

        }
        System.out.println("Waiting.. ");
        ConcessionPublisherThread.Report report =ConcessionPublisherThread.waitCompletion();


        System.out.println("Completed in "+(System.currentTimeMillis()-start)+"ms");


        System.out.println("SUCCESS "+report.getSuccess().size());
        report.getSuccess().forEach(concessionePrinter);
        System.out.println("ERROR "+report.getError().size());
        report.getError().forEach(concessionePrinter);
        System.out.println("WARNING "+report.getWarning().size());
        report.getWarning().forEach(concessionePrinter);
        System.out.println("NO REPORT "+report.getNoReport().size());
        report.getNoReport().forEach(s -> {
            System.out.println(s);
        });
    }



    private static Consumer<Concessione> concessionePrinter=new Consumer<Concessione>() {
        @Override
        public void accept(Concessione c) {
            try {
                System.out.println(c.getNome() + "\t" + c.getMongo_id() + "\t" + c.getReport().getStatus());
            } catch (Throwable t) {
//                System.out.println(c.getNome()+"\t"+c.getMongo_id()+"\t PROBLEMATIC, NO REPORT");
                throw t;
            }
        }
    };





}

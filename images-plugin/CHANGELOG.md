# Changelog for org.gcube.application.cms.images-plugin

## [v1.0.2] - 2023-01-10
- Pom updates

## [v1.0.1] - 2022-12-07
- Pom updates

## [v1.0.0] - 2022-02-24
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

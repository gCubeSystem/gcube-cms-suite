# Changelog for org.gcube.application.cms.notifications-plugins

## [v1.0.5] - 2024-07-03
- Implemented the notification-plugin [#26453]
- Updated lower range of social-service-client to `2.1.0-SNAPSHOT`

## [v1.0.4] - 2023-09-06
- Using parent version range [#25572]

## [v1.0.3] - 2023-03-06
- [#24702] Fixed the default-lc-managers dependency 

## [v1.0.2] - 2023-01-10
- Pom updates

## [v1.0.1] - 2022-12-07
- Pom updates

## [v1.0.0] - 2022-02-24
- First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

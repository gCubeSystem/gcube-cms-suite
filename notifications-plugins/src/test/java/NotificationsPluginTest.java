
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.gcube.application.cms.implementations.utils.UserUtils;
import org.gcube.application.cms.notifications.NotificationEventsSubscribedConfig;
import org.gcube.application.cms.notifications.NotificationGenericConstants.NOTIFICATION_TYPE;
import org.gcube.application.cms.notifications.NotificationsPlugin;
import org.gcube.application.cms.notifications.config.ExportAsPDF;
import org.gcube.application.cms.notifications.config.NotificationFor;
import org.gcube.application.cms.notifications.config.NotificationWhen;
import org.gcube.application.cms.notifications.config.Notify;
import org.gcube.application.cms.notifications.config.SubscribeNotificationEvent;
import org.gcube.application.cms.notifications.manage.ManageDoActionNotification;
import org.gcube.application.cms.plugins.events.EventManager;
import org.gcube.application.cms.plugins.events.EventManager.Event;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.cms.plugins.events.ItemObserved.OPTIONAL_FIELD;
import org.gcube.application.cms.plugins.faults.InitializationException;
import org.gcube.application.cms.serialization.Serialization;
import org.gcube.application.cms.tests.TestProfiles;
import org.gcube.application.cms.tests.plugins.BasicPluginTest;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.AccountingInfo;
import org.gcube.application.geoportal.common.model.document.accounting.PublicationInfo;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The Class NotificationsPluginTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Apr 10, 2024
 */
public class NotificationsPluginTest extends BasicPluginTest {

	NotificationsPlugin plugin;

	/**
	 * Check plugin.
	 */
	//@Before
	public void checkPlugin() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		plugin = (NotificationsPlugin) plugins.get(NotificationsPlugin.DESCRIPTOR.getId());

//		try {
//			plugin.init();
//		} catch (InitializationException e1) {
//			e1.printStackTrace();
//		}
//		
//		try {
//			plugin.initInContext();
//		} catch (InitializationException e1) {
//			e1.printStackTrace();
//		}

		System.out.println("Plugin check: " + plugin);

	}

	/**
	 * Check plugin config.
	 */
	//@Test
	public void checkPluginConfig() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");
		NotificationEventsSubscribedConfig notificationEventsSubs;
		try {
			System.out.println(NotificationEventsSubscribedConfig.class.getSimpleName() + ": ");
			notificationEventsSubs = plugin.readNotificationsSubscribedFromConfigurationInTheUCD(descriptor);
			List<SubscribeNotificationEvent> events = notificationEventsSubs.getListNotificationEventSubscribed();
			for (SubscribeNotificationEvent subscribeNotificationEvent : events) {
				sysOut(subscribeNotificationEvent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Check notifications SUBMIT_FOR_REVIEW to USERS
	 */
	//@Test
	public void checkNotifications_LIFECYCLE_STEP_PERFORMED_TO_SUBMIT_FOR_REVIEW() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");

		NotificationEventsSubscribedConfig notificationEventsSubs;
		try {
			System.out.println(NotificationEventsSubscribedConfig.class.getSimpleName() + ": ");
			notificationEventsSubs = plugin.readNotificationsSubscribedFromConfigurationInTheUCD(descriptor);
			List<SubscribeNotificationEvent> events = notificationEventsSubs.getListNotificationEventSubscribed();
			for (SubscribeNotificationEvent subscribeNotificationEvent : events) {
				sysOut(subscribeNotificationEvent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ItemObserved<Project> item = mockItemObserverd();
		
		//Setting creator
		User creator = new User();
		creator.setUsername("giancarlo.panichi");
		item.getProject().getInfo().getCreationInfo().setUser(creator);
		
		EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
		item.setEvent(event);
		
		LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();
		
		//Test Stage (Pending Approval, "SUBMIT-FOR-REVIEW")	
		lifecycleInfo.setPhase("Pending Approval");
		lifecycleInfo.setLastInvokedStep("SUBMIT-FOR-REVIEW");
		
		System.out.println("By notifying event " + event + " project " + item.getProjectId());
		EventManager.getInstance().notify(event, item);
	}
	
	/**
	 * Check notifications REJECT_DRAFT to USERS
	 */
	//@Test
	public void checkNotifications_LIFECYCLE_STEP_PERFORMED_TO_REJECT_DRAFT() {
		//org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");

		NotificationEventsSubscribedConfig notificationEventsSubs;
		try {
			System.out.println(NotificationEventsSubscribedConfig.class.getSimpleName() + ": ");
			notificationEventsSubs = plugin.readNotificationsSubscribedFromConfigurationInTheUCD(descriptor);
			List<SubscribeNotificationEvent> events = notificationEventsSubs.getListNotificationEventSubscribed();
			for (SubscribeNotificationEvent subscribeNotificationEvent : events) {
				sysOut(subscribeNotificationEvent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ItemObserved<Project> item = mockItemObserverd();
		
		//Setting creator
		User creator = new User();
		creator.setUsername("gianluca.vannini");
		item.getProject().getInfo().getCreationInfo().setUser(creator);
		
		EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
		item.setEvent(event);
		
		LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();
		//Test Stage (DRAFT, REJECT-DRAFT)
		lifecycleInfo.setPhase("DRAFT");
		lifecycleInfo.setLastInvokedStep("REJECT-DRAFT");
		System.out.println("By notifying event " + event + " project " + item.getProjectId());
		EventManager.getInstance().notify(event, item);
	}
	
	/**
	 * Check the VRE POST
	 */
	//@Test
	public void checkNotifications_LIFECYCLE_STEP_PERFORMED_TO_APPROVE_SUBMITTED() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");

		NotificationEventsSubscribedConfig notificationEventsSubs;
		try {
			System.out.println(NotificationEventsSubscribedConfig.class.getSimpleName() + ": ");
			notificationEventsSubs = plugin.readNotificationsSubscribedFromConfigurationInTheUCD(descriptor);
			List<SubscribeNotificationEvent> events = notificationEventsSubs.getListNotificationEventSubscribed();
			for (SubscribeNotificationEvent subscribeNotificationEvent : events) {
				sysOut(subscribeNotificationEvent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ItemObserved<Project> item = mockItemObserverd();
		
		//Setting creator
		User creator = new User();
		creator.setUsername("francesco.mangiacrapa");
		item.getProject().getInfo().getCreationInfo().setUser(creator);
		
		EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
		item.setEvent(event);
		
		LifecycleInformation lifecycleInfo = item.getProject().getLifecycleInformation();
		
		//Test Stage (Published, null)
		lifecycleInfo.setPhase("Published");
		lifecycleInfo.setLastInvokedStep("APPROVE-SUBMITTED");
		
		System.out.println("By notifying event " + event + " project " + item.getProjectId());
		EventManager.getInstance().notify(event, item);
	}
	

	/**
	 * Sys out.
	 *
	 * @param subscribeNotificationEvent the subscribe notification event
	 */
	private static void sysOut(SubscribeNotificationEvent subscribeNotificationEvent) {
		System.out.println("\n#SubscribeNotificationEvent - Event: " + subscribeNotificationEvent.getEvent());
		List<NotificationFor> listNotificationFor = subscribeNotificationEvent.getNotificationFor();
		for (NotificationFor notificationFor : listNotificationFor) {
			System.out.println("\tNotificationFor - roles: " + notificationFor.getRoles());
			for (NotificationWhen notificationWhen : notificationFor.getWhen()) {
				System.out.println("\t\tNotificationWhen - Target_phase: " + notificationWhen.getTarget_phase()
						+ ", Last_invoked_step: " + notificationWhen.getLast_invoked_step());
				for (Notify notify : notificationWhen.getNotify()) {
					System.out.println("\t\t\tNotify type : " + notify.getType());
					System.out.println("\t\t\tNotify send : " + notify.getSend());
					System.out.println("\t\t\tNotify placeholder_message : " + notify.getPlaceholder_msg());
					if (notify.getExport_as_pdf() != null) {
						System.out.println("\t\t\t\tExport as PDF : " + notify.getExport_as_pdf());
					}
				}
			}
		}
	}
	
	/**
	 * Mock item observerd.
	 *
	 * @return the item observed
	 */
	private static ItemObserved<Project> mockItemObserverd() {
		
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");
		
		// notifying the Event.PROJECT_CREATED;
		ItemObserved<Project> item = new ItemObserved<Project>();
		EventManager.Event event = Event.LIFECYCLE_STEP_PERFORMED;
		AccountingInfo user = UserUtils.getCurrent().asInfo();
//		System.out.println(user.getUser());
//		sysOut(user);
		System.out.println("User is: " + user.getUser());
		System.out.println("Context is: " + user.getContext());
		item.setUserCaller(user.getUser());
		item.setContext(user.getContext());
		item.setEvent(event);
		item.setUseCaseDescriptor(descriptor);

		// MOCK PROJECT
		Project testProject = new Project();
		testProject.setId("655489965bdd5478cca320ea");
		testProject.setProfileID(descriptor.getId());
		PublicationInfo info = new PublicationInfo(descriptor.getCreationInfo(), null, null);
		testProject.setInfo(info);
		
		Document theDocument = new Document();
		theDocument.append("name", "My Great Project");
		testProject.setTheDocument(theDocument);
		
		LifecycleInformation lifecycleInfo = new LifecycleInformation();
		//Pending Approval, "SUBMIT-FOR-REVIEW"	
//		lifecycleInfo.setPhase("Pending Approval");
//		lifecycleInfo.setLastInvokedStep("SUBMIT-FOR-REVIEW");
		
		lifecycleInfo.setPhase("DRAFT");
		lifecycleInfo.setLastInvokedStep("REJECT-DRAFT");
	
		testProject.setLifecycleInformation(lifecycleInfo);
		item.setProject(testProject);
		
		
		item.setOptional(OPTIONAL_FIELD.message, "Il mio grande progetto");
		
		return item;
	}

	/**
	 * Matching notification test.
	 */
	//@Test
	public void matchingNotificationTest() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		
//		String itemPhase = "Pending Approval";
//		String lastInvokedStep = "SUBMIT-FOR-REVIEW";
		
		String itemPhase = "DRAFT";
		String lastInvokedStep = "REJECT-DRAFT";
		
		UseCaseDescriptor descriptor = TestProfiles.profiles.get("profiledConcessioni");
		NotificationEventsSubscribedConfig notificationEventsSubs;
		List<NotificationWhen>  listWhen = new ArrayList<NotificationWhen>();
		try {
			System.out.println(NotificationEventsSubscribedConfig.class.getSimpleName() + ": ");
			notificationEventsSubs = plugin.readNotificationsSubscribedFromConfigurationInTheUCD(descriptor);
			List<SubscribeNotificationEvent> events = notificationEventsSubs.getListNotificationEventSubscribed();
			for (SubscribeNotificationEvent subscribeNotificationEvent : events) {
				for (NotificationFor notificationFor : subscribeNotificationEvent.getNotificationFor()) {
					listWhen.addAll(ManageDoActionNotification.matchNotificationsAccordingItemStatus(notificationFor, itemPhase, lastInvokedStep));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		SubscribeNotificationEvent sne = mockSubscribeNotificationEvent();
		for (NotificationFor notificationFor : sne.getNotificationFor()) {
			listWhen.addAll(ManageDoActionNotification.matchNotificationsAccordingItemStatus(notificationFor, itemPhase, lastInvokedStep));
		}
		*/
		System.out.println("Filtered notifications: "+listWhen);
	}

	/**
	 * Check serialize deserialize.
	 */
	// @Test
	public void checkSerializeDeserialize() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());

		SubscribeNotificationEvent sne = mockSubscribeNotificationEvent();

		try {

			String serializeTOJSON = Serialization.write(sne);
			System.out.println("Serialized toJSON :" + serializeTOJSON);

			SubscribeNotificationEvent deserializeToJSON = Serialization.read(serializeTOJSON,
					SubscribeNotificationEvent.class);
			System.out.println("Deserialized from JSON :" + deserializeToJSON);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Mock subscribe notification event.
	 *
	 * @return the subscribe notification event
	 */
	public static SubscribeNotificationEvent mockSubscribeNotificationEvent() {
		SubscribeNotificationEvent sne = new SubscribeNotificationEvent();
		sne.setEvent(Event.LIFECYCLE_STEP_PERFORMED);

		List<NotificationFor> listNotificationFor = new ArrayList<NotificationFor>();

		NotificationFor notificatioFor = new NotificationFor();
		notificatioFor.setRoles(Arrays.asList("Data-Manager"));

		//Pending Approval, "SUBMIT-FOR-REVIEW"
		List<NotificationWhen> listWhen = new ArrayList<NotificationWhen>();
		NotificationWhen notificationWhen = new NotificationWhen();
		
		notificationWhen.setTarget_phase(Arrays.asList("Pending Approval"));
		notificationWhen.setLast_invoked_step("SUBMIT-FOR-REVIEW");
		
//		notificationWhen.setTarget_phase(Arrays.asList("DRAFT"));
//		notificationWhen.setLast_invoked_step("REJECT-DRAFT");
		
		Notify notify = new Notify();
		notify.setType(NOTIFICATION_TYPE.EMAIL);
		notify.setSend(true);
		ExportAsPDF exportAsPDF = new ExportAsPDF();
		exportAsPDF.setExport(true);
		exportAsPDF.setPlaceholder_msg("Export as PDF placeholder");
		notify.setExport_as_pdf(exportAsPDF);
		// notify.setAttach_pdf_document(true);
		notify.setPlaceholder_msg("Notification Placeholder message");
		notificationWhen.setNotify(Arrays.asList(notify));

		listWhen.add(notificationWhen);
		notificatioFor.setWhen(listWhen);

		listNotificationFor.add(notificatioFor);
		
		sne.setNotificationFor(listNotificationFor);

		return sne;
	}

}

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.text.StringSubstitutor;
import org.gcube.application.cms.notifications.substitutor.SubstitutorPlaceholdersMap;
import org.gcube.application.geoportal.common.utils.tests.GCubeTest;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StringReplacerTest {

	public static String NOTIFICATION_MESSAGE_FILE_PATH = String.format("%s%s", System.getProperty("user.dir"),
			"/src/test/resources/Notifications_Messages.properties");

	public static final List<String> PLACEHOLDERS = Arrays.asList("TITLE_REVIEW_REQUIRED", "MSG_REVIEW_REQUIRED",
			"TITLE_ITEM_REJECTED", "MSG_ITEM_REJECTED", "TITLE_ITEM_REJECTED_REVIEW_REQUIRED",
			"MSG_ITEM_REJECTED_REVIEW_REQUIRED", "MSG_ITEM_PUBLISHED");

	public static String USERNAME = "francesco.mangiacrapa";
	public static String PROJECT_ID = "12345";
	public static String PROJECT_NAME = "\"My Great Project\"";
	public static String APPLICATION_LINK = "https://my_gis_link";

	public static String FILE_URL = "https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/raw/branch/event_manager/D4S_UCDs/DEV/devVRE/notifications/Notifications_Messages.properties";

	//@Test
	public void checkReplace() {
		org.junit.Assume.assumeTrue(GCubeTest.isTestInfrastructureEnabled());

//		Map<String, String> valuesMap = new HashMap<>();
//		valuesMap.put(SubstitutorPlaceholdersMap.USER, USERNAME);
//		valuesMap.put(SubstitutorPlaceholdersMap.PROJECT_NAME, PROJECT_NAME);
//		valuesMap.put(SubstitutorPlaceholdersMap.GIS_LINK, GIS_LINK);
//		valuesMap.put(SubstitutorPlaceholdersMap.PROJECT_ID, PROJECT_ID);
		
		SubstitutorPlaceholdersMap smp = new SubstitutorPlaceholdersMap();
		smp.putUserCaller(USERNAME);
		smp.putPublicDataEntryLink(APPLICATION_LINK);
		smp.putProjectId(PROJECT_ID);
		smp.putProjectName(PROJECT_NAME);
		
		// Build StringSubstitutor
		StringSubstitutor sub = new StringSubstitutor(smp);

		try (BufferedInputStream in = new BufferedInputStream(new URL(FILE_URL).openStream())) {

			Properties prop = new Properties();
			// load a properties file
			prop.load(in);

			for (String placeholder : PLACEHOLDERS) {
				log.info("placeholder: {}", placeholder);
				String templateString = prop.getProperty(placeholder);
				log.info("templateString: {}", templateString);
				// Replace
				String resolvedString = sub.replace(templateString);
				log.info("resolvedString: {}", resolvedString);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

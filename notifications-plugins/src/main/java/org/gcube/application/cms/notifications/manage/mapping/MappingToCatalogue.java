package org.gcube.application.cms.notifications.manage.mapping;

import org.gcube.application.cms.plugins.implementations.Default3PhaseManager;
import org.gcube.application.cms.plugins.implementations.SimpleLifeCycleManager;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;

public class MappingToCatalogue {

	public static CatalogueEventType toCatalogueEventType(String itemPhase, String lastInvokedStep) {

		// SimpleLifeCycleManager
		if (itemPhase.equals(SimpleLifeCycleManager.Phases.PUBLISHED)
				&& (lastInvokedStep == null || lastInvokedStep.equals(SimpleLifeCycleManager.Steps.PUBLISH.getId()))) {
			return CatalogueEventType.ITEM_SUBMITTED;
		} else if (itemPhase.equals(SimpleLifeCycleManager.Phases.UNPUBLISHED) && (lastInvokedStep == null
				|| lastInvokedStep.equals(SimpleLifeCycleManager.Steps.UNPUBLISH.getId()))) {
			// TODO
			// Missing ITEM UNPUBLISHED;

		}

		// Default3PhaseManager
		if (itemPhase.equals(Default3PhaseManager.Phases.PENDING_APPROVAL)
				&& (lastInvokedStep == null || lastInvokedStep.equals(Default3PhaseManager.STEPS.SUBMIT.getId()))) {
			return CatalogueEventType.ITEM_SUBMITTED;
		} else if (itemPhase.equals(Default3PhaseManager.Phases.PUBLISHED)
				&& (lastInvokedStep == null || lastInvokedStep.equals(Default3PhaseManager.STEPS.APPROVE.getId()))) {
			return CatalogueEventType.ITEM_PUBLISHED;
		} else if (itemPhase.equals(LifecycleInformation.CommonPhases.DRAFT_PHASE)
				&& (lastInvokedStep == null || lastInvokedStep.equals(Default3PhaseManager.STEPS.REJECT.getId()))) {
			return CatalogueEventType.ITEM_REJECTED;
		} else if (itemPhase.equals(LifecycleInformation.CommonPhases.DRAFT_PHASE)
				&& (lastInvokedStep == null || lastInvokedStep.equals(Default3PhaseManager.STEPS.UNPUBLISH.getId()))) {
			// TODO
			// Missing ITEM UNPUBLISHED;
		}

		return null;
	}
}

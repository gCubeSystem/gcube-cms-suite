package org.gcube.application.cms.notifications.manage;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.gcube.application.cms.notifications.config.ExportAsPDF;
import org.gcube.application.cms.notifications.config.NotificationWhen;
import org.gcube.application.cms.notifications.config.Notify;
import org.gcube.application.cms.notifications.social.SocialClients;
import org.gcube.application.cms.notifications.substitutor.NMessagesPlaceholdersSubstitutorUtil;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.cms.plugins.events.ItemObserved.OPTIONAL_FIELD;
import org.gcube.application.cms.serviceaccount.GeoportalServiceAccount;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.accounting.User;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.portal.databook.shared.Post;
import org.gcube.portlets.user.uriresolvermanager.geoportal.GeoportalExporterAPI;
import org.gcube.social_networking.social_networking_client_library.NotificationClient;
import org.gcube.social_networking.socialnetworking.model.beans.PostInputBean;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEvent;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NotifyToSocial {

	private List<User> recipientUsers;
	private NMessagesPlaceholdersSubstitutorUtil nMPlaceholdersSUtil;
	private List<NotificationWhen> listNotificationWhen;
	private SocialClients socialClients;
	private CatalogueEventType catalogueEventType;
	private ItemObserved<Project> itemObserved;

	public NotifyToSocial(ItemObserved<Project> itemObserved, List<NotificationWhen> listWhen,
			NMessagesPlaceholdersSubstitutorUtil nMPlaceholdersSUtil, List<User> recipientUsers,
			CatalogueEventType catalogueEventType) {
		this.socialClients = new SocialClients();
		this.itemObserved = itemObserved;
		this.listNotificationWhen = listWhen;
		this.recipientUsers = recipientUsers;
		this.nMPlaceholdersSUtil = nMPlaceholdersSUtil;
		this.catalogueEventType = catalogueEventType;
	}

	public void sendNotification() throws Exception {
		log.info("send notification...");
		List<Notify> listNotifies = toNotify();
		log.info("listNotifies are {} ", listNotifies);

		for (Notify notify : listNotifies) {
			switch (notify.getType()) {
			case USER_NOTIFICATION:
				
				log.info("Notification type {}, send: {}", notify.getType(), notify.getSend());
				if (notify.getSend()) {
					log.debug("Building Notification...");
					String subject = "Notification";
					String body = null;
					subject = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, notify.getPlaceholder_title());
					body = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, notify.getPlaceholder_msg());
					
					ExportAsPDF exportAsPDF = notify.getExport_as_pdf();
					String pdfRequest = getPDFLinkRequest(exportAsPDF);
					if(pdfRequest!=null) {
						String pdfReportMsg = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, exportAsPDF.getPlaceholder_msg());
						body += "\n" + pdfReportMsg;
					}

					String optionalMessage = itemObserved.getOptionalValue(OPTIONAL_FIELD.message);
					if (optionalMessage != null) {
						body += "\n" + optionalMessage;
					}
					
					log.info("Going to send notification to users: {}", recipientUsers);
					log.info("subject: {}", subject);
					log.info("body: {}", body);
					userNotify(subject, body, catalogueEventType);

				}
				break;
			case VRE_POST:
				
				log.info("Notification type {}, send: {}", notify.getType(), notify.getSend());
				if (notify.getSend()) {
					PostInputBean toWrite = new PostInputBean();
					String title = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, notify.getPlaceholder_title());
					String body = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, notify.getPlaceholder_msg());

					if (title != null) {
						toWrite.setPreviewtitle(title);
					}
					
					ExportAsPDF exportAsPDF = notify.getExport_as_pdf();
					String pdfRequest = getPDFLinkRequest(exportAsPDF);
					if(pdfRequest!=null) {
						String pdfReportMsg = nMPlaceholdersSUtil.replacePlaceholder(itemObserved, exportAsPDF.getPlaceholder_msg());
						body += "\n" + pdfReportMsg;
					}

					String optionalMessage = itemObserved.getOptionalValue(OPTIONAL_FIELD.message);

					if (optionalMessage != null) {
						body += "\n" + optionalMessage;
					}

					toWrite.setText(body);

					String previewURL = itemObserved.getOptionalValue(OPTIONAL_FIELD.preview_url);
					System.out.println("previewURL: "+previewURL);
					if (previewURL != null) {
						toWrite.setPreviewurl(previewURL);
					}

					vrePost(toWrite, notify);
				}
				break;
			case EMAIL:
				break;

			default:
				break;
			}
		}

	}
	
	private String getPDFLinkRequest(ExportAsPDF exportAsPDF) {
		try {
			if(exportAsPDF!=null && exportAsPDF.getExport()) {
				GeoportalExporterAPI geoportalExporterAPI = new GeoportalExporterAPI();
				String thePDFShortLink = geoportalExporterAPI.exportProject("pdf", itemObserved.getUCD_Id(), itemObserved.getProjectId(), false, true);
				nMPlaceholdersSUtil.getPlaceholderMapValues().putProjectAsPDFLink(thePDFShortLink);
				return thePDFShortLink;
			}
		} catch (Exception e) {
			log.error("Error while getting PDFLinkRequest", e);
			return null;
		}
		
		return null;
	}

	private void vrePost(PostInputBean toWrite, Notify notify) {

		final SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			final Secret geoportalSecret = GeoportalServiceAccount.getGeoportalSecret();
			secretManager.startSession(geoportalSecret);
			log.debug("{} is going to send vrePost {}", SecretManagerProvider.instance.get().getUser().getUsername(),
					toWrite);

			String bodyWithTag = toWrite.getText() + "\n#" + geoportalSecret.getUser().getUsername();
			log.debug("body with tag: {}", bodyWithTag);
			toWrite.setText(bodyWithTag);
			toWrite.setEnablenotification(true);

			log.info("Calling social - going to write application post: {}", toWrite);
			Post thePost = socialClients.writeApplication(toWrite);
			log.info("{} post created: {} ", notify.getType(), thePost);

		} catch (Exception e) {
			log.error("Error while sending vrePost", e);
		} finally {
			if (secretManager != null) {
				try {
					secretManager.endSession();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void userNotify(String subject, String bodyMessage, CatalogueEventType catalogueEventType)
			throws Exception {
		CatalogueEvent catalogueEvent = getCatalogueEvent(subject, bodyMessage);
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = GeoportalServiceAccount.getGeoportalSecret();
			secretManager.startSession(secret);
			NotificationClient nc = socialClients.getNotificationClient();
			log.info("Calling social - {} is going to send the notification {}",
					SecretManagerProvider.instance.get().getUser().getUsername(), catalogueEvent);
			nc.sendCatalogueEvent(catalogueEvent);
		} catch (Exception e) {
			log.error("Error while sending notification", e);

		} finally {
			if (secretManager != null) {
				try {
					secretManager.endSession();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private List<Notify> toNotify() {
		List<Notify> listNotifies = new ArrayList<Notify>();
		for (NotificationWhen notificationWhen : listNotificationWhen) {
			listNotifies.addAll(notificationWhen.getNotify().stream().filter(n -> n.getSend() == true)
					.collect(Collectors.toList()));
		}
		return listNotifies;
	}

	protected CatalogueEvent getCatalogueEvent(String subject, String bodyMessage) throws Exception {
		CatalogueEvent catalogueEvent = new CatalogueEvent();
		catalogueEvent.setType(catalogueEventType);
		catalogueEvent.setNotifyText(bodyMessage);
		catalogueEvent.setItemId(subject);

		String itemURL = nMPlaceholdersSUtil.getPlaceholderMapValues().getFirstApplicationLink();
		if (itemURL != null) {
			catalogueEvent.setItemURL(new URL(itemURL));
		}

		// Adding recipient users
		String[] usersToNotify = recipientUsers.stream().map(u -> u.getUsername()).toArray(String[]::new);
		catalogueEvent.setIdsToNotify(usersToNotify);
		catalogueEvent.setIdsAsGroup(false);

		return catalogueEvent;
	}

	protected CatalogueEvent toCatalogueEvent(CatalogueEventType catalogueEventType, String messageString,
			String subject, String itemURL, List<String> users) throws Exception {
		CatalogueEvent catalogueEvent = new CatalogueEvent();
		catalogueEvent.setType(catalogueEventType);
		catalogueEvent.setNotifyText(messageString);
		catalogueEvent.setItemId(subject);
		if (itemURL != null) {
			catalogueEvent.setItemURL(new URL(itemURL));
		}
		catalogueEvent.setIdsToNotify(users.toArray(new String[users.size()]));
		catalogueEvent.setIdsAsGroup(false);

		return catalogueEvent;
	}

}

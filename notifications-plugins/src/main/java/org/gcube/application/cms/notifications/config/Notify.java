package org.gcube.application.cms.notifications.config;

import org.gcube.application.cms.notifications.NotificationGenericConstants.NOTIFICATION_TYPE;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notify {
	

	NOTIFICATION_TYPE type;
    Boolean send;
    String placeholder_title;
    String placeholder_msg;
	@JsonProperty("export_as_pdf")
    ExportAsPDF export_as_pdf;
}

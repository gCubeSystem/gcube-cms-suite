package org.gcube.application.cms.notifications.config;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExportAsPDF {

	Boolean export;
	String placeholder_msg;

}

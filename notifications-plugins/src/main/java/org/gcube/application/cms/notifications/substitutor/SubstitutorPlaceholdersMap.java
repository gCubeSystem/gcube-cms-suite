package org.gcube.application.cms.notifications.substitutor;

import java.util.HashMap;

/**
 * The Class SubstitutorPlaceholdersMap.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 15, 2024
 * 
 *         This class contains the placeholder known
 */
public class SubstitutorPlaceholdersMap extends HashMap<String, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6815259883194732178L;

	public static final String PROJECT_ID = "project_id";
	public static final String PROJECT_NAME = "project_name";
	public static final String USER_CALLER = "user_caller";
	private static final String ITEM_CREATOR = "item_creator";

	public static final String PRIVATE_DATA_ENTRY_LINK = "private_data_entry_link";
	public static final String PUBLIC_DATA_ENTRY_LINK = "public_data_entry_link";
	public static final String PRIVATE_DATA_VIEWER_LINK = "private_data_viewer_link";
	public static final String PUBLIC_DATA_VIEWER_LINK = "public_data_viewer_link";

	public static final String PROJECT_AS_PDF_LINK = "project_as_pdf_link";

	/**
	 * Instantiates a new substitutor placeholders map.
	 */
	public SubstitutorPlaceholdersMap() {
		super();
	}

	/**
	 * Put user caller.
	 *
	 * @param username the username
	 * @return the string
	 */
	public String putUserCaller(String username) {
		return put(USER_CALLER, username);
	}

	/**
	 * Put item creator.
	 *
	 * @param username the username
	 * @return the string
	 */
	public String putItemCreator(String username) {
		return put(ITEM_CREATOR, username);
	}

	/**
	 * Put project name.
	 *
	 * @param projectName the project name
	 * @return the string
	 */
	public String putProjectName(String projectName) {
		return put(PROJECT_NAME, projectName);
	}

	/**
	 * Put project id.
	 *
	 * @param projectId the project id
	 * @return the string
	 */
	public String putProjectId(String projectId) {
		return put(PROJECT_ID, projectId);
	}

	/**
	 * Put project as PDF link.
	 *
	 * @param pdfLink the pdf link
	 * @return the string
	 */
	public String putProjectAsPDFLink(String pdfLink) {
		return put(PROJECT_AS_PDF_LINK, pdfLink);
	}

	/**
	 * Put private data entry link.
	 *
	 * @param link the link
	 * @return the string
	 */
	public String putPrivateDataEntryLink(String link) {
		return put(PRIVATE_DATA_ENTRY_LINK, link);
	}

	/**
	 * Put public data entry link.
	 *
	 * @param link the link
	 * @return the string
	 */
	public String putPublicDataEntryLink(String link) {
		return put(PUBLIC_DATA_ENTRY_LINK, link);
	}

	/**
	 * Put private data viewer link.
	 *
	 * @param link the link
	 * @return the string
	 */
	public String putPrivateDataViewerLink(String link) {
		return put(PRIVATE_DATA_VIEWER_LINK, link);
	}

	/**
	 * Put public data viewer link.
	 *
	 * @param link the link
	 * @return the string
	 */
	public String putPublicDataViewerLink(String link) {
		return put(PUBLIC_DATA_VIEWER_LINK, link);
	}

	/**
	 * Gets the first application link.
	 *
	 * @return the first application link
	 */
	// not best solution
	public String getFirstApplicationLink() {

		if (get(PRIVATE_DATA_ENTRY_LINK) != null) {
			return get(PRIVATE_DATA_ENTRY_LINK);
		}
		if (super.get(PUBLIC_DATA_ENTRY_LINK) != null) {
			return get(PUBLIC_DATA_ENTRY_LINK);
		}
		if (super.get(PRIVATE_DATA_VIEWER_LINK) != null) {
			return get(PRIVATE_DATA_VIEWER_LINK);
		}
		if (super.get(PUBLIC_DATA_VIEWER_LINK) != null) {
			return get(PUBLIC_DATA_VIEWER_LINK);
		}
		return null;
	}

}

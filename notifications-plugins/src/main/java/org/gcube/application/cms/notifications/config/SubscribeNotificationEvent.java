package org.gcube.application.cms.notifications.config;

import java.util.List;

import org.gcube.application.cms.plugins.events.EventManager.Event;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscribeNotificationEvent {

	Event event;
	List<NotificationFor> notificationFor;
}

package org.gcube.application.cms.notifications.manage;

import org.gcube.application.cms.notifications.gis.GisClient;
import org.gcube.application.cms.notifications.substitutor.SubstitutorPlaceholdersMap;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.RESOLVE_AS;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.TARGET_GEOPORTAL_APP;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class ResolveProjectLinkFromPlaceholder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Apr 8, 2024
 */
@Slf4j
public class ResolveProjectLinkFromPlaceholder {

	/**
	 * ResolveLink
	 * Replaces {data-entry|data-viewer} placeholder links with their values and returns the updated {#SubstitutorPlaceholdersMap}
	 * 
	 * @param itemObserved the item observed
	 * @param map the map
	 * @param placeholder the placeholder
	 * @return the substitutor placeholders map
	 */
	public static SubstitutorPlaceholdersMap resolveLink(ItemObserved<Project> itemObserved, SubstitutorPlaceholdersMap map,
			String placeholderMessage) {
		
		log.debug("resolveLink called for map {} and placeholderMessage {}", map, placeholderMessage);

		if (placeholderMessage == null || placeholderMessage.isEmpty())
			return map;

		if (placeholderMessage.contains(SubstitutorPlaceholdersMap.PRIVATE_DATA_ENTRY_LINK)) {
			// if the map does not contian the placeholder link value
			if (map.get(SubstitutorPlaceholdersMap.PRIVATE_DATA_ENTRY_LINK) == null) {
				String link = GisClient.gisLink(RESOLVE_AS.PRIVATE, itemObserved.getUCD_Id(),
						itemObserved.getProjectId(), itemObserved.getContext().getId(), TARGET_GEOPORTAL_APP.GEO_DE,
						true);
				map.putPrivateDataEntryLink(link);
				return map;
			}
		}

		if (placeholderMessage.contains(SubstitutorPlaceholdersMap.PUBLIC_DATA_ENTRY_LINK)) {
			// if the map does not contian the placeholder link value
			if (map.get(SubstitutorPlaceholdersMap.PUBLIC_DATA_ENTRY_LINK) == null) {
				String link = GisClient.gisLink(RESOLVE_AS.PUBLIC, itemObserved.getUCD_Id(),
						itemObserved.getProjectId(), itemObserved.getContext().getId(), TARGET_GEOPORTAL_APP.GEO_DE,
						true);
				map.putPublicDataEntryLink(link);
				return map;
			}
		}

		if (placeholderMessage.contains(SubstitutorPlaceholdersMap.PRIVATE_DATA_VIEWER_LINK)) {
			// if the map does not contian the placeholder link value
			if (map.get(SubstitutorPlaceholdersMap.PRIVATE_DATA_VIEWER_LINK) == null) {
				String link = GisClient.gisLink(RESOLVE_AS.PRIVATE, itemObserved.getUCD_Id(),
						itemObserved.getProjectId(), itemObserved.getContext().getId(), TARGET_GEOPORTAL_APP.GEO_DV,
						true);
				map.putPrivateDataViewerLink(link);
				return map;
			}
		}

		if (placeholderMessage.contains(SubstitutorPlaceholdersMap.PUBLIC_DATA_VIEWER_LINK)) {
			if (map.get(SubstitutorPlaceholdersMap.PUBLIC_DATA_VIEWER_LINK) == null) {
				String link = GisClient.gisLink(RESOLVE_AS.PUBLIC, itemObserved.getUCD_Id(),
						itemObserved.getProjectId(), itemObserved.getContext().getId(), TARGET_GEOPORTAL_APP.GEO_DV,
						true);
				map.putPublicDataViewerLink(link);
				return map;
			}
		}

		return map;

	}

}

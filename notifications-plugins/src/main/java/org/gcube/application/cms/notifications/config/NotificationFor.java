package org.gcube.application.cms.notifications.config;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationFor {
	
	List<String> roles;
	@JsonProperty("when")
	List<NotificationWhen> when;

}

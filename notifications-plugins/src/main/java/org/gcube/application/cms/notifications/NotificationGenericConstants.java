package org.gcube.application.cms.notifications;

public class NotificationGenericConstants {

	public static enum CUSTOM_TARGET_PHASE {
		Any
	};

	public static enum CUSTOM_USER_ROLES {
		Item_Creator, Any
	};

	public static enum NOTIFICATION_TYPE {
		USER_NOTIFICATION, EMAIL, VRE_POST
	};

}

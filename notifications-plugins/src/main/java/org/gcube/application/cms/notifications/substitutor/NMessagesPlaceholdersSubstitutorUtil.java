package org.gcube.application.cms.notifications.substitutor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.text.StringSubstitutor;
import org.gcube.application.cms.notifications.manage.ResolveProjectLinkFromPlaceholder;
import org.gcube.application.cms.plugins.events.ItemObserved;
import org.gcube.application.geoportal.common.model.document.Project;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NMessagesPlaceholdersSubstitutorUtil {

	private String linkToNotificationMessages;
	private Properties notificationMessagesProperties;

	private SubstitutorPlaceholdersMap placeholderMapValues;

	private StringSubstitutor stringSub;

	public NMessagesPlaceholdersSubstitutorUtil(String linkToNotificationMessages) throws IOException {
		this.linkToNotificationMessages = linkToNotificationMessages;

		try (BufferedInputStream in = new BufferedInputStream(new URL(linkToNotificationMessages).openStream())) {

			notificationMessagesProperties = new Properties();
			// load a properties file
			notificationMessagesProperties.load(in);

		} catch (IOException e) {
			log.error("Error on reading notificationMessagesFileURL for input: {}", linkToNotificationMessages, e);
			throw e;
		}

	}

	public String getLinkToNotificationMessages() {
		return linkToNotificationMessages;
	}

	public void setPlaceholderMapValues(SubstitutorPlaceholdersMap placeholderMapValues) {
		this.placeholderMapValues = placeholderMapValues;
		stringSub = new StringSubstitutor(placeholderMapValues);
	}

	public String replacePlaceholder(ItemObserved<Project> itemObserved, String notificationPlaceholder) {

		if (notificationPlaceholder == null || notificationPlaceholder.isEmpty())
			return notificationPlaceholder;
		
		log.trace("stringSub is {}",stringSub);

		if (stringSub != null) {

			log.debug("notificationPlaceholder: {}", notificationPlaceholder);
			String templateString = notificationMessagesProperties.getProperty(notificationPlaceholder);
			
			this.placeholderMapValues = ResolveProjectLinkFromPlaceholder.resolveLink(itemObserved, placeholderMapValues, templateString);
			
			log.debug("templateString: {}", templateString);
			// Replace
			String resolvedString = stringSub.replace(templateString);
			log.debug("resolvedString: {}", resolvedString);
			return resolvedString;
		}
		log.info("placeholderMapValues is null, returning source: {}", notificationPlaceholder);
		return notificationPlaceholder;
	}
	
	public SubstitutorPlaceholdersMap getPlaceholderMapValues() {
		return placeholderMapValues;
	}

}

package org.gcube.application.cms.notifications.gis;

import java.util.Map;

import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.RESOLVE_AS;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.TARGET_GEOPORTAL_APP;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GisClient {
	
	public static String gisLink(RESOLVE_AS resolveAs, String profileID, String projectID, String scope, TARGET_GEOPORTAL_APP targetApp, boolean shortLink) {
		log.debug("called gisLink as {} for scope {}, profileId {}, projectId {} ", resolveAs, scope, profileID,
				projectID);

		String theLink = "";
		try {
			log.debug("Trying to generate Geoportal Gis Link...");
			// Contacting the Geoportal-Resolver via UriResolverManager
			UriResolverManager uriResolverManager = new UriResolverManager("GEO");
			GeoportalResolverQueryStringBuilder builder = new GeoportalResolverQueryStringBuilder(profileID, projectID);
			builder.scope(scope);
			builder.resolverAs(resolveAs);

			builder.targetApp(targetApp);
			Map<String, String> params = builder.buildQueryParameters();
			theLink = uriResolverManager.getLink(params, shortLink);
			log.info("Geoportal GisViewer link is {} ", theLink);

		} catch (Exception e) {
			log.error("Error on creating the Geoportal GisViewer link for project id {}", projectID, e);
		}

		return theLink;
	}

}

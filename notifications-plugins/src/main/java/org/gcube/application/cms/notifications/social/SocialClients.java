package org.gcube.application.cms.notifications.social;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portal.databook.shared.Post;
import org.gcube.social_networking.social_networking_client_library.MessageClient;
import org.gcube.social_networking.social_networking_client_library.NotificationClient;
import org.gcube.social_networking.social_networking_client_library.PostClient;
import org.gcube.social_networking.social_networking_client_library.UserClient;
import org.gcube.social_networking.socialnetworking.model.beans.MessageInputBean;
import org.gcube.social_networking.socialnetworking.model.beans.PostInputBean;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class SocialClients.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 20, 2024
 */
@Slf4j
public class SocialClients {

	private PostClient postClient;
	private MessageClient messagesClient;
	private UserClient userClient;
	private NotificationClient notificationClient;

	/**
	 * Instantiates a new social util.
	 */
	public SocialClients() {
		try {
			this.postClient = new PostClient();
		} catch (Exception e) {
			log.error("Error on instacing {}: ", PostClient.class.getSimpleName(), e);
		}

		try {
			this.messagesClient = new MessageClient();
		} catch (Exception e) {
			log.error("Error on instacing {}: ", MessageClient.class.getSimpleName(), e);
		}

		try {
			this.userClient = new UserClient();
		} catch (Exception e) {
			log.error("Error on instacing {}: ", UserClient.class.getSimpleName(), e);
		}

		try {
			this.notificationClient = new NotificationClient();
		} catch (Exception e) {
			log.error("Error on instacing {}: ", NotificationClient.class.getSimpleName(), e);
		}
	}

	/**
	 * Gets the usernames by role.
	 *
	 * @param roleName the role name
	 * @return the usernames by role
	 * @throws Exception the exception
	 */
	public List<String> getUsernamesByRole(String roleName) throws Exception {
		return new ArrayList<String>(userClient.getAllUsernamesByRole(roleName));
	}

	/**
	 * Gets the usernames by scope.
	 *
	 * @return the usernames by scope
	 * @throws Exception the exception
	 */
	public List<String> getUsernamesByScope() throws Exception {
		return new ArrayList<String>(userClient.getAllUsernamesContext());
	}

	/**
	 * Write message.
	 *
	 * @param msg the msg
	 * @return the string
	 * @throws Exception the exception
	 */
	public String writeMessage(MessageInputBean msg) throws Exception {
		return messagesClient.writeMessage(msg);
	}

	/**
	 * Write user post.
	 *
	 * @param toWrite the to write
	 * @return the post
	 */
	public Post writeUserPost(PostInputBean toWrite) {
		return postClient.writeUserPost(toWrite);
	}

	/**
	 * Write application.
	 *
	 * @param toWrite the to write
	 * @return the post
	 */
	public Post writeApplication(PostInputBean toWrite) {
		return postClient.writeApplicationPost(toWrite);
	}

	/**
	 * Gets the post client.
	 *
	 * @return the post client
	 */
	public PostClient getPostClient() {
		return postClient;
	}

	/**
	 * Gets the messages client.
	 *
	 * @return the messages client
	 */
	public MessageClient getMessagesClient() {
		return messagesClient;
	}

	/**
	 * Gets the user client.
	 *
	 * @return the user client
	 */
	public UserClient getUserClient() {
		return userClient;
	}

	/**
	 * Gets the notification client.
	 *
	 * @return the notification client
	 */
	public NotificationClient getNotificationClient() {
		return notificationClient;
	}

}

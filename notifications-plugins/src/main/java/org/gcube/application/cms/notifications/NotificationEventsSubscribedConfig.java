package org.gcube.application.cms.notifications;

import java.util.List;

import org.gcube.application.cms.notifications.config.SubscribeNotificationEvent;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationEventsSubscribedConfig {
	UseCaseDescriptor ucd;
	List<SubscribeNotificationEvent> listNotificationEventSubscribed;
	String linkToNotificationsMessages;
	boolean enabled = true;
}

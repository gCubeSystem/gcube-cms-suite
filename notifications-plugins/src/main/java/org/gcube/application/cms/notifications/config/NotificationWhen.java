package org.gcube.application.cms.notifications.config;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Instantiates a new notification when.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationWhen {

	List<String> target_phase;
	String last_invoked_step;
	List<Notify> notify;

}

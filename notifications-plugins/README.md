gCube CMS Suite : Notifications Plugin
--------------------------------------------------
 
gCube CMS Suite is a set of components designed to manage complex space-temporal Documents defined by metadata Profiles.

This module is expected to contain plugin definitions for the generations of notifications upon certain Lifecycle events. 


## Built with
* [gCube SmartGears] (https://gcube.wiki.gcube-system.org/gcube/SmartGears) - The gCube SmartGears framework 
* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [JAX-RS](https://github.com/eclipse-ee4j/jaxrs-api) - Java™ API for RESTful Web Services
* [Jersey](https://jersey.github.io/) - JAX-RS runtime
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation
[gCube CMS Suite](../) parent module containing references, documentation, guides ad utilities.

This plugin requires a configuration of type:

```json
{
  "subscribeNotifications": [
    {
      "event": "EVENT_NAME",
      "notificationFor": [
        {
          "roles": [
            "USER_ROLE"
          ],
          "when": [
            {
              "target_phase": [
                "TARGET_PHASE"
              ],
              "last_invoked_step": LAST_INVOKED_STEP_1,
              "notify": [
                {
                  "type": "NOTIFICATION_TYPE",
                  "send": "true/false",
                  "placeholder_title": "PLACEHOLDER_TITLE_1",
                  "placeholder_msg": "PLACEHOLDER_MESSAGE_1"
                }
              ]
            },
            {
              "target_phase": [
                "TARGET_PHASE"
              ],
              "last_invoked_step": LAST_INVOKED_STEP_2,
              "notify": [
                {
                  "type": "NOTIFICATION_TYPE",
                  "send": "true/false",
                  "placeholder_title": "PLACEHOLDER_TITLE_2",
                  "placeholder_msg": "PLACEHOLDER_MESSAGE_2"
                }
              ]
            }
          ]
        }
      ]
    }
  ],
  "enabled":true/false,
  "link_to_notifications_messages":  "link to file containing the messages as properties like PLACEHOLDER_MESSAGE_{N} = value"
}
```

Where:

* `enabled` (optional) can be: true/false. Default is true. It enables or not the plugin.
* `link_to_notifications_messages` (mandatory): it is the URL to Notifications Messages file with placeholder values. A property file like PLACEHOLDER_MESSAGE_{N} = value

and

* `EVENT_NAME` (mandatory) can be: `PROJECT_CREATED`, `PROJECT_UPDATED`, `PROJECT_DELETED`, `LIFECYCLE_STEP_PERFORMED`;
* `USER_ROLE` (mandatory) can be: `Data-Manager`, `Data-Editor`, `Member`, `Item_Creator`, `Any`;
* `TARGET_PHASE` (mandatory) can be: `DRAFT`, `Pending Approval`, `Published`, `Any`;
* `LAST_INVOKED_STEP` (optional) can be: `SUBMIT-FOR-REVIEW​`, `APPROVE-SUBMITTED`, `REJECT-DRAFT​`, `UNPUBLISH`;
* `NOTIFICATION_TYPE` (mandatory) can be: `USER_NOTIFICATION`, `VRE_POST`, `EMAIL`;

**PLACEHOLDER_MESSAGE_{N}.properties** managed/known placeholders:

* `${project_name}`
* `${project_id}`
* `${private_data_entry_link}`
* `${public_data_entry_link}`
* `${private_data_viewer_link}`
* `${public_data_viewer_link}`
* `${project_as_pdf_link}`
* `${user_caller}`


e.g. Messages using the placeholders:

```properties
# REVIEW ACTION
TITLE_REVIEW_REQUIRED=Review required for ${project_name}
MSG_REVIEW_REQUIRED=@${user_caller} submitted the project ${project_name}. You are kindly requested to review it and decide either to APPROVE or REJECT it. See the Project at ${private_data_entry_link}
# REJECTED ACTION
TITLE_ITEM_REJECTED=Rejected ${project_name}
MSG_ITEM_REJECTED=The project ${project_name} (id: ${project_id}) has just been rejected by @${user_caller}. See the Project at ${private_data_entry_link}
# REJECTED ACTION - REVIEW REQUIRED
TITLE_ITEM_REJECTED_REVIEW_REQUIRED=Rejected ${project_name}
MSG_ITEM_REJECTED_REVIEW_REQUIRED=The project ${project_name} has just been rejected. You are kindly requested to review it. See the Project at ${private_data_entry_link}
```

[See here a full file with messages](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/raw/branch/event_manager/D4S_UCDs/DEV/devVRE/notifications/Notifications_Messages.properties)


## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience) 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.
 
## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - iMarine(grant no. 283644);
    - EUBrazilOpenBio (grant no. 288754).
- the H2020 research and innovation programme 
    - SoBigData (grant no. 654024);
    - PARTHENOS (grant no. 654119);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - BlueBRIDGE (grant no. 675680);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);

